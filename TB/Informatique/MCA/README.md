# Project Title

MCA - Chargeur de tubes


---

## Prérequis

Pour le developpement, il est nécessaire d'installer python 3 et Django

### Python
Vous pouvez installer Python directement depuis le [Site Officiel](https://www.python.org/downloads/)   


Vous pouvez vérifier l'installation en tapant dans un invité de commande

    $ python3

### Django

Pour installer Django, il suffit décrire

    $ python3 -m pip install Django

## Installation

    $ git clone https://gitlab.forge.hefr.ch/lucas.bueche/mca-polytype.git

Il est conseiller de créer et activer un environnement virtuel pour l'installation des modules : 

    $ python3 -m venv /djenv
    $ source /djenv/bin/activate

Aller dans le dossier du projet et installer les modules nécessaires

    $ cd mca-polytype/TB/Informatique/MCA
    $ pip install -r requirements.txt
    $ pip install asyncua django-bootstrap4

## Launching app

    $ python manage.py runserver

Vous pouvez maintenant aller sur [localhost:8000](http://localhost:8000/) et voir le projet

