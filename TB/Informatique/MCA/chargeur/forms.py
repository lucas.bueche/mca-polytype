from django.forms import ModelForm, ModelChoiceField, Textarea
from django.utils.translation import gettext_lazy as _
from .models import Recipe, Setting

#Form for Setting model
class SettingForm(ModelForm):
    #Redefine init for value verification
    def __init__(self, *args, **kwargs):
        super(SettingForm, self).__init__(*args, **kwargs)
        #Length between 40 and 200
        self.fields['tube_length'].widget.attrs['min'] = 40
        self.fields['tube_length'].widget.attrs['max'] = 200
        #Diameter between 1 and 50
        self.fields['tube_width'].widget.attrs['min'] = 1
        self.fields['tube_width'].widget.attrs['max'] = 50
        #Loading offset above 10
        self.fields['loading_offset'].widget.attrs['min'] = 10

    #Optional Choice Field
    recipe = ModelChoiceField(queryset=Recipe.objects.all(), required=False)

    class Meta:
        model = Setting
        fields = ['setting_name','tube_width','tube_length','loading_offset','recipe']
        labels = {
            'setting_name' : _('Name'),
            'tube_length' : _('Length of tube'),
            'tube_width' : _('Diameter of tube'),
            'loading_offset' : _('Distance offset'),
            'recipe' : _('Recipe'),
        }

#Form for Recipe model
class RecipeForm(ModelForm):
    #Redefine init for value verification
    def __init__(self, *args, **kwargs):
        super(RecipeForm, self).__init__(*args, **kwargs)
        #Crit Speed between 1 and 240
        self.fields['crit_low_speed'].widget.attrs['min'] = 1
        self.fields['crit_low_speed'].widget.attrs['max'] = 240
        #Charging factor between 1 and 5
        self.fields['charging_factor_below_speed'].widget.attrs['min'] = 1
        self.fields['charging_factor_below_speed'].widget.attrs['max'] = 5
        #Wait time between 0 and 360
        self.fields['wait_time_below_speed'].widget.attrs['min'] = 0
        self.fields['wait_time_below_speed'].widget.attrs['max'] = 360
        #Conveyor speed above 0
        self.fields['conveyor_speed_correction'].widget.attrs['min'] = 0
        #Allowed speed change between 0 and 100
        self.fields['allowed_speed_change'].widget.attrs['min'] = 0
        self.fields['allowed_speed_change'].widget.attrs['max'] = 100
        #Start Offset between 0 and 360
        self.fields['start_offset_angle'].widget.attrs['min'] = 0
        self.fields['start_offset_angle'].widget.attrs['max'] = 360
        #Loading factor between 0 and 1
        self.fields['loading_marging_factor'].widget.attrs['min'] = 0
        self.fields['loading_marging_factor'].widget.attrs['max'] = 1
        #Pos angle between 10 and 50
        self.fields['allowed_pos_angle_error'].widget.attrs['min'] = 10
        self.fields['allowed_pos_angle_error'].widget.attrs['max'] = 50
        #Neg angle between -50 and -10
        self.fields['allowed_neg_angle_error'].widget.attrs['min'] = -50
        self.fields['allowed_neg_angle_error'].widget.attrs['max'] = -10

    class Meta:
        model = Recipe
        fields = '__all__'
        labels = {
            'recipe_name' : _('Recipe name'),
            'crit_low_speed' : _('Critical low speed value'),
            'charging_factor_below_speed' : _('Charging speed factor below critical speed'),
            'wait_time_below_speed' : _('Wait time below critical speed'),
            'conveyor_speed_correction' : _('Speed correction on conveyor'),
            'allowed_speed_change' : _('Allowed speed change (in %)'),
            'start_offset_angle' : _('Start offset angle'),
            'loading_marging_factor' : _('Loading marging factor'),
            'allowed_pos_angle_error' : _('Positive angle error allowed'),
            'allowed_neg_angle_error' : _('Negative angle error allowed'),
        }
