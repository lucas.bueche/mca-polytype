# Generated by Django 3.2.5 on 2021-07-14 11:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chargeur', '0020_remove_recipe_master_offset_angle'),
    ]

    operations = [
        migrations.AddField(
            model_name='recipe',
            name='conveyor_speed_correction',
            field=models.FloatField(default=1.5),
            preserve_default=False,
        ),
    ]
