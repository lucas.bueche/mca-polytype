# Generated by Django 3.2.5 on 2021-07-19 16:15

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('chargeur', '0026_alter_error_error_description'),
    ]

    operations = [
        migrations.CreateModel(
            name='Axis_Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('angular_speed', models.DecimalField(decimal_places=2, max_digits=10)),
                ('start_angle', models.DecimalField(decimal_places=2, max_digits=5)),
                ('master_deg', models.DecimalField(decimal_places=2, max_digits=5)),
                ('slave_deg', models.DecimalField(decimal_places=2, max_digits=5)),
            ],
        ),
        migrations.CreateModel(
            name='Chain_Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('speed', models.DecimalField(decimal_places=2, max_digits=10)),
                ('pace', models.DecimalField(decimal_places=2, max_digits=5)),
            ],
        ),
        migrations.CreateModel(
            name='Other_Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('insert_time', models.DecimalField(decimal_places=2, max_digits=6)),
                ('time_between', models.DecimalField(decimal_places=2, max_digits=6)),
                ('time_full', models.DecimalField(decimal_places=2, max_digits=6)),
                ('conveyor_speed', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
        ),
        migrations.CreateModel(
            name='Automate_Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now)),
                ('axis', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='chargeur.axis_data')),
                ('chain', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='chargeur.chain_data')),
                ('other', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='chargeur.other_data')),
            ],
        ),
    ]
