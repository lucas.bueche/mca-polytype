# Generated by Django 3.2.4 on 2021-07-05 11:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chargeur', '0014_auto_20210512_0924'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='offset',
            field=models.FloatField(default=300.0),
        ),
    ]
