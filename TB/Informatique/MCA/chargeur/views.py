import chargeur
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.http import  HttpRequest
from django.contrib import messages
from django.db.models import Avg, Min

from .models import Recipe, Setting, Error, Chain_Data, Axis_Data, Other_Data, Automate_Data
from .forms import RecipeForm, SettingForm

import asyncio
import sys
import logging
import concurrent.futures
from asyncua import Client, Node, ua

#OPC-UA Server address
url = 'opc.tcp://169.254.11.22:4840'

#Automate State Machines
states = ["INIT", "STOP", "DETECT_CHAIN", "CALC_CAM", "RESET_AXIS", "START_SYNCH", "READY", "STARTING", "RUN", "STOPING", "RESTART", "WAIT", "EMERGENCY_STOP","ERROR","DISCONNECTED",]
errors = ["NO_ERROR","SYNCH_ERROR","CHAIN_NOT_STABLE","NOT_STOP","DISJONCTEUR_OFF","DISJONCTEUR_24V_OFF", "Error_FREQVAR_Conveyor"]

#Arrays with information concerning automate errors
errors_name = [
    "No error",
    "Failed to synchronise",
    "Chain not stable",
    "Emergency button",
    "Breaker popped",
    "24V Breaker popped",
    "Conveyor frequency converter error"
]
errors_desc = [
    "No error occurred",
    "Automate did not manage to synchronise with the chain",
    "Speed of chain changed above threshold",
    "Emergency stop button pressed",
    "The top breaker popped to prevent issues",
    "The 24V breaker popped to prevent issues",
    "There was a problem with the conveyor's frequency converter. Could not control anymore",
]
errors_type = ['1', '1', '1', '5', '5', '5', '4']

#String to simplify node naming
node_prefix = "ns=2;s=unit/"
CC = "Calculate_CAM."
SSM = "Sequencer_State_Machine."
MAS = "Master_axis_Synch."
CD = "Chain_Detection."

"""
Creates a new Error instance, and insert it into database
@param name : Name of the error,
       desc : Description of the error,
       type: serioussness of the error
"""
def _create_error(name, desc, type):
    Error.objects.create(error_name=name, error_description=desc, alert_type=type)

"""
Check if an error occurred in automate, and insert it into DB if there is
"""
async def check_for_error():
    try:
        #Connect to the OPC-UA Server
        async with Client(url=url) as client:
            #Check Flag for error
            var = client.get_node(node_prefix +"ERROR_HANDLE.Error_Flag")
            error = await var.read_value()
            if error:
                #Get error and insert it into DB
                var1 = client.get_node(node_prefix + "ERROR_HANDLE.ERROR_VALUE")
                error = await var1.read_value()
                loop = get_or_create_event_loop()
                await loop.run_in_executor(None, _create_error, errors_name[error], errors_desc[error], errors_type[error])
                #Reset flag to false
                await var.write_value(False)
    except Exception as err:
        #Insert exception into DB
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Checking for error)", str(err), '1')

"""
Get event loop, or create new one if no loop.
Used to convert async to sync, or sync to async
@return event_loop
"""
def get_or_create_event_loop():
    try:
        return asyncio.get_event_loop()
    except RuntimeError as ex:
        if "There is no current event loop in thread" in str(ex):
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            return asyncio.get_event_loop()

"""
Get value of calculated variables from automate
@return chain_data, axis_data, other_data : data concerning chain,axis and other sources
"""
async def read_working_data():
    try:
        await check_for_error()
        #connect to automate
        async with Client(url=url) as client:
            #Read data from chain, and instanciate a Chain_Data
            var1 = client.get_node(node_prefix + CC +"Chain_speed")
            var2 = client.get_node(node_prefix + CC +"tube_p_min")
            chain_data = Chain_Data()
            chain_data.speed = round(await var1.read_value(),2)
            chain_data.pace = round(await var2.read_value(),2)
            #Read data from axis, and instanciate an Axis_Data
            var3 = client.get_node(node_prefix + CC +"Master_degs")
            var4 = client.get_node(node_prefix + CC +"Master_cam_start")
            var5 = client.get_node(node_prefix + CC +"Master_cam_deg")
            var6 = client.get_node(node_prefix + CC +"Following_cam_deg")
            axis_data = Axis_Data()
            axis_data.angular_speed = round(await var3.read_value(),2)
            axis_data.start_angle = round(await var4.read_value(),2)
            axis_data.master_deg = round(await var5.read_value(),2)
            axis_data.slave_deg = round(await var6.read_value(),2)
            #Read data from other sources, and instanciate an Other_Data
            var7 = client.get_node(node_prefix + CC +"Insertion_time")
            var8 = client.get_node(node_prefix + CC +"Mean_t_between")
            var9 = client.get_node(node_prefix + CC +"Mean_t_full")
            var10 = client.get_node(node_prefix + CC +"Conveyor_speed_cmd")
            other_data = Other_Data()
            other_data.insert_time = round(await var7.read_value()*1000,2)
            other_data.time_between = round(await var8.read_value(),2)
            other_data.time_full = round(await var9.read_value(),2)
            other_data.conveyor_speed = round(await var10.read_value(),2)
            return chain_data, axis_data, other_data
    except Exception as err:
        #Insert exception into DB
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Reading Data)", str(err),'2')
        return False, False, False

"""
Main page, create new Setting form
@param request: Request to handle
@return HttpResponse page with data
"""
def index(request):
    active = 'index'
    #Create form
    form = SettingForm(request.POST or None)
    #Get current automate state
    loop = get_or_create_event_loop()
    coroutine = get_current_state()
    machine_state = loop.run_until_complete(coroutine)
    #Get data
    loop = get_or_create_event_loop()
    coroutine = read_working_data()
    chain, axis, other = loop.run_until_complete(coroutine)
    if request.method == 'POST':      #Form button
        if 'save' in request.POST:    #New Setting
            if form.is_valid():
                #Save new setting in DB, and notify user
                form.save()
                messages.info(request, 'Setting has been successfully added')
                return redirect('chargeur:index')
        elif 'send' in request.POST:  #Send to automate
            if form.is_valid():
                #Can only send if in INIT or STOP state
                if machine_state <= states.index("STOP"):
                    try:
                         #Send Setting and notify user
                         loop = get_or_create_event_loop()
                         coroutine = send(form.cleaned_data)
                         loop.run_until_complete(coroutine)
                         messages.info(request, 'Setting has been successfully sent')
                    except Exception as err:
                         _create_error(type(err).__name__ + " (Sending Parameter)", str(err), '3')
                         messages.info(request, 'Could not connect to automate')
                else:
                    messages.info(request, 'Incorrect state: cannot send data')
                return redirect('chargeur:index')
        elif 'data' in request.POST:  #Save Data
            #Save Chain, Axis and Other_Data, create and save Automate_Data
            chain.save()
            axis.save()
            other.save()
            data = Automate_Data(chain=chain, axis=axis, other=other)
            data.save()
            messages.info(request, 'Data has been successfully saved')
            return redirect('chargeur:index')
    #Data sent to template
    context = {
        'new' : True,
        'active' : active,
        'form': form,
        'machine_state': states[machine_state],
        'start_button' : start_button_ok(machine_state),
        'stop_button' : stop_button_ok(machine_state),
        'reset_button' : reset_button_ok(machine_state),
        'synchro_state' : synchronization_state(machine_state),
        'loading_state' : loading_state(machine_state),
        'chain' : chain,
        'axis' : axis,
        'other' : other,
        }
    return render(request, 'chargeur/index.html', context)

"""
Send Setting to automate
@param data: cleaned data from Setting Form,
       recipe: optionnal recipe to send with Setting
"""
async def send(data, recipe):
    await check_for_error()
    #Send recipe if it exists
    if recipe:
        await send_recipe_instance(data['recipe'])
    #Connect to automate
    async with Client(url=url) as client:
        #Get node from variable to write into
        var1 = client.get_node(node_prefix + CC + "Tube_length_mm")
        var2 = client.get_node(node_prefix + CC + "Tube_dia_mm")
        var3 = client.get_node(node_prefix + CC + "Loading_offset_tube")
        #Write value into variables
        await var1.write_value(ua.Variant([data['tube_length']], ua.VariantType.Float))
        await var2.write_value(ua.Variant([data['tube_width']], ua.VariantType.Float))
        await var3.write_value(ua.Variant([data['loading_offset']], ua.VariantType.Float))

"""
Get the current state the automate is in
@return state : State of the automate
"""
async def get_current_state():
    try:
        await check_for_error()
        #Connect to automate
        async with Client(url=url) as client:
            #Read value from state variable
            var = client.get_node(node_prefix + SSM +"Current_State")
            state = await var.read_value()
        return state
    except Exception as err:
        #Insert exception into DB
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Reading State)", str(err),'3')
        return len(states)-1

"""
Check the validity of the start button
@param state : State of the automate
@return ok : active button or not
"""
def start_button_ok(state):
    return state == states.index("STOP") or state == states.index("READY")

"""
Check the validity of the stop button
@param state : State of the automate
@return ok : active button or not
"""
def stop_button_ok(state):
    return state == states.index("RUN") or state == states.index("READY") or state == states.index("STARTING")

"""
Check the validity of the reset button
@param state : State of the automate
@return ok : active button or not
"""
def reset_button_ok(state):
    return state == states.index("INIT")

"""
Get the state of the synchronization of the automate with the chain
@param state : State of the automate
@return string : state of the synchronization
"""
def synchronization_state(state):
    if state > states.index("STOP") and state < states.index("READY"):
        return 'orange'
    elif state >= states.index("READY") and state < states.index("RESTART"):
        return 'green'
    else:
        return 'red'

"""
Get the state of the loading, either loading, can load or cannot load
@param state : State of the automate
@return string : state of the loading
"""
def loading_state(state):
    if state == states.index("RUN"):
        return "green"
    elif state == states.index("READY") or state == states.index("STARTING"):
        return "orange"
    else:
        return "red"

"""
Send the instruction corresponding to the physical start button
@param request : Request to handle
@return HttpResponse : Page to send to user
"""
async def start_load(request):
    try:
        await check_for_error()
        #Connect to automate
        async with Client(url=url) as client:
            state = await get_current_state()
            if start_button_ok(state):  #State is correct
                #Write value to corresponding variable
                var = client.get_node(node_prefix + SSM + "OPC_START")
                await var.write_value(True)
                messages.info(request, 'Start button pressed')
            else:
                messages.info(request, 'Incorrect state: cannot press start button')
    except Exception as err:
        #Insert exception in DB
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Start Button)", str(err),'3')
        messages.info(request, "Error : Could not connect to automate")
    finally:
        return redirect('chargeur:load')

"""
Send the instruction corresponding to the physical stop button
@param request : Request to handle
@return HttpResponse : Page to send to user
"""
async def stop_load(request):
    try:
        await check_for_error()
        #Connect to automate
        async with Client(url=url) as client:
            state = await get_current_state()
            if stop_button_ok(state):  #state is correct
                #Write value to corresponding variable
                var = client.get_node(node_prefix + SSM + "OPC_STOP")
                await var.write_value(True)
                messages.info(request, 'Stop button pressed')
            else:
                messages.info(request, 'Incorrect state: cannot press stop button')
    except Exception as err:
        #Insert exception in DB
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Stop Button)", str(err),'4')
        messages.info(request, "Error : Could not connect to automate")
    finally:
        return redirect('chargeur:load')

"""
Send the instruction corresponding to the physical reset button
@param request : Request to handle
@return HttpResponse : Page to send to user
"""
async def reset_load(request):
    try:
        await check_for_error()
        #Connect to automate
        async with Client(url=url) as client:
            state = await get_current_state()
            if reset_button_ok(state):  #State is correct
                #Write value to corresponding variable
                var = client.get_node(node_prefix + SSM + "OPC_RESET")
                await var.write_value(True)
                messages.info(request, 'Reset button pressed')
            else:
                messages.info(request, 'Incorrect state: cannot press reset button')
    except Exception as err:
        #Insert exception in DB
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Reset Button)", str(err),'2')
        messages.info(request, "Error : Could not connect to automate")
    finally:
        return redirect('chargeur:load')

"""
Main page, Setting has been loaded
@param: request : request to handle
        setting_id : id of the setting to load
@return: HttpResponse, page to send to user
"""
def index_load(request, setting_id):
    active = 'index'
    #Get instance and create form based on it
    setting = get_object_or_404(Setting, pk=setting_id)
    form = SettingForm(request.POST or None, instance=setting)
    #Get current state
    loop = get_or_create_event_loop()
    coroutine = get_current_state()
    machine_state = loop.run_until_complete(coroutine)
    #Get data
    loop = get_or_create_event_loop()
    coroutine = read_working_data()
    chain, axis, other = loop.run_until_complete(coroutine)
    if 'save' in request.POST:     #Update Setting
        if form.is_valid():
            form.save()
            messages.info(request, 'Setting has been successfully updated')
            return redirect('chargeur:index_load', setting_id)
    elif 'send' in request.POST:   #Send Setting to automate
        if form.is_valid():
            if machine_state <= states.index("STOP"):  #Correct state
                try:
                    loop = get_or_create_event_loop()
                    if not form.data['recipe']:   #Send setting without recipe
                        coroutine = send(form.cleaned_data, False)
                    else:                         #Send setting with recipe
                        coroutine = send(form.cleaned_data, True)
                    loop.run_until_complete(coroutine)
                    messages.info(request, 'Setting has been successfully sent')
                except Exception as err:
                    #Insert exception in DB and notify user
                    _create_error(type(err).__name__ + " (Sending Parameter)", str(err), '3')
                    messages.info(request, 'Could not connect to automate')
            else:                                      #Wrong state
                messages.info(request, 'Incorrect state: cannot send data')
                return redirect('chargeur:index_load', setting_id)
    elif 'delete' in request.POST: #Delete setting
        setting.delete()
        messages.info(request, 'Setting has been successfully deleted')
        return redirect('chargeur:load')
    elif 'data' in request.POST:   #Save Data in DB
        chain.save()
        axis.save()
        other.save()
        data = Automate_Data(chain=chain, axis=axis, other=other)
        data.save()
        messages.info(request, 'Data has been successfully saved')
        return redirect('chargeur:index_load', setting_id)
    #Data sent to template
    context = {
        'active' : active,
        'form': form,
        'machine_state': states[machine_state],
        'start_button' : start_button_ok(machine_state),
        'stop_button' : stop_button_ok(machine_state),
        'reset_button' : reset_button_ok(machine_state),
        'synchro_state' : synchronization_state(machine_state),
        'loading_state' : loading_state(machine_state),
        'chain' : chain,
        'axis' : axis,
        'other' : other,
    }
    return render(request, 'chargeur/index.html', context)

"""
Main page, choose setting to load
@param request: request to handle
@return HttpResponse : Page sent to user
"""
def load(request):
    active = 'index'
    #Get all existing settings
    setting_list = Setting.objects.order_by('setting_name')
    #Get current state
    loop = get_or_create_event_loop()
    coroutine = get_current_state()
    machine_state = loop.run_until_complete(coroutine)
    #Get data
    loop = get_or_create_event_loop()
    coroutine = read_working_data()
    chain, axis, other = loop.run_until_complete(coroutine)
    if request.method == 'POST':  #Form button
        if 'load' in request.POST:    #Load
            #Redirect to page with setting id
            setting_id =  request.POST['setting_id']
            return redirect('chargeur:index_load', setting_id)
        elif 'data' in request.POST:  #Save Data in DB
            chain.save()
            axis.save()
            other.save()
            data = Automate_Data(chain=chain, axis=axis, other=other)
            data.save()
            messages.info(request, 'Data has been successfully saved')
            return redirect('chargeur:load')
    #Data sent to template
    context = {
        'active' : active,
        'setting_list' : setting_list,
        'machine_state': states[machine_state],
        'start_button' : start_button_ok(machine_state),
        'stop_button' : stop_button_ok(machine_state),
        'reset_button' : reset_button_ok(machine_state),
        'synchro_state' : synchronization_state(machine_state),
        'loading_state' : loading_state(machine_state),
        'chain' : chain,
        'axis' : axis,
        'other' : other,
    }
    return render(request, 'chargeur/index.html', context)

"""
Recipe page, create new recipe
@param request : request to handle
@return HttpResponse : Page sent to user
"""
def recipe(request):
    active = 'recipe'
    #Get current recipe in automate
    loop = get_or_create_event_loop()
    coroutine = read_recipe()
    curr_recipe = loop.run_until_complete(coroutine)
    #Create form
    form = RecipeForm(request.POST or None)
    if request.method == 'POST':  #Form Button
        if 'save' in request.POST:   #Save Recipe
            if form.is_valid():
                #Save in DB and notify user
                form.save()
                messages.info(request, 'Recipe successfully saved')
                return redirect('chargeur:recipe')
        elif 'send' in request.POST: #Send recipe to automate
            if form.is_valid():
                #Get current state
                loop = get_or_create_event_loop()
                coroutine = get_current_state()
                machine_state = loop.run_until_complete(coroutine)
                if machine_state <= states.index("STOP"): #Correct state 
                    try:
                        #Send recipe and notify user
                        loop = get_or_create_event_loop()
                        coroutine = send_recipe(form.cleaned_data)
                        loop.run_until_complete(coroutine)
                        messages.info(request, 'Recipe has been successfully sent')
                    except Exception as err:
                        #Insert exception in DB
                        _create_error(type(err).__name__ + " (Sending Recipe)", str(err), '3')
                        messages.info(request, 'Could not connect to automate')
                else:                                     #Wrong state
                    messages.info(request, 'Incorrect state: cannot send data')
                    return redirect('chargeur:recipe')
    #Data sent to template
    context = {
        'active' : active,
        'new' : True,
        'form' : form,
        'curr_recipe' : curr_recipe,
        }
    return render(request, 'chargeur/recipe.html', context)

"""
Read recipe currently set in automate
@return recipe : recipe set in automate
"""
async def read_recipe():
    await check_for_error()
    #create instance
    recipe = Recipe()
    try:
        #connect to automate
        async with Client(url=url) as client:
            #Read value from automate and set instance attribute
            var1 = client.get_node(node_prefix + CC + "Critical_low_speed")
            var2 = client.get_node(node_prefix + CC + "Faster_charging_factor_below_critLowSpeed")
            var3 = client.get_node(node_prefix + CC + "Wait_time_below_Critical_low_speed")
            var4 = client.get_node(node_prefix + CD + "Allowed_speed_change")
            var5 = client.get_node(node_prefix + MAS + "Start_charging_Offset_angle")
            var6 = client.get_node(node_prefix + CC + "Loading_margin_factor")
            var7 = client.get_node(node_prefix + MAS + "Allowed_Pos_angle_Error")
            var8 = client.get_node(node_prefix + MAS + "Allowed_Neg_angle_Error")
            var9 = client.get_node(node_prefix + CC + "Conveyor_Speed_correction")
            recipe.crit_low_speed = await var1.read_value()
            recipe.charging_factor_below_speed = await var2.read_value()
            recipe.wait_time_below_speed = await var3.read_value()
            recipe.allowed_speed_change = await var4.read_value()
            recipe.start_offset_angle = await var5.read_value()
            recipe.loading_marging_factor = await var6.read_value()
            recipe.allowed_pos_angle_error = await var7.read_value()
            recipe.allowed_neg_angle_error = await var8.read_value()
            recipe.conveyor_speed_correction = await var9.read_value()
    except Exception as err:
        #Insert exception in DB
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Reading Recipe)", str(err), '2')
        return False
    return recipe

"""
Send a recipe based on an instance
@param recipe : instance of the recipe to send
"""
async def send_recipe_instance(recipe):
    await check_for_error()
    #Connect to automate
    async with Client(url=url) as client:
        #Write values from instance to variables in automate
        var1 = client.get_node(node_prefix + CC + "Critical_low_speed")
        var2 = client.get_node(node_prefix + CC + "Faster_charging_factor_below_critLowSpeed")
        var3 = client.get_node(node_prefix + CC + "Wait_time_below_Critical_low_speed")
        var4 = client.get_node(node_prefix + CD + "Allowed_speed_change")
        var5 = client.get_node(node_prefix + MAS + "Start_charging_Offset_angle")
        var6 = client.get_node(node_prefix + CC + "Loading_margin_factor")
        var7 = client.get_node(node_prefix + MAS + "Allowed_Pos_angle_Error")
        var8 = client.get_node(node_prefix + MAS + "Allowed_Neg_angle_Error")
        var9 = client.get_node(node_prefix + CC + "Conveyor_Speed_correction")
        await var1.write_value(ua.Variant([recipe.crit_low_speed], ua.VariantType.Float))
        await var2.write_value(ua.Variant([recipe.charging_factor_below_speed], ua.VariantType.Float))
        await var3.write_value(ua.Variant([recipe.wait_time_below_speed], ua.VariantType.Float))
        await var4.write_value(ua.Variant([recipe.allowed_speed_change], ua.VariantType.Int16))
        await var5.write_value(ua.Variant([recipe.start_offset_angle], ua.VariantType.Double))
        await var6.write_value(ua.Variant([recipe.loading_marging_factor], ua.VariantType.Float))
        await var7.write_value(ua.Variant([recipe.allowed_pos_angle_error], ua.VariantType.Float))
        await var8.write_value(ua.Variant([recipe.allowed_neg_angle_error], ua.VariantType.Float))
        await var9.write_value(ua.Variant([recipe.conveyor_speed_correction], ua.VariantType.Float))

"""
Send recipe based on a form
@param data : cleaned form data of recipe
"""
async def send_recipe(data):
    await check_for_error()
    #Connect to automate
    async with Client(url=url) as client:
        #Write values from instance to variables in automate
        var1 = client.get_node(node_prefix + CC + "Critical_low_speed")
        var2 = client.get_node(node_prefix + CC + "Faster_charging_factor_below_critLowSpeed")
        var3 = client.get_node(node_prefix + CC + "Wait_time_below_Critical_low_speed")
        var4 = client.get_node(node_prefix + CD + "Allowed_speed_change")
        var5 = client.get_node(node_prefix + MAS + "Start_charging_Offset_angle")
        var6 = client.get_node(node_prefix + CC + "Loading_margin_factor")
        var7 = client.get_node(node_prefix + MAS + "Allowed_Pos_angle_Error")
        var8 = client.get_node(node_prefix + MAS + "Allowed_Neg_angle_Error")
        var9 = client.get_node(node_prefix + CC + "Conveyor_Speed_correction")
        await var1.write_value(ua.Variant([data['crit_low_speed']], ua.VariantType.Float))
        await var2.write_value(ua.Variant([data['charging_factor_below_speed']], ua.VariantType.Float))
        await var3.write_value(ua.Variant([data['wait_time_below_speed']], ua.VariantType.Float))
        await var4.write_value(ua.Variant([data['allowed_speed_change']], ua.VariantType.Int16))
        await var5.write_value(ua.Variant([data['start_offset_angle']], ua.VariantType.Double))
        await var6.write_value(ua.Variant([data['loading_marging_factor']], ua.VariantType.Float))
        await var7.write_value(ua.Variant([data['allowed_pos_angle_error']], ua.VariantType.Float))
        await var8.write_value(ua.Variant([data['allowed_neg_angle_error']], ua.VariantType.Float))
        await var9.write_value(ua.Variant([data['conveyor_speed_correction']], ua.VariantType.Float))

"""
Recipe page, details from a recipe's instance
@param request : request to handle
       recipe_ip : id of the recipe to load
@return HttpResponse : Page sent to user
"""
def recipe_detail(request, recipe_id):
    active = 'recipe'
    #Get current recipe
    loop = get_or_create_event_loop()
    coroutine = read_recipe()
    curr_recipe = loop.run_until_complete(coroutine)
    #Get recipe instance and create form based on it
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    form = RecipeForm(request.POST or None, instance=recipe)
    if 'save' in request.POST:      #Update recipe
        if form.is_valid():
            form.save()
            messages.info(request, 'Recipe successfully updated')
            return redirect('chargeur:recipe_detail', recipe_id)
    elif 'send' in request.POST:    #Send recipe to automate
        if form.is_valid():
            #Get current state
            loop = get_or_create_event_loop()
            coroutine = get_current_state()
            machine_state = loop.run_until_complete(coroutine)
            if machine_state <= states.index("STOP"):  #Correct state
                try:
                    #Send recipe and notify user
                    loop = get_or_create_event_loop()
                    coroutine = send_recipe(form.cleaned_data)
                    loop.run_until_complete(coroutine)
                    messages.info(request, 'Recipe has been successfully sent')
                    return redirect('chargeur:recipe_detail', recipe_id)
                except Exception as err:
                    #Insert exception in DB
                    _create_error(type(err).__name__ + " (Sending Recipe)", str(err), '3')
                    messages.info(request, 'Could not connect to automate')
                    return redirect('chargeur:recipe_detail', recipe_id)
            else:                                       #Wrong state
                messages.info(request, 'Incorrect state: cannot send data')
                return redirect('chargeur:recipe_detail', recipe_id)
    elif 'delete' in request.POST:  #Delete recipe
        recipe.delete()
        messages.info(request, 'Recipe has been successfully deleted')
        return redirect('chargeur:load_recipe')
    #Data sent to template
    context = {
        'active' : active,
        'form': form,
        'curr_recipe': curr_recipe,
    }
    return render(request, 'chargeur/recipe.html', context)

"""
Recipe page, load recipe
@param request : request to handle
@return HttpResponse : Page sent to user
"""
def load_recipe(request):
    active = 'recipe'
    #Get current recipe
    loop = get_or_create_event_loop()
    coroutine = read_recipe()
    curr_recipe = loop.run_until_complete(coroutine)
    #Get all recipes
    recipe_list = Recipe.objects.order_by('recipe_name')
    if request.method == 'POST':
        #Load recipe based on id
        recipe_id = request.POST['recipe_id']
        return redirect('chargeur:recipe_detail', recipe_id)
    #Data sent to template
    context = {
        'active' : active,
        'recipe_list': recipe_list,
        'curr_recipe': curr_recipe,
    }
    return render(request, 'chargeur/recipe.html', context)

"""
Convert an int value to days, hours, minutes and seconds
@param value : value to convert
@return s : seconds,
        m : minutes,
        h : hours,
        d : days
"""
def int_to_dhms(value):
    value = value//1000
    s= value%60
    value = value//60
    m=value%60
    value = value//60
    h=value%24
    value = value//24
    d = value
    return d,h,m,s

"""
Get stats from Automate_Data in DB
@return average_pace : average pace from values in Chain_Data table
        average_insert : average insertion time from values in Other_Data table
        fastest_insert : fastest insertion time from values in Ohter_data table
        samples : number of values in Automate_Data table
"""
def get_stats_db():
    average_pace = Chain_Data.objects.all().aggregate(Avg('pace'))['pace__avg'] or 0.0
    average_insert = Other_Data.objects.all().aggregate(Avg('insert_time'))['insert_time__avg'] or 0.0
    fastest_insert = Other_Data.objects.all().aggregate(Min('insert_time'))['insert_time__min'] or 0.0
    samples = Automate_Data.objects.all().count()
    return round(average_pace,2), round(average_insert,2), round(fastest_insert,2), samples

"""
Stats page, display all automate and DB stats
@param request : request to handle
@return HttpResponse : Page sent to user
"""
async def stats(request):
    active = 'stats'
    try:
        await check_for_error()
        #Connect to automate
        async with Client(url=url) as client:
            #Read values from stat variable
            var1 = client.get_node(node_prefix + "Camming.Loaded_Tubes_counter_totaliser")
            var2 = client.get_node(node_prefix + "Camming.Loaded_Tubes_counter")
            var3 = client.get_node(node_prefix + CD +"Time_counter_current_cycle_INT_totaliser")
            total_tube = await var1.read_value()
            current_tube = await var2.read_value()
            time_run_total = await var3.read_value()
    except Exception as err:
        #Set dummy values and insert exception in DB
        total_tube = 'cannot connect'
        current_tube = 'cannot connect'
        time_run_total = 0
        loop = get_or_create_event_loop()
        await loop.run_in_executor(None, _create_error, type(err).__name__ + " (Reading stats)", str(err), '2')
    #Get time in RUN mode in days, hours, minutes and seconds
    day,hour,min,sec = int_to_dhms(time_run_total*4)
    #Get stats from DB
    loop = get_or_create_event_loop()
    avg_pace, avg_time, fastest_time, samples = await loop.run_in_executor(None, get_stats_db)
    #Data sent to template
    context = {
        'active' : active,
        'total_tube' : total_tube,
        'current_tube' : current_tube,
        'day' : day,
        'hour': hour,
        'min' : min,
        'sec': sec,
        'average_pace' : avg_pace,
        'average_insert' : avg_time,
        'fastest_insert' : fastest_time,
        'samples' : samples,
    }
    return render(request, 'chargeur/stats.html', context)

"""
Error page, display un-acked errors
@param request : request to handle
@return HttpResponse : Page sent to user
"""
def error(request):
    active = 'errors'
    #Get all un-acked errors, sorted by gravity
    warning_list = Error.objects.filter(alert_type='1').exclude(ack=True).order_by("-timestamp")
    low_list = Error.objects.filter(alert_type='2').exclude(ack=True).order_by("-timestamp")
    medium_list = Error.objects.filter(alert_type='3').exclude(ack=True).order_by("-timestamp")
    high_list = Error.objects.filter(alert_type='4').exclude(ack=True).order_by("-timestamp")
    critical_list = Error.objects.filter(alert_type='5').exclude(ack=True).order_by("-timestamp")
    #Data sent to template
    context = {
        'active' : active,
        'warning_list':warning_list,
        'low_list':low_list,
        'medium_list':medium_list,
        'high_list':high_list,
        'critical_list':critical_list,
        }
    return render(request, 'chargeur/error.html', context)

"""
Error page, display all errors in DB
@param request : request to handle
@return HttpResponse : Page sent to user
"""
def error_all(request):
    active = 'errors'
    #Get all errors, sorted by gravity
    warning_list = Error.objects.filter(alert_type='1').order_by("-timestamp")
    low_list = Error.objects.filter(alert_type='2').order_by("-timestamp")
    medium_list = Error.objects.filter(alert_type='3').order_by("-timestamp")
    high_list = Error.objects.filter(alert_type='4').order_by("-timestamp")
    critical_list = Error.objects.filter(alert_type='5').order_by("-timestamp")
    #Data sent to template
    context = {
        'active' : active,
        'warning_list':warning_list,
        'low_list':low_list,
        'medium_list':medium_list,
        'high_list':high_list,
        'critical_list':critical_list,
        'all' : True,
        }
    return render(request, 'chargeur/error.html', context)

"""
Error page, display an error's details
@param request : request to handle,
       errror_id : id of the error to display
@return HttpResponse : Page sent to the user
"""
def error_detail(request, error_id):
    active = 'errors'
    #Get error based on id
    error = get_object_or_404(Error, pk=error_id)
    if request.method == 'POST':
        #Ack the error and save the modification
        error.ack = True
        error.save()
        return redirect('chargeur:error_detail', error_id)
    #Data sent to template
    context = {
        'active' : active,
        'error' : error,
        'error_id' : error_id,
    }
    return render(request, 'chargeur/error_detail.html', context)

"""
Unused, handler of subscription from asyncua 
"""
class SubscriptionHandler:
    def datachange_notification(self, node: Node, val, data):

        req = HttpRequest()
        req.method = 'GET'
        return index(req)

"""
Unused, method to subscription to specific variables from OPC-UA server
"""
async def sub():
    client = Client(url=url)
    async with client:
        handler = SubscriptionHandler()
        var = client.get_node("ns=2;s=unit/Sequencer_State_Machine.Current_State")
        subscription = await client.create_subscription(500, handler)
        nodes = [
            var
        ]
        await subscription.subscribe_data_change(nodes)
        while True:
            await asyncio.sleep(10)
