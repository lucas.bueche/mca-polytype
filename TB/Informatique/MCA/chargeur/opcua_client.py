import asyncio
import sys
import logging
from asyncua import Client, Node, ua

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger('asyncua')

async def browse_nodes(node: Node):
    """
    Build a nested node tree dict by recursion (filtered by OPC UA objects and variables).
    """
    node_class = await node.read_node_class()
    children = []
    for child in await node.get_children():
        if await child.read_node_class() in [ua.NodeClass.Object, ua.NodeClass.Variable]:
            children.append(
                await browse_nodes(child)
            )
    if node_class != ua.NodeClass.Variable:
        var_type = None
    else:
        try:
            var_type = (await node.read_data_type_as_variant_type()).value
        except ua.UaError:
            _logger.warning('Node Variable Type could not be determined for %r', node)
            var_type = None
    return {
        'id': node.nodeid.to_string(),
        'name': (await node.read_display_name()).Text,
        'cls': node_class.value,
        'children': children,
        'type': var_type,
    }

async def main():
    url = 'opc.tcp://169.254.11.22:4840'
    
    async with Client(url=url) as client:
        #_logger.info('Children of root are: %r', await client.nodes.root.get_children())

        client.load_type_definitions()

        var1 = client.get_node("ns=2;s=unit/Calculate_CAM.Current_tube")
        print("STRUCT", await var1.read_value())
        tube = await var1.read_value()
        print(tube.__dict__)
        #await var1.write_value(False)

if __name__ == '__main__':
    asyncio.run(main())
