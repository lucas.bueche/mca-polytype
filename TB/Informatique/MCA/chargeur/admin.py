from django.contrib import admin

from .models import Recipe, Setting, Error, Chain_Data, Axis_Data, Other_Data, Automate_Data
# Register your models here.

admin.site.register(Recipe)
admin.site.register(Setting)
admin.site.register(Error)
admin.site.register(Chain_Data)
admin.site.register(Axis_Data)
admin.site.register(Other_Data)
admin.site.register(Automate_Data)
