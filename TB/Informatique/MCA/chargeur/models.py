from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.forms import ModelForm

# Create your models here.

#Recipe : advanced parameters in automate
class Recipe(models.Model):
    recipe_name = models.CharField(max_length=200, unique=True)
    crit_low_speed = models.FloatField()
    conveyor_speed_correction = models.FloatField()
    charging_factor_below_speed = models.FloatField()
    wait_time_below_speed = models.FloatField()
    allowed_speed_change = models.IntegerField()
    start_offset_angle = models.FloatField()
    loading_marging_factor = models.FloatField()
    allowed_pos_angle_error = models.FloatField()
    allowed_neg_angle_error = models.FloatField()
    note = models.TextField(blank=True)

    def __str__(self):
        return self.recipe_name

#Setting : Base parameters in automate
class Setting(models.Model):
    setting_name = models.CharField(max_length=200, unique=True)
    loading_offset = models.FloatField(default=120.0)
    tube_length = models.FloatField()
    tube_width = models.FloatField()
    recipe = models.ForeignKey(Recipe, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.setting_name

#Error : Issues popping in automate or interface
class Error(models.Model):
    class Alert_Type(models.TextChoices):
        WARNING = '1', _('Warning')
        LOW = '2', _('Low')
        MEDIUM = '3', _('Medium')
        HIGH = '4', _('High')
        CRITICAL = '5', _('Critical')

    error_name = models.CharField(max_length=100)
    error_description = models.TextField(max_length=300)
    alert_type = models.CharField(max_length=1, choices=Alert_Type.choices, default=Alert_Type.WARNING)
    timestamp = models.DateTimeField(default=timezone.now)
    ack = models.BooleanField(default=False)
    date_modified = models.DateTimeField(auto_now=True)

#Chain_Data: Data concerning the Chain
class Chain_Data(models.Model):
    speed = models.DecimalField(max_digits=10, decimal_places=2)
    pace = models.DecimalField(max_digits=5, decimal_places=2)

#Axis_Data: Data concerning the axis in automate
class Axis_Data(models.Model):
    angular_speed = models.DecimalField(max_digits=10, decimal_places=2)
    start_angle = models.DecimalField(max_digits=5, decimal_places=2)
    master_deg = models.DecimalField(max_digits=5, decimal_places=2)
    slave_deg = models.DecimalField(max_digits=5, decimal_places=2)

#Other_Data: Data concerning other sources
class Other_Data(models.Model):
    insert_time = models.DecimalField(max_digits=6, decimal_places=2)
    time_between = models.DecimalField(max_digits=6, decimal_places=2)
    time_full = models.DecimalField(max_digits=6, decimal_places=2)
    conveyor_speed = models.DecimalField(max_digits=10, decimal_places=2)

#Automate_Data: Data got from Automate
class Automate_Data(models.Model):
    chain = models.ForeignKey(Chain_Data, on_delete=models.CASCADE, null=True)
    axis = models.ForeignKey(Axis_Data, on_delete=models.CASCADE, null=True)
    other = models.ForeignKey(Other_Data, on_delete=models.CASCADE, null=True)
    timestamp = models.DateTimeField(default=timezone.now)
