from django.urls import path, register_converter

from . import views

app_name = 'chargeur'
urlpatterns = [
    #//ex: /chargeur/
    path('new/', views.index, name='index'),
    path('start/', views.start_load, name='loading_start'),
    path('stop/', views.stop_load, name='loading_stop'),
    path('reset/', views.reset_load, name='loading_reset'),
    path('send/', views.send, name='send'),
    path('setting/<int:setting_id>/', views.index_load, name='index_load'),
    path('', views.load, name='load'),
    path('recipe/', views.recipe, name='recipe'),
    path('recipe/<int:recipe_id>/', views.recipe_detail, name='recipe_detail'),
    path('recipe/load/', views.load_recipe, name='load_recipe'),
    path('stats/', views.stats, name='stats'),
    path('error/', views.error, name='error'),
    path('error/all/', views.error_all, name='error_all'),
    path('error/<int:error_id>/', views.error_detail, name='error_detail'),
]
