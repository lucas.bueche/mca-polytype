                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2492974
Raspberry Pi DIN Rail Case by mdkendall is licensed under the Creative Commons - Attribution - Share Alike license.
http://creativecommons.org/licenses/by-sa/3.0/

# Summary

A remix of 0110-M-P's excellent Raspberry Pi case to remove the VESA mounting tabs and replace with a DIN rail mount.

The enclosure mounts the Raspberry Pi on the rail vertically, sideways on, with SD card slot facing up and connectors facing down and to the front. It therefore takes much less space on the rail than other cases that mount the Pi  flat against the rail.

The case clips on the rail in the usual way. Hang it over the top of the rail then rotate downwards and it will clip over the bottom. A front-accessible notch at the bottom allows releasing the spring clip in the usual way with a fingernail or screwdriver.

The case was based on the Pi 3 bottom and universal Pi 2/3 top, so it should suit Pi 3 or Pi 2. The pictures show it with a Pi 3. Like the original, the board is secured into the enclosure with M3 x 6mm machine screws, and the two halves of the enclosure are secured together with M3 x 16mm machine screws. The screws self-tap into the plastic. Other similar size screws would likely work fine.

The small gaps between the parts of the spring clip are all at least 0.4mm, so this should be printable on most machines. The example in the photos was printed on a Prusa i3 Mk2 using Boots Industries orange PLA.

Downloadable STEP files are included if you want to make modifications.