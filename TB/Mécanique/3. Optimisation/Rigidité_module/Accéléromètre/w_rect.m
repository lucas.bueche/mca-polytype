function [X] = w_rect(x)
%w_rect Creates the spectrum analysis of a vector using the FFT function.
%The temporal values are multiplied with a rectangular weighting window.
%   x = input signal
%   X = calculated spectre and shifted to center the DC component.

N = length(x);

% create rectangular window
window_RECT = zeros(size(x));
for n=1:N 
    window_RECT(n)=1;
end
% Value to normalize the spectre
Norm_Rect    = sum(window_RECT);

% FFT of signal
X_d = fft(window_RECT .* x);
% Center the DC component
X_d = fftshift(X_d);
% Normalize the spectre
X_d = X_d/Norm_Rect;

X = X_d;
end

