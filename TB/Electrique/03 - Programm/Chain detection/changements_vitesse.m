%%
clear all;
%%
Val = [
    224
238
259
271
272
272
269
271
274
268
272
271
272
269
271
274
272
271
273
273
275
240
227
209
194
176
176
166
158
153
146
147
141
139
135
131
134
133
133
135
135
132
134
134
130
136
131
134
133
134
134
132
133
134
126
87
83
85
85
85
87
87
88
89
87
88
88
88
89
89
86
90
88
87
90
87
88
88
88
89
87
87
90
87
88
88
89
88
88
88
88
86
86
84
82
76
77
77
76
73
76
75
74
73
72
73
71
71
71
69
70
69
68
70
68
67
67
67
64
67
65
66
66
66
66
65
65
66
66
66
65
66
66
65
67
66
65
66
65
65
66
65
67
66
65
66
65
67
67
66
68
68
68
70
69
69
71
71
71
72
73
72
74
74
75
75
76
78
78
75
78
79
80
77
82
82
82
83
82
85
85
86
87
87
88
89
88
90
89
87
89
88
87
89
87
88
89
88
89
87
90
92
92
94
96
96
99
98
103
102
103
115
120
124
125
129
133
135
133
135
132
133
135
132
134
139
142
141
136
132
136
131
131
133
134
133
135
134
131
133
135
136
135
129
138
249
269
265
274
274
271
];

figure(2)
stem(Val)

%% Mean Val 
Synchronised_Mean_val = zeros(241,1);

Chain_STABLE = zeros(241,1);

n=11;
w=11;
while n<251
    x=Val(n)+Val(n-1)+Val(n-2)+Val(n-3)+Val(n-4)+Val(n-5)+Val(n-6)+Val(n-7)+Val(n-8)+Val(n-9)+Val(n-10);
    Synchronised_Mean_val(w) = x/11;
    
    
    
    Allowed_delta = 5;
    Delta_val = Allowed_delta * Synchronised_Mean_val(w)/100;
    
    if abs(Val(n)-Synchronised_Mean_val(w))>Delta_val
      Chain_STABLE(w)= 0; 
    elseif abs(Val(n-1)-Synchronised_Mean_val(w))>Delta_val
         Chain_STABLE(w)= 0;
    elseif abs(Val(n-2)-Synchronised_Mean_val(w))>Delta_val
         Chain_STABLE(w)= 0;
    elseif abs(Val(n-3)-Synchronised_Mean_val(w))>Delta_val
        Chain_STABLE(w)= 0;
    elseif abs(Val(n-4)-Synchronised_Mean_val(w))>Delta_val
        Chain_STABLE(w)= 0;
    elseif abs(Val(n-5)-Synchronised_Mean_val(w))>Delta_val
        Chain_STABLE(w)= 0;
    elseif abs(Val(n-6)-Synchronised_Mean_val(w))>Delta_val
       Chain_STABLE(w)= 0;
    elseif abs(Val(n-7)-Synchronised_Mean_val(w))>Delta_val
         Chain_STABLE(w)= 0;
    elseif abs(Val(n-8) -Synchronised_Mean_val(w))>Delta_val
     Chain_STABLE(w)= 0;
    elseif abs(Val(n-9)-Synchronised_Mean_val(w))>Delta_val
         Chain_STABLE(w)= 0;
    elseif abs(Val(n-10)-Synchronised_Mean_val(w))>Delta_val
         Chain_STABLE(w)= 0;
    else 
        
        Chain_STABLE(w)= Synchronised_Mean_val(w)+10; 
    end
    
    n=n+1;
    w=w+1;
    
    
end

hold on 

plot(Synchronised_Mean_val);

hold on 

plot(Chain_STABLE);
% %% CHAIN STABLE 
% Allowed_delta = 5; % IN %
% 
% 
% Chain_STABLE = zeros(241,1);
% n=11;
% w=11;
% while n<251
%     
%     Delta_val = Allowed_delta * Synchronised_Mean_val(n)/100;
%     
%     z=0;
%     Max_val=0;
%     Min_val=0;
%     
%     while z<11
%       if Val(n-z) < Min_val 
%         Min_val = Val(n-z);
%       elseif Val(n-z) > Max_val 
%           Max_val = Val(n-z);
%       end
%       z=z+1;       
%     end
%     
%     if (Max_val - Synchronised_Mean_val(n)) > Delta_val 
%           Chain_STABLE(n)=0
%     elseif ( Synchronised_Mean_val(n)-Min_val)< Delta_val
%         Chain_STABLE(n)=0
%     else
%         Chain_STABLE(n)=(Synchronised_Mean_val(n)+10)
%     end 
% %     x=Val(n)+Val(n-1)+Val(n-2)+Val(n-3)+Val(n-4)+Val(n-5)+Val(n-6)+Val(n-7)+Val(n-8)+Val(n-9)+Val(n-10);
% %     Chain_STABLE(w) = x/11;
% %     n=n+1;
% %     w=w+1;
%     
%     
% end
% 
% hold on 
% 
% plot(Chain_STABLE);


%%
%     Allowed_delta = 5;
%     Synchronised_Mean_val = zeros(241);
%     
%     
%     
%     Delta_val = Allowed_delta * Synchronised_Mean_val/100;
%     %// check if values in table are to much separated
%     z := 0; 
%     x := table[z]; // smallest val
%     y := table[z]; // biggest val 
% 
% 
%     WHILE(z<11) DO
% 
%         IF table[z] < x THEN
%         x := table[z];
%         END_IF;
% 
%         IF table[z] > y THEN
%         y := table[z];
%         END_IF;
% 
%         z := z+1;
%     END_WHILE;
% 
%     IF (y-Synchronised_Mean_val) >  Delta_val THEN 
%         DELTA := TRUE;	
% 
%     ELSIF (Synchronised_Mean_val-x) >  Delta_val THEN 
%         DELTA := TRUE;	
% 
%     ELSE 	
%         DELTA := FALSE;
%     END_IF;