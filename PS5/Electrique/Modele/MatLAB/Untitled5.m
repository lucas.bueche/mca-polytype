clc          % clear the command window

clear all    % clear the workspace

close all    % close all figure window

 

J = 3.2284E-6;         % moment of inertia of the rotor

b = 3.5077E-6;         % motor viscous friction constant

K = 0.0274;            % electromotive force constant

R = 4;                 % electric resistance

L = 2.75E-6;           % electric inductance

s = tf('s');

P_motor_tf = K/(s*((J*s+b)*(L*s+R)+K^2))   % transfer function

A = [0 1 0;0 -b/J K/J;0 -K/L -R/L];

B = [0 ; 0 ; 1/L];

C = [1 0 0];

D = [0];

motor_ss = ss(A,B,C,D)

%%% motor_ss = ss(P_motor);

t = 0:0.001:0.2;

figure,bode(P_motor_tf)

grid

figure,step(P_motor_tf,t)

grid

isstable(P_motor_tf)

pole(P_motor_tf)

%  Identify possible feedback data here feedback =1

sys_cl = feedback(P_motor_tf,1)

 

% if you want to only one plot then no need figure command but

% you want to more then one figure then need figure command for open the new gifure window

figure,step(sys_cl,t)

grid

figure,pzmap(sys_cl)

grid

damp(sys_cl)

[Wn,zeta,poles] = damp(sys_cl);

Mp = exp((-zeta(1)*pi)/sqrt(1-zeta(1)^2))

Ts = 4/(zeta(1)*Wn(1))