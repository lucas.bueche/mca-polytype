%% Définition des variables

% Import 
A = DonneestechniquechargementGES1;
V_chaine = A(1,2);
T_insertion = A(5,2);
T_cycle=A(10,2);

%% Valeur pour controleur PI 
% 
% (J)     moment of inertia of the rotor     3.2284E-6 kg.m^2
% (b)     motor viscous friction constant    3.5077E-6 Nms
% (Ke)    electromotive force constant       0.0274 V/rad/sec
% (Kt)    motor torque constant              0.0274 Nm/Amp
% (R)     electric resistance                4 ohm
% (L)     electric inductance                2.75E-6H

J = 3.2284E-6;
b = 3.5077E-6;
Ke = 0.0274;
Kt = 0.0274;
R = 4;
L = 2.75E-6;

IN = 5;


%% Valeur pour Moteur 1Fk7022
% 
% (J)     moment of inertia of the rotor     3.2284E-6 kg.m^2
% (b)     motor viscous friction constant    3.5077E-6 Nms
% (Ke)    electromotive force constant       0.0274 V/rad/sec
% (Kt)    motor torque constant              0.0274 Nm/Amp
% (R)     electric resistance                4 ohm
% (L)     electric inductance                2.75E-6H

J = 0.028e-3;
b = E-6;
Ke = 0.0274;
Kt = 0.0274;
R = 4;
L = 2.75E-6;

IN = 5;
