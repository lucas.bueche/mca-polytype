#
# Simcenter Amesim system make file
#

# This variable can be used in -L statements
# or otherwise to separate machine dependent code
# The legal values are : sun, ibm, hp, sgi, lnx, win32

# This makefile has been created using the following cathegory path list
#	$AME
#	$AME/libsig
#	$AME/libmec
#	$AME/libhydr
#	$AME/libhcd
#	$AME/libhr
#	$AME/libpn
#	$AME/libpcd
#	$AME/libth
#	$AME/libthh
#	$AME/libthcd
#	$AME/libtr
#	$AME/libfi
#	$AME/libcs
#	$AME/libem
#	$AME/libtpf
#	$AME/libac
#	$AME/libemd
#	$AME/libdrv
#	$AME/libeng
#	$AME/libexh
#	$AME/libplm
#	$AME/libheat
#	$AME/libdv
#	$AME/libicar
#	$AME/libma
#	$AME/libgm
#	$AME/libcosim
#	$AME/libesc
#	$AME/libesg
#	$AME/libec
#	$AME/libfc
#	$AME/libae
#	$AME/libcf
#	$AME/libcfd1d
#	$AME/libaero
#	$AME/libace
#	$AME/libacf
#	$AME/libess
#	$AME/libeb
#	$AME/libm6dof
#	$AME/liblp
#	$AME/libgte
#	$AME/libmotion
# End category path list

MACHINETYPE = win64-gcc

# Then the object files
OBJECTS = \
	c:/amesim/v2019_1/amesim/libemd/submodels/win64-gcc/EMDPMDC01.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/W000.o \
	c:/amesim/v2019_1/amesim/libth/submodels/win64-gcc/THTC0.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/CONS00.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECRL0A.o \
	c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EBVS01.o \
	c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EBZV01.o \
	c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EB3N03.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SIGVSAT0.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SGN10.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SPLT0.o \
	c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EBCT00.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/JUN3M.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/PID001.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECADS1B.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/RACK01A.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECRN10A.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/SD0000A.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECMAS21.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECDS0A.o \
	c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/F000.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SSINK.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/GA00.o \
	c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/UD00.o

OBJECTS2 = \
	"c:/amesim/v2019_1/amesim/libemd/submodels/win64-gcc/EMDPMDC01.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/W000.o" \
	"c:/amesim/v2019_1/amesim/libth/submodels/win64-gcc/THTC0.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/CONS00.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECRL0A.o" \
	"c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EBVS01.o" \
	"c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EBZV01.o" \
	"c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EB3N03.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SIGVSAT0.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SGN10.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SPLT0.o" \
	"c:/amesim/v2019_1/amesim/libeb/submodels/win64-gcc/EBCT00.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/JUN3M.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/PID001.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECADS1B.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/RACK01A.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECRN10A.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/SD0000A.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECMAS21.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/MECDS0A.o" \
	"c:/amesim/v2019_1/amesim/libmec/submodels/win64-gcc/F000.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/SSINK.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/GA00.o" \
	"c:/amesim/v2019_1/amesim/libsig/submodels/win64-gcc/UD00.o"

SIMOTICS_1_Test_.dll: $(OBJECTS) SIMOTICS_1_Test_.o
	@echo SIMOTICS_1_Test_.make.link_args =
	@type SIMOTICS_1_Test_.make.link_args
	"$(AME)\interfaces\standalonesimulator\win32\stdsim_dynlink" win64 $(CC) -m64 $(LDFLAGS) -o SIMOTICS_1_Test_.dll SIMOTICS_1_Test_.o @"SIMOTICS_1_Test_.make.link_args" $(AMELIBS)

SIMOTICS_1_Test_.o: SIMOTICS_1_Test_.c
	"$(AME)\interfaces\user_cosim\win32\ame_user_cosim_dyncompile" $(CC) -m64 -c -I"$(AME)\interfaces\user_cosim" -I"$(AME)\interfaces\standalonesimulator" -I"$(AME)\interfaces" $(CFLAGS) -o SIMOTICS_1_Test_.o SIMOTICS_1_Test_.c

.c.o:
	@echo
	@echo "Warning: \"$<\" is newer than the object."
	@echo ""

.f.o:
	@echo
	@echo "Warning: \"$<\" is newer than the object."
	@echo ""

# End of file

