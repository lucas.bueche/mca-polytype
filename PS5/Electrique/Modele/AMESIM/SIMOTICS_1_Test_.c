/************** -*-C-*- *********************************************
 * $Id: ame_model.ctemp 78997 2019-02-01 09:12:42Z jandre $
 *                                                                  *
 *       AMESim C code for cosimulation written by code generator.  *
 
                                 SIMOTICS_1_Test_
 *
 *******************************************************************/

#include "SIMOTICS_1_Test_.h"
#include "ame_interfaces.h"

#include <assert.h>
#include <stdio.h>

#if !defined(AME_DS1005) && !defined(AME_DS1006)
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#endif

#ifndef AME_DS1006
#include <fcntl.h>
#endif

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <setjmp.h>

#ifdef _WIN32
#ifndef WIN32
#define WIN32
#endif
#endif

#include "ameutils.h"
#include "codegen_ameutils_public.h"

#if defined (WIN32) || defined(XPCMSVISUALC)
#include <io.h>
#elif !defined(AME_DS1005) && !defined(AME_DS1006)
#include <unistd.h>
#endif

#if defined(_WINDOWS) || defined(_WIN32) || defined(WIN32)
#define DLLEXPORTDEF __declspec(dllexport)
#else
#define DLLEXPORTDEF
#endif

#if defined (RTLAB) || defined(AME_DS1005) || defined(AME_DS1006) || defined(LABCAR) || defined(AME_ADX) || defined(AME_HWA) || defined(RT) ||defined(AMEVERISTAND)
#ifdef DLLEXPORTDEF
#undef DLLEXPORTDEF
#endif
#define DLLEXPORTDEF
#endif

/* If we are in Simulink we should set things up for a AMEMULTIDLL (only static symbols) */
#if defined(AME_CS_SIMULINK) || defined(AME_ME_SIMULINK)
#ifndef AMEMULTIDLL
#define AMEMULTIDLL
#endif
#endif

/* If we are in Veristand we should set things up for a AMEMULTIDLL (only static symbols) */
#ifdef AMEVERISTAND
#ifndef AMEMULTIDLL
#define AMEMULTIDLL
#endif
#endif

/* avoid globally exported functions - required for exporting several models in one executable */
#if defined AMEMULTIDLL
#define DLLEXPORTDEF_OR_STATIC static
#else
#define DLLEXPORTDEF_OR_STATIC DLLEXPORTDEF
#endif

#if defined CREATING_STATIC_LIB
#define DLLEXPORTDEF_OR_EXTERN extern 
#else
#define DLLEXPORTDEF_OR_EXTERN DLLEXPORTDEF
#endif

#define TIME_ROUNDOFF 1.0e-10
#define TLAST_MIN     -1E30

#if defined(FMICS1) || defined(FMICS2) || defined(FMIME1) || defined(FMIME2)
#ifdef FMIX
#undef FMIX
#endif
#define FMI
#define MODEL_IDENTIFIER         SIMOTICS_1_Test_
#define MODEL_IDENTIFIER_STRING "SIMOTICS_1_Test_"
#endif

/* ***************************************************************** 

   code inserted by AMESim
   
   *****************************************************************/

#define AMEVERSION                  20190100
#define SUB_LENGTH                  63

static const char soldToId[] = "5305435";

/**** Model structure definition ****/
/* Structural definition of the model */
#define AME_MODEL_ISEXPLICIT        1

/* Number of explicit declared states  */
#define AME_NBOF_EXPLICIT_STATE     24

/* Number of implicit declared states  */
#define AME_NBOF_IMPLICIT_STATE_DECLARED  0
#define AME_NBOF_IMPLICIT_STATE_GENERATED 0
#define AME_NBOF_IMPLICIT_STATE           0

/* Number of discrete declared states  */
#define AME_NBOF_DISCRETE_STATE     75

/* Total number of states manipulated by the Amesim solver including generated states */
#define AME_NBOF_SOLVER_STATES      24

#define AME_NBOF_PARAMS             498
#define AME_NBOF_REAL_PARAMS        312
#define AME_NBOF_INT_PARAMS         69
#define AME_NBOF_TEXT_PARAMS        3
#define AME_NBOF_CSTATE_PARAMS      24
#define AME_NBOF_DSTATE_PARAMS      75
#define AME_NBOF_FIXED_VAR_PARAMS   15

#define AME_NBOF_VARS               405
#define AME_NBOF_INPUTS             0
#define AME_NBOF_OUTPUTS            0

#define AME_NBOF_REAL_STORES        54
#define AME_NBOF_INT_STORES         120
#define AME_NBOF_POINTER_STORES     0

#define AME_HAS_ENABLED_SUBMODEL    0

#define AME_SIZEOF_DBWORK_ARRAY     0

#define AME_NB_VAR_INFO            405

static const S_AMEModelDef GmodelDef = {
   AME_MODEL_ISEXPLICIT,
   AME_NBOF_EXPLICIT_STATE,
   AME_NBOF_IMPLICIT_STATE_DECLARED,
   AME_NBOF_IMPLICIT_STATE_GENERATED,
   AME_NBOF_DISCRETE_STATE,
   AME_NBOF_VARS,
   AME_NBOF_PARAMS,
   AME_NBOF_REAL_PARAMS,
   AME_NBOF_INT_PARAMS,
   AME_NBOF_TEXT_PARAMS,
   AME_NBOF_CSTATE_PARAMS,
   AME_NBOF_DSTATE_PARAMS,
   AME_NBOF_FIXED_VAR_PARAMS,
   AME_NBOF_INPUTS,
   AME_NBOF_OUTPUTS,
   AME_NBOF_REAL_STORES,
   AME_NBOF_INT_STORES,
   AME_NBOF_POINTER_STORES,
   AME_SIZEOF_DBWORK_ARRAY,
   AME_NB_VAR_INFO
};


static const char **GinputVarTitles = NULL;
static const char **GoutputVarTitles = NULL;

/**** Parameters structure definition ****/

#define NB_SUBMODELNAME       60
static const char* GsubmodelNameArray[NB_SUBMODELNAME] = {
     "EMDPMDC01 instance 1"
   , "W000 instance 1"
   , "CONS00 instance 1"
   , "MECRL0A instance 1"
   , "EBZV01 instance 1"
   , "CONS00 instance 2"
   , "EBCT00 instance 1"
   , "PID001 instance 1"
   , "CONS00 instance 3"
   , "MECADS1B instance 1"
   , "RACK01A instance 1"
   , "MECRN10A instance 1"
   , "SD0000A instance 1"
   , "MECMAS21 instance 1"
   , "MECDS0A instance 1"
   , "F000 instance 1"
   , "PID001 instance 2"
   , "GA00 instance 1"
   , "GA00 instance 2"
   , "UD00 instance 1"
   , "EMDPMDC01 instance 2"
   , "W000 instance 2"
   , "CONS00 instance 4"
   , "MECRL0A instance 2"
   , "EBZV01 instance 2"
   , "CONS00 instance 5"
   , "EBCT00 instance 2"
   , "PID001 instance 3"
   , "CONS00 instance 6"
   , "MECADS1B instance 2"
   , "RACK01A instance 2"
   , "MECRN10A instance 2"
   , "SD0000A instance 2"
   , "MECMAS21 instance 2"
   , "MECDS0A instance 2"
   , "F000 instance 2"
   , "PID001 instance 4"
   , "GA00 instance 3"
   , "GA00 instance 4"
   , "UD00 instance 2"
   , "EMDPMDC01 instance 3"
   , "W000 instance 3"
   , "CONS00 instance 7"
   , "MECRL0A instance 3"
   , "EBZV01 instance 3"
   , "CONS00 instance 8"
   , "EBCT00 instance 3"
   , "PID001 instance 5"
   , "CONS00 instance 9"
   , "MECADS1B instance 3"
   , "RACK01A instance 3"
   , "SD0000A instance 3"
   , "MECMAS21 instance 3"
   , "MECDS0A instance 3"
   , "F000 instance 3"
   , "PID001 instance 6"
   , "GA00 instance 5"
   , "GA00 instance 6"
   , "UD00 instance 3"
   , "MECRN10A instance 3"
};

static const S_AMEParamInfo GParamInfo[498] = {
     {E_Real_Param           , E_Expression_Param, 0, -1, 0, 0}
   , {E_Real_Param           , E_Expression_Param, 1, -1, 0, 0}
   , {E_Real_Param           , E_Expression_Param, 2, -1, 0, 0}
   , {E_Real_Param           , E_Expression_Param, 3, -1, 0, 0}
   , {E_Real_Param           , E_Expression_Param, 4, -1, 0, 0}
   , {E_Real_Param           , E_Expression_Param, 5, -1, 0, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 0, 13, 0, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 0, 9, 1, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 1, 10, 1, 0}
   , {E_Real_Param           , E_Expression_Param, 6, -1, 2, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 0, 22, 2, 0}
   , {E_Real_Param           , E_Expression_Param, 7, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 8, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 9, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 10, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 11, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 12, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 13, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 14, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 15, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 16, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 17, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 18, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 19, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 20, -1, 3, 0}
   , {E_Real_Param           , E_Expression_Param, 21, -1, 3, 0}
   , {E_Int_Param            , E_Expression_Param, 0, -1, 3, 0}
   , {E_Int_Param            , E_Expression_Param, 1, -1, 3, 0}
   , {E_Int_Param            , E_Expression_Param, 2, -1, 3, 0}
   , {E_Int_Param            , E_Expression_Param, 3, -1, 3, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 1, 24, 3, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 2, 25, 3, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 2, 45, 4, 0}
   , {E_Real_Param           , E_Expression_Param, 22, -1, 5, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 1, 50, 5, 0}
   , {E_Real_Param           , E_Expression_Param, 23, -1, 6, 0}
   , {E_Real_Param           , E_Expression_Param, 24, -1, 6, 0}
   , {E_Int_Param            , E_Expression_Param, 4, -1, 6, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 3, 53, 6, 0}
   , {E_Real_Param           , E_Expression_Param, 25, -1, 7, 1}
   , {E_Real_Param           , E_Expression_Param, 26, -1, 7, 1}
   , {E_Real_Param           , E_Expression_Param, 27, -1, 7, 1}
   , {E_Real_Param           , E_Expression_Param, 28, -1, 7, 1}
   , {E_Real_Param           , E_Expression_Param, 29, -1, 7, 1}
   , {E_Real_Param           , E_Expression_Param, 30, -1, 7, 1}
   , {E_Real_Param           , E_Expression_Param, 31, -1, 7, 1}
   , {E_Real_Param           , E_Expression_Param, 32, -1, 7, 1}
   , {E_Int_Param            , E_Expression_Param, 5, -1, 7, 0}
   , {E_Int_Param            , E_Expression_Param, 6, -1, 7, 1}
   , {E_Int_Param            , E_Expression_Param, 7, -1, 7, 1}
   , {E_ContinuousState_Param, E_Expression_Param, 3, 57, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 2, 59, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 3, 60, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 4, 61, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 5, 62, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 6, 63, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 7, 64, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 8, 65, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 9, 66, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 10, 67, 7, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 11, 68, 7, 0}
   , {E_Real_Param           , E_Expression_Param, 33, -1, 8, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 12, 70, 8, 0}
   , {E_Real_Param           , E_Expression_Param, 34, -1, 9, 0}
   , {E_Real_Param           , E_Expression_Param, 35, -1, 9, 0}
   , {E_Int_Param            , E_Expression_Param, 8, -1, 9, 0}
   , {E_Real_Param           , E_Expression_Param, 36, -1, 10, 0}
   , {E_Real_Param           , E_Expression_Param, 37, -1, 10, 0}
   , {E_Real_Param           , E_Expression_Param, 38, -1, 11, 0}
   , {E_Real_Param           , E_Expression_Param, 39, -1, 11, 0}
   , {E_Real_Param           , E_Expression_Param, 40, -1, 11, 0}
   , {E_Real_Param           , E_Expression_Param, 41, -1, 11, 0}
   , {E_Int_Param            , E_Expression_Param, 9, -1, 11, 0}
   , {E_Int_Param            , E_Expression_Param, 10, -1, 11, 0}
   , {E_Text_Param           , E_Expression_Param, 0, -1, 11, 0}
   , {E_Real_Param           , E_Expression_Param, 42, -1, 12, 0}
   , {E_Real_Param           , E_Expression_Param, 43, -1, 12, 0}
   , {E_Real_Param           , E_Expression_Param, 44, -1, 12, 0}
   , {E_Real_Param           , E_Expression_Param, 45, -1, 12, 0}
   , {E_Real_Param           , E_Expression_Param, 46, -1, 12, 0}
   , {E_Real_Param           , E_Expression_Param, 47, -1, 12, 0}
   , {E_Real_Param           , E_Expression_Param, 48, -1, 12, 0}
   , {E_Int_Param            , E_Expression_Param, 11, -1, 12, 0}
   , {E_Real_Param           , E_Expression_Param, 49, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 50, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 51, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 52, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 53, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 54, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 55, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 56, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 57, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 58, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 59, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 60, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 61, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 62, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 63, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 64, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 65, -1, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 66, -1, 13, 0}
   , {E_Int_Param            , E_Expression_Param, 12, -1, 13, 0}
   , {E_Int_Param            , E_Expression_Param, 13, -1, 13, 0}
   , {E_Int_Param            , E_Expression_Param, 14, -1, 13, 0}
   , {E_Int_Param            , E_Expression_Param, 15, -1, 13, 0}
   , {E_Int_Param            , E_Expression_Param, 16, -1, 13, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 4, 97, 13, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 5, 98, 13, 0}
   , {E_Real_Param           , E_Expression_Param, 67, -1, 14, 0}
   , {E_Real_Param           , E_Expression_Param, 68, -1, 14, 0}
   , {E_Int_Param            , E_Expression_Param, 17, -1, 14, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 4, 110, 15, 0}
   , {E_Real_Param           , E_Expression_Param, 69, -1, 16, 1}
   , {E_Real_Param           , E_Expression_Param, 70, -1, 16, 1}
   , {E_Real_Param           , E_Expression_Param, 71, -1, 16, 1}
   , {E_Real_Param           , E_Expression_Param, 72, -1, 16, 1}
   , {E_Real_Param           , E_Expression_Param, 73, -1, 16, 1}
   , {E_Real_Param           , E_Expression_Param, 74, -1, 16, 1}
   , {E_Real_Param           , E_Expression_Param, 75, -1, 16, 1}
   , {E_Real_Param           , E_Expression_Param, 76, -1, 16, 1}
   , {E_Int_Param            , E_Expression_Param, 18, -1, 16, 0}
   , {E_Int_Param            , E_Expression_Param, 19, -1, 16, 1}
   , {E_Int_Param            , E_Expression_Param, 20, -1, 16, 1}
   , {E_ContinuousState_Param, E_Expression_Param, 6, 116, 16, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 7, 117, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 13, 120, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 14, 121, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 15, 122, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 16, 123, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 17, 124, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 18, 125, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 19, 126, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 20, 127, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 21, 128, 16, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 22, 129, 16, 0}
   , {E_Real_Param           , E_Expression_Param, 77, -1, 17, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 23, 133, 17, 0}
   , {E_Real_Param           , E_Expression_Param, 78, -1, 18, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 24, 134, 18, 0}
   , {E_Real_Param           , E_Expression_Param, 79, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 80, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 81, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 82, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 83, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 84, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 85, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 86, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 87, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 88, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 89, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 90, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 91, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 92, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 93, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 94, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 95, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 96, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 97, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 98, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 99, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 100, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 101, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 102, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 103, -1, 19, 0}
   , {E_Int_Param            , E_Expression_Param, 21, -1, 19, 0}
   , {E_Int_Param            , E_Expression_Param, 22, -1, 19, 0}
   , {E_Real_Param           , E_Expression_Param, 104, -1, 20, 0}
   , {E_Real_Param           , E_Expression_Param, 105, -1, 20, 0}
   , {E_Real_Param           , E_Expression_Param, 106, -1, 20, 0}
   , {E_Real_Param           , E_Expression_Param, 107, -1, 20, 0}
   , {E_Real_Param           , E_Expression_Param, 108, -1, 20, 0}
   , {E_Real_Param           , E_Expression_Param, 109, -1, 20, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 8, 148, 20, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 5, 144, 21, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 6, 145, 21, 0}
   , {E_Real_Param           , E_Expression_Param, 110, -1, 22, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 25, 157, 22, 0}
   , {E_Real_Param           , E_Expression_Param, 111, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 112, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 113, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 114, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 115, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 116, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 117, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 118, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 119, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 120, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 121, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 122, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 123, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 124, -1, 23, 0}
   , {E_Real_Param           , E_Expression_Param, 125, -1, 23, 0}
   , {E_Int_Param            , E_Expression_Param, 23, -1, 23, 0}
   , {E_Int_Param            , E_Expression_Param, 24, -1, 23, 0}
   , {E_Int_Param            , E_Expression_Param, 25, -1, 23, 0}
   , {E_Int_Param            , E_Expression_Param, 26, -1, 23, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 9, 159, 23, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 10, 160, 23, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 7, 180, 24, 0}
   , {E_Real_Param           , E_Expression_Param, 126, -1, 25, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 26, 185, 25, 0}
   , {E_Real_Param           , E_Expression_Param, 127, -1, 26, 0}
   , {E_Real_Param           , E_Expression_Param, 128, -1, 26, 0}
   , {E_Int_Param            , E_Expression_Param, 27, -1, 26, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 8, 188, 26, 0}
   , {E_Real_Param           , E_Expression_Param, 129, -1, 27, 1}
   , {E_Real_Param           , E_Expression_Param, 130, -1, 27, 1}
   , {E_Real_Param           , E_Expression_Param, 131, -1, 27, 1}
   , {E_Real_Param           , E_Expression_Param, 132, -1, 27, 1}
   , {E_Real_Param           , E_Expression_Param, 133, -1, 27, 1}
   , {E_Real_Param           , E_Expression_Param, 134, -1, 27, 1}
   , {E_Real_Param           , E_Expression_Param, 135, -1, 27, 1}
   , {E_Real_Param           , E_Expression_Param, 136, -1, 27, 1}
   , {E_Int_Param            , E_Expression_Param, 28, -1, 27, 0}
   , {E_Int_Param            , E_Expression_Param, 29, -1, 27, 1}
   , {E_Int_Param            , E_Expression_Param, 30, -1, 27, 1}
   , {E_ContinuousState_Param, E_Expression_Param, 11, 192, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 27, 194, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 28, 195, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 29, 196, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 30, 197, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 31, 198, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 32, 199, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 33, 200, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 34, 201, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 35, 202, 27, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 36, 203, 27, 0}
   , {E_Real_Param           , E_Expression_Param, 137, -1, 28, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 37, 205, 28, 0}
   , {E_Real_Param           , E_Expression_Param, 138, -1, 29, 0}
   , {E_Real_Param           , E_Expression_Param, 139, -1, 29, 0}
   , {E_Int_Param            , E_Expression_Param, 31, -1, 29, 0}
   , {E_Real_Param           , E_Expression_Param, 140, -1, 30, 0}
   , {E_Real_Param           , E_Expression_Param, 141, -1, 30, 0}
   , {E_Real_Param           , E_Expression_Param, 142, -1, 31, 0}
   , {E_Real_Param           , E_Expression_Param, 143, -1, 31, 0}
   , {E_Real_Param           , E_Expression_Param, 144, -1, 31, 0}
   , {E_Real_Param           , E_Expression_Param, 145, -1, 31, 0}
   , {E_Int_Param            , E_Expression_Param, 32, -1, 31, 0}
   , {E_Int_Param            , E_Expression_Param, 33, -1, 31, 0}
   , {E_Text_Param           , E_Expression_Param, 1, -1, 31, 0}
   , {E_Real_Param           , E_Expression_Param, 146, -1, 32, 0}
   , {E_Real_Param           , E_Expression_Param, 147, -1, 32, 0}
   , {E_Real_Param           , E_Expression_Param, 148, -1, 32, 0}
   , {E_Real_Param           , E_Expression_Param, 149, -1, 32, 0}
   , {E_Real_Param           , E_Expression_Param, 150, -1, 32, 0}
   , {E_Real_Param           , E_Expression_Param, 151, -1, 32, 0}
   , {E_Real_Param           , E_Expression_Param, 152, -1, 32, 0}
   , {E_Int_Param            , E_Expression_Param, 34, -1, 32, 0}
   , {E_Real_Param           , E_Expression_Param, 153, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 154, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 155, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 156, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 157, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 158, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 159, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 160, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 161, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 162, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 163, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 164, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 165, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 166, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 167, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 168, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 169, -1, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 170, -1, 33, 0}
   , {E_Int_Param            , E_Expression_Param, 35, -1, 33, 0}
   , {E_Int_Param            , E_Expression_Param, 36, -1, 33, 0}
   , {E_Int_Param            , E_Expression_Param, 37, -1, 33, 0}
   , {E_Int_Param            , E_Expression_Param, 38, -1, 33, 0}
   , {E_Int_Param            , E_Expression_Param, 39, -1, 33, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 12, 232, 33, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 13, 233, 33, 0}
   , {E_Real_Param           , E_Expression_Param, 171, -1, 34, 0}
   , {E_Real_Param           , E_Expression_Param, 172, -1, 34, 0}
   , {E_Int_Param            , E_Expression_Param, 40, -1, 34, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 9, 245, 35, 0}
   , {E_Real_Param           , E_Expression_Param, 173, -1, 36, 1}
   , {E_Real_Param           , E_Expression_Param, 174, -1, 36, 1}
   , {E_Real_Param           , E_Expression_Param, 175, -1, 36, 1}
   , {E_Real_Param           , E_Expression_Param, 176, -1, 36, 1}
   , {E_Real_Param           , E_Expression_Param, 177, -1, 36, 1}
   , {E_Real_Param           , E_Expression_Param, 178, -1, 36, 1}
   , {E_Real_Param           , E_Expression_Param, 179, -1, 36, 1}
   , {E_Real_Param           , E_Expression_Param, 180, -1, 36, 1}
   , {E_Int_Param            , E_Expression_Param, 41, -1, 36, 0}
   , {E_Int_Param            , E_Expression_Param, 42, -1, 36, 1}
   , {E_Int_Param            , E_Expression_Param, 43, -1, 36, 1}
   , {E_ContinuousState_Param, E_Expression_Param, 14, 251, 36, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 15, 252, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 38, 255, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 39, 256, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 40, 257, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 41, 258, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 42, 259, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 43, 260, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 44, 261, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 45, 262, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 46, 263, 36, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 47, 264, 36, 0}
   , {E_Real_Param           , E_Expression_Param, 181, -1, 37, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 48, 268, 37, 0}
   , {E_Real_Param           , E_Expression_Param, 182, -1, 38, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 49, 269, 38, 0}
   , {E_Real_Param           , E_Expression_Param, 183, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 184, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 185, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 186, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 187, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 188, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 189, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 190, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 191, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 192, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 193, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 194, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 195, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 196, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 197, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 198, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 199, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 200, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 201, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 202, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 203, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 204, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 205, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 206, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 207, -1, 39, 0}
   , {E_Int_Param            , E_Expression_Param, 44, -1, 39, 0}
   , {E_Int_Param            , E_Expression_Param, 45, -1, 39, 0}
   , {E_Real_Param           , E_Expression_Param, 208, -1, 40, 0}
   , {E_Real_Param           , E_Expression_Param, 209, -1, 40, 0}
   , {E_Real_Param           , E_Expression_Param, 210, -1, 40, 0}
   , {E_Real_Param           , E_Expression_Param, 211, -1, 40, 0}
   , {E_Real_Param           , E_Expression_Param, 212, -1, 40, 0}
   , {E_Real_Param           , E_Expression_Param, 213, -1, 40, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 16, 283, 40, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 10, 279, 41, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 11, 280, 41, 0}
   , {E_Real_Param           , E_Expression_Param, 214, -1, 42, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 50, 292, 42, 0}
   , {E_Real_Param           , E_Expression_Param, 215, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 216, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 217, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 218, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 219, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 220, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 221, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 222, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 223, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 224, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 225, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 226, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 227, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 228, -1, 43, 0}
   , {E_Real_Param           , E_Expression_Param, 229, -1, 43, 0}
   , {E_Int_Param            , E_Expression_Param, 46, -1, 43, 0}
   , {E_Int_Param            , E_Expression_Param, 47, -1, 43, 0}
   , {E_Int_Param            , E_Expression_Param, 48, -1, 43, 0}
   , {E_Int_Param            , E_Expression_Param, 49, -1, 43, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 17, 294, 43, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 18, 295, 43, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 12, 315, 44, 0}
   , {E_Real_Param           , E_Expression_Param, 230, -1, 45, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 51, 320, 45, 0}
   , {E_Real_Param           , E_Expression_Param, 231, -1, 46, 0}
   , {E_Real_Param           , E_Expression_Param, 232, -1, 46, 0}
   , {E_Int_Param            , E_Expression_Param, 50, -1, 46, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 13, 323, 46, 0}
   , {E_Real_Param           , E_Expression_Param, 233, -1, 47, 1}
   , {E_Real_Param           , E_Expression_Param, 234, -1, 47, 1}
   , {E_Real_Param           , E_Expression_Param, 235, -1, 47, 1}
   , {E_Real_Param           , E_Expression_Param, 236, -1, 47, 1}
   , {E_Real_Param           , E_Expression_Param, 237, -1, 47, 1}
   , {E_Real_Param           , E_Expression_Param, 238, -1, 47, 1}
   , {E_Real_Param           , E_Expression_Param, 239, -1, 47, 1}
   , {E_Real_Param           , E_Expression_Param, 240, -1, 47, 1}
   , {E_Int_Param            , E_Expression_Param, 51, -1, 47, 0}
   , {E_Int_Param            , E_Expression_Param, 52, -1, 47, 1}
   , {E_Int_Param            , E_Expression_Param, 53, -1, 47, 1}
   , {E_ContinuousState_Param, E_Expression_Param, 19, 327, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 52, 329, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 53, 330, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 54, 331, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 55, 332, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 56, 333, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 57, 334, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 58, 335, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 59, 336, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 60, 337, 47, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 61, 338, 47, 0}
   , {E_Real_Param           , E_Expression_Param, 241, -1, 48, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 62, 340, 48, 0}
   , {E_Real_Param           , E_Expression_Param, 242, -1, 49, 0}
   , {E_Real_Param           , E_Expression_Param, 243, -1, 49, 0}
   , {E_Int_Param            , E_Expression_Param, 54, -1, 49, 0}
   , {E_Real_Param           , E_Expression_Param, 244, -1, 50, 0}
   , {E_Real_Param           , E_Expression_Param, 245, -1, 50, 0}
   , {E_Real_Param           , E_Expression_Param, 246, -1, 51, 0}
   , {E_Real_Param           , E_Expression_Param, 247, -1, 51, 0}
   , {E_Real_Param           , E_Expression_Param, 248, -1, 51, 0}
   , {E_Real_Param           , E_Expression_Param, 249, -1, 51, 0}
   , {E_Real_Param           , E_Expression_Param, 250, -1, 51, 0}
   , {E_Real_Param           , E_Expression_Param, 251, -1, 51, 0}
   , {E_Real_Param           , E_Expression_Param, 252, -1, 51, 0}
   , {E_Int_Param            , E_Expression_Param, 55, -1, 51, 0}
   , {E_Real_Param           , E_Expression_Param, 253, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 254, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 255, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 256, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 257, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 258, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 259, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 260, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 261, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 262, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 263, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 264, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 265, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 266, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 267, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 268, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 269, -1, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 270, -1, 52, 0}
   , {E_Int_Param            , E_Expression_Param, 56, -1, 52, 0}
   , {E_Int_Param            , E_Expression_Param, 57, -1, 52, 0}
   , {E_Int_Param            , E_Expression_Param, 58, -1, 52, 0}
   , {E_Int_Param            , E_Expression_Param, 59, -1, 52, 0}
   , {E_Int_Param            , E_Expression_Param, 60, -1, 52, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 20, 365, 52, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 21, 366, 52, 0}
   , {E_Real_Param           , E_Expression_Param, 271, -1, 53, 0}
   , {E_Real_Param           , E_Expression_Param, 272, -1, 53, 0}
   , {E_Int_Param            , E_Expression_Param, 61, -1, 53, 0}
   , {E_FixedVar_Param       , E_Expression_Param, 14, 378, 54, 0}
   , {E_Real_Param           , E_Expression_Param, 273, -1, 55, 1}
   , {E_Real_Param           , E_Expression_Param, 274, -1, 55, 1}
   , {E_Real_Param           , E_Expression_Param, 275, -1, 55, 1}
   , {E_Real_Param           , E_Expression_Param, 276, -1, 55, 1}
   , {E_Real_Param           , E_Expression_Param, 277, -1, 55, 1}
   , {E_Real_Param           , E_Expression_Param, 278, -1, 55, 1}
   , {E_Real_Param           , E_Expression_Param, 279, -1, 55, 1}
   , {E_Real_Param           , E_Expression_Param, 280, -1, 55, 1}
   , {E_Int_Param            , E_Expression_Param, 62, -1, 55, 0}
   , {E_Int_Param            , E_Expression_Param, 63, -1, 55, 1}
   , {E_Int_Param            , E_Expression_Param, 64, -1, 55, 1}
   , {E_ContinuousState_Param, E_Expression_Param, 22, 384, 55, 0}
   , {E_ContinuousState_Param, E_Expression_Param, 23, 385, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 63, 388, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 64, 389, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 65, 390, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 66, 391, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 67, 392, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 68, 393, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 69, 394, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 70, 395, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 71, 396, 55, 0}
   , {E_DiscreteState_Param  , E_Expression_Param, 72, 397, 55, 0}
   , {E_Real_Param           , E_Expression_Param, 281, -1, 56, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 73, 401, 56, 0}
   , {E_Real_Param           , E_Expression_Param, 282, -1, 57, 1}
   , {E_DiscreteState_Param  , E_Expression_Param, 74, 402, 57, 0}
   , {E_Real_Param           , E_Expression_Param, 283, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 284, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 285, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 286, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 287, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 288, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 289, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 290, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 291, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 292, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 293, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 294, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 295, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 296, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 297, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 298, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 299, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 300, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 301, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 302, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 303, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 304, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 305, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 306, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 307, -1, 58, 0}
   , {E_Int_Param            , E_Expression_Param, 65, -1, 58, 0}
   , {E_Int_Param            , E_Expression_Param, 66, -1, 58, 0}
   , {E_Real_Param           , E_Expression_Param, 308, -1, 59, 0}
   , {E_Real_Param           , E_Expression_Param, 309, -1, 59, 0}
   , {E_Real_Param           , E_Expression_Param, 310, -1, 59, 0}
   , {E_Real_Param           , E_Expression_Param, 311, -1, 59, 0}
   , {E_Int_Param            , E_Expression_Param, 67, -1, 59, 0}
   , {E_Int_Param            , E_Expression_Param, 68, -1, 59, 0}
   , {E_Text_Param           , E_Expression_Param, 2, -1, 59, 0}
};

static const int GcontStateVarNum[AME_NBOF_EXPLICIT_STATE] = {
     13, 24, 25, 57, 97, 98, 116, 117
   , 148, 159, 160, 192, 232, 233, 251, 252
   , 283, 294, 295, 327, 365, 366, 384, 385
};

static const int GdiscStateVarNum[AME_NBOF_DSTATE_PARAMS] = {
     22, 50, 59, 60, 61, 62, 63, 64
   , 65, 66, 67, 68, 70, 120, 121, 122
   , 123, 124, 125, 126, 127, 128, 129, 133
   , 134, 157, 185, 194, 195, 196, 197, 198
   , 199, 200, 201, 202, 203, 205, 255, 256
   , 257, 258, 259, 260, 261, 262, 263, 264
   , 268, 269, 292, 320, 329, 330, 331, 332
   , 333, 334, 335, 336, 337, 338, 340, 388
   , 389, 390, 391, 392, 393, 394, 395, 396
   , 397, 401, 402
};

static const int *GInputVarNum = NULL;

static const int *GOutputVarNum = NULL;

static const int GFixedVarNum[AME_NBOF_FIXED_VAR_PARAMS] = {
     9, 10, 45, 53, 110, 144, 145, 180
   , 188, 245, 279, 280, 315, 323, 378
};

#define IS0 (&amesys->pModel->intStoreArray[0])
#define RP0 (&amesys->pModel->realParamArray[0])
#define RP3 (&amesys->pModel->realParamArray[6])
#define RS4 (&amesys->pModel->realStoreArray[0])
#define IS4 (&amesys->pModel->intStoreArray[2])
#define RP4 (&amesys->pModel->realParamArray[7])
#define IP4 (&amesys->pModel->integerParamArray[0])
#define IS8 (&amesys->pModel->intStoreArray[12])
#define RP9 (&amesys->pModel->realParamArray[22])
#define RS12 (&amesys->pModel->realStoreArray[2])
#define RP12 (&amesys->pModel->realParamArray[23])
#define IP12 (&amesys->pModel->integerParamArray[4])
#define IS14 (&amesys->pModel->intStoreArray[13])
#define RP14 (&amesys->pModel->realParamArray[25])
#define IP14 (&amesys->pModel->integerParamArray[5])
#define RP15 (&amesys->pModel->realParamArray[33])
#define RS18 (&amesys->pModel->realStoreArray[3])
#define RP18 (&amesys->pModel->realParamArray[34])
#define IP18 (&amesys->pModel->integerParamArray[8])
#define RP19 (&amesys->pModel->realParamArray[36])
#define IS20 (&amesys->pModel->intStoreArray[15])
#define RP20 (&amesys->pModel->realParamArray[38])
#define IP20 (&amesys->pModel->integerParamArray[9])
#define TP20 (&amesys->pModel->textParamArray[0])
#define RS21 (&amesys->pModel->realStoreArray[4])
#define IS21 (&amesys->pModel->intStoreArray[20])
#define RP21 (&amesys->pModel->realParamArray[42])
#define IP21 (&amesys->pModel->integerParamArray[11])
#define RS22 (&amesys->pModel->realStoreArray[6])
#define IS22 (&amesys->pModel->intStoreArray[22])
#define RP22 (&amesys->pModel->realParamArray[49])
#define IP22 (&amesys->pModel->integerParamArray[12])
#define RS23 (&amesys->pModel->realStoreArray[8])
#define RP23 (&amesys->pModel->realParamArray[67])
#define IP23 (&amesys->pModel->integerParamArray[17])
#define IS26 (&amesys->pModel->intStoreArray[34])
#define RP26 (&amesys->pModel->realParamArray[69])
#define IP26 (&amesys->pModel->integerParamArray[18])
#define RP28 (&amesys->pModel->realParamArray[77])
#define RP29 (&amesys->pModel->realParamArray[78])
#define RS30 (&amesys->pModel->realStoreArray[9])
#define IS30 (&amesys->pModel->intStoreArray[36])
#define RP30 (&amesys->pModel->realParamArray[79])
#define IP30 (&amesys->pModel->integerParamArray[21])
#define IS31 (&amesys->pModel->intStoreArray[39])
#define IS32 (&amesys->pModel->intStoreArray[40])
#define RP32 (&amesys->pModel->realParamArray[104])
#define RP35 (&amesys->pModel->realParamArray[110])
#define RS36 (&amesys->pModel->realStoreArray[18])
#define IS36 (&amesys->pModel->intStoreArray[42])
#define RP36 (&amesys->pModel->realParamArray[111])
#define IP36 (&amesys->pModel->integerParamArray[23])
#define IS40 (&amesys->pModel->intStoreArray[52])
#define RP41 (&amesys->pModel->realParamArray[126])
#define RS44 (&amesys->pModel->realStoreArray[20])
#define RP44 (&amesys->pModel->realParamArray[127])
#define IP44 (&amesys->pModel->integerParamArray[27])
#define IS46 (&amesys->pModel->intStoreArray[53])
#define RP46 (&amesys->pModel->realParamArray[129])
#define IP46 (&amesys->pModel->integerParamArray[28])
#define RP47 (&amesys->pModel->realParamArray[137])
#define RS50 (&amesys->pModel->realStoreArray[21])
#define RP50 (&amesys->pModel->realParamArray[138])
#define IP50 (&amesys->pModel->integerParamArray[31])
#define RP51 (&amesys->pModel->realParamArray[140])
#define IS52 (&amesys->pModel->intStoreArray[55])
#define RP52 (&amesys->pModel->realParamArray[142])
#define IP52 (&amesys->pModel->integerParamArray[32])
#define TP52 (&amesys->pModel->textParamArray[1])
#define RS53 (&amesys->pModel->realStoreArray[22])
#define IS53 (&amesys->pModel->intStoreArray[60])
#define RP53 (&amesys->pModel->realParamArray[146])
#define IP53 (&amesys->pModel->integerParamArray[34])
#define RS54 (&amesys->pModel->realStoreArray[24])
#define IS54 (&amesys->pModel->intStoreArray[62])
#define RP54 (&amesys->pModel->realParamArray[153])
#define IP54 (&amesys->pModel->integerParamArray[35])
#define RS55 (&amesys->pModel->realStoreArray[26])
#define RP55 (&amesys->pModel->realParamArray[171])
#define IP55 (&amesys->pModel->integerParamArray[40])
#define IS58 (&amesys->pModel->intStoreArray[74])
#define RP58 (&amesys->pModel->realParamArray[173])
#define IP58 (&amesys->pModel->integerParamArray[41])
#define RP60 (&amesys->pModel->realParamArray[181])
#define RP61 (&amesys->pModel->realParamArray[182])
#define IS62 (&amesys->pModel->intStoreArray[76])
#define RS63 (&amesys->pModel->realStoreArray[27])
#define IS63 (&amesys->pModel->intStoreArray[77])
#define RP63 (&amesys->pModel->realParamArray[183])
#define IP63 (&amesys->pModel->integerParamArray[44])
#define IS64 (&amesys->pModel->intStoreArray[80])
#define RP64 (&amesys->pModel->realParamArray[208])
#define RP67 (&amesys->pModel->realParamArray[214])
#define RS68 (&amesys->pModel->realStoreArray[36])
#define IS68 (&amesys->pModel->intStoreArray[82])
#define RP68 (&amesys->pModel->realParamArray[215])
#define IP68 (&amesys->pModel->integerParamArray[46])
#define IS72 (&amesys->pModel->intStoreArray[92])
#define RP73 (&amesys->pModel->realParamArray[230])
#define RS76 (&amesys->pModel->realStoreArray[38])
#define RP76 (&amesys->pModel->realParamArray[231])
#define IP76 (&amesys->pModel->integerParamArray[50])
#define IS78 (&amesys->pModel->intStoreArray[93])
#define RP78 (&amesys->pModel->realParamArray[233])
#define IP78 (&amesys->pModel->integerParamArray[51])
#define RP79 (&amesys->pModel->realParamArray[241])
#define RS82 (&amesys->pModel->realStoreArray[39])
#define RP82 (&amesys->pModel->realParamArray[242])
#define IP82 (&amesys->pModel->integerParamArray[54])
#define RP83 (&amesys->pModel->realParamArray[244])
#define RS84 (&amesys->pModel->realStoreArray[40])
#define IS84 (&amesys->pModel->intStoreArray[95])
#define RP84 (&amesys->pModel->realParamArray[246])
#define IP84 (&amesys->pModel->integerParamArray[55])
#define RS85 (&amesys->pModel->realStoreArray[42])
#define IS85 (&amesys->pModel->intStoreArray[97])
#define RP85 (&amesys->pModel->realParamArray[253])
#define IP85 (&amesys->pModel->integerParamArray[56])
#define RS86 (&amesys->pModel->realStoreArray[44])
#define RP86 (&amesys->pModel->realParamArray[271])
#define IP86 (&amesys->pModel->integerParamArray[61])
#define IS89 (&amesys->pModel->intStoreArray[109])
#define RP89 (&amesys->pModel->realParamArray[273])
#define IP89 (&amesys->pModel->integerParamArray[62])
#define RP91 (&amesys->pModel->realParamArray[281])
#define RP92 (&amesys->pModel->realParamArray[282])
#define IS93 (&amesys->pModel->intStoreArray[111])
#define RS94 (&amesys->pModel->realStoreArray[45])
#define IS94 (&amesys->pModel->intStoreArray[112])
#define RP94 (&amesys->pModel->realParamArray[283])
#define IP94 (&amesys->pModel->integerParamArray[65])
#define IS95 (&amesys->pModel->intStoreArray[115])
#define RP95 (&amesys->pModel->realParamArray[308])
#define IP95 (&amesys->pModel->integerParamArray[67])
#define TP95 (&amesys->pModel->textParamArray[2])


static const S_AMEVariableInfo GVarInfo[AME_NB_VAR_INFO] = {
   { 0, 1, 1, 1, "MECRL0A", "wdup" }
  ,{ 1, 1, 1, 1, "MECRL0A", "thetadup" }
  ,{ 2, 1, 1, 1, "EMDPMDC01", "torqrot" }
  ,{ 3, 1, 1, 1, "EBCT00", "V3" }
  ,{ 4, 1, 1, 1, "EMDPMDC01", "I2" }
  ,{ 5, 1, 1, 1, "EB3N03", "V1" }
  ,{ 6, 0, 1, 1, "EMDPMDC01", "I3" }
  ,{ 7, 1, 1, 1, "THTC0", "t2" }
  ,{ 8, 1, 1, 1, "EMDPMDC01", "dh" }
  ,{ 9, 1, 1, 1, "W000", "wzero" }
  ,{ 10, 1, 1, 1, "W000", "theta" }
  ,{ 11, 1, 1, 1, "EMDPMDC01", "torqstat" }
  ,{ 12, 1, 1, 1, "EMDPMDC01", "Ua" }
  ,{ 13, 1, 1, 1, "EMDPMDC01", "Ia" }
  ,{ 14, 1, 1, 1, "EMDPMDC01", "wdif" }
  ,{ 15, 1, 1, 1, "EMDPMDC01", "Tem" }
  ,{ 16, 1, 1, 1, "EMDPMDC01", "bemf" }
  ,{ 17, 1, 1, 1, "EMDPMDC01", "Ra" }
  ,{ 18, 1, 1, 1, "EMDPMDC01", "Kt" }
  ,{ 19, 0, 1, 1, "EMDPMDC01", "powerRSelec" }
  ,{ 20, 0, 1, 1, "EMDPMDC01", "powerIelec" }
  ,{ 21, 1, 1, 1, "CONS00", "out" }
  ,{ 22, 1, 1, 1, "CONS00", "k0GEN" }
  ,{ 23, 1, 1, 1, "MECADS1B", "tordup" }
  ,{ 24, 1, 1, 1, "MECRL0A", "w" }
  ,{ 25, 1, 1, 1, "MECRL0A", "theta" }
  ,{ 26, 1, 1, 1, "MECRL0A", "accel" }
  ,{ 27, 1, 1, 1, "MECRL0A", "thetamodulo" }
  ,{ 28, 1, 1, 1, "MECRL0A", "thetancycle" }
  ,{ 29, 1, 1, 1, "MECRL0A", "Tvisc" }
  ,{ 30, 1, 1, 1, "MECRL0A", "Tcont" }
  ,{ 31, 1, 1, 1, "MECRL0A", "TcontMin" }
  ,{ 32, 1, 1, 1, "MECRL0A", "TcontMax" }
  ,{ 33, 0, 1, 1, "MECRL0A", "powerRfrict" }
  ,{ 34, 0, 1, 1, "MECRL0A", "powerIrmass" }
  ,{ 35, 0, 1, 1, "MECRL0A", "powerCendstop" }
  ,{ 36, 0, 1, 1, "MECRL0A", "powerRendstop" }
  ,{ 37, 1, 1, 1, "EB3N03", "V2" }
  ,{ 38, 1, 1, 1, "EBVS01", "I1" }
  ,{ 39, 1, 1, 1, "EBCT00", "I1" }
  ,{ 40, 1, 1, 1, "EBVS01", "V2" }
  ,{ 41, 1, 1, 1, "SIGVSAT0", "output" }
  ,{ 42, 1, 1, 1, "EBVS01", "U" }
  ,{ 43, 1, 1, 1, "EBVS01", "I" }
  ,{ 44, 1, 1, 1, "EB3N03", "I3" }
  ,{ 45, 1, 1, 1, "EBZV01", "V1" }
  ,{ 46, 1, 1, 1, "SPLT0", "out2" }
  ,{ 47, 1, 1, 1, "PID001", "v" }
  ,{ 48, 1, 1, 1, "SGN10", "outp" }
  ,{ 49, 1, 1, 2, "CONS00", "out" }
  ,{ 50, 1, 1, 2, "CONS00", "k0GEN" }
  ,{ 51, 1, 1, 1, "SPLT0", "out1" }
  ,{ 52, 1, 1, 1, "EBCT00", "vsig" }
  ,{ 53, 1, 1, 1, "EBCT00", "U" }
  ,{ 54, 1, 1, 1, "EBCT00", "I" }
  ,{ 55, 1, 1, 1, "JUN3M", "output" }
  ,{ 56, 1, 1, 2, "SIGVSAT0", "output" }
  ,{ 57, 1, 1, 1, "PID001", "ipart" }
  ,{ 58, 1, 1, 1, "PID001", "ppart" }
  ,{ 59, 1, 1, 1, "PID001", "outlim0GEN" }
  ,{ 60, 1, 1, 1, "PID001", "antiwind0GEN" }
  ,{ 61, 1, 1, 1, "PID001", "Kp0GEN" }
  ,{ 62, 1, 1, 1, "PID001", "Ki0GEN" }
  ,{ 63, 1, 1, 1, "PID001", "Kd0GEN" }
  ,{ 64, 1, 1, 1, "PID001", "tau0GEN" }
  ,{ 65, 1, 1, 1, "PID001", "outmin0GEN" }
  ,{ 66, 1, 1, 1, "PID001", "outmax0GEN" }
  ,{ 67, 1, 1, 1, "PID001", "Ks0GEN" }
  ,{ 68, 1, 1, 1, "PID001", "hyst0GEN" }
  ,{ 69, 1, 1, 3, "CONS00", "out" }
  ,{ 70, 1, 1, 3, "CONS00", "k0GEN" }
  ,{ 71, 1, 1, 2, "SGN10", "outp" }
  ,{ 72, 1, 1, 2, "SPLT0", "out1" }
  ,{ 73, 1, 1, 2, "SPLT0", "out2" }
  ,{ 74, 1, 1, 1, "MECADS1B", "output" }
  ,{ 75, 1, 1, 1, "MECRN10A", "t1" }
  ,{ 76, 1, 1, 1, "MECADS1B", "wdup" }
  ,{ 77, 1, 1, 1, "MECADS1B", "thetadup" }
  ,{ 78, 1, 1, 1, "SD0000A", "force1" }
  ,{ 79, 1, 1, 1, "RACK01A", "v1" }
  ,{ 80, 1, 1, 1, "RACK01A", "x1" }
  ,{ 81, 1, 1, 1, "MECRN10A", "w2" }
  ,{ 82, 1, 1, 1, "MECRN10A", "theta2" }
  ,{ 83, 1, 1, 1, "RACK01A", "t2" }
  ,{ 84, 1, 1, 1, "MECRN10A", "eff" }
  ,{ 85, 0, 1, 1, "MECRN10A", "powerRgear" }
  ,{ 86, 1, 1, 1, "MECMAS21", "v1dup" }
  ,{ 87, 1, 1, 1, "MECMAS21", "x1dup" }
  ,{ 88, 1, 1, 1, "MECMAS21", "acc1dup" }
  ,{ 89, 1, 1, 1, "SD0000A", "force2" }
  ,{ 90, 1, 1, 1, "SD0000A", "force" }
  ,{ 91, 1, 1, 1, "SD0000A", "damperforce" }
  ,{ 92, 1, 1, 1, "SD0000A", "x" }
  ,{ 93, 1, 1, 1, "SD0000A", "kval" }
  ,{ 94, 0, 1, 1, "SD0000A", "powerCspring" }
  ,{ 95, 0, 1, 1, "SD0000A", "powerRdamp" }
  ,{ 96, 1, 1, 1, "MECDS0A", "fordup" }
  ,{ 97, 1, 1, 1, "MECMAS21", "v1" }
  ,{ 98, 1, 1, 1, "MECMAS21", "x1" }
  ,{ 99, 1, 1, 1, "MECMAS21", "acc1" }
  ,{ 100, 1, 1, 1, "MECMAS21", "Fmin" }
  ,{ 101, 1, 1, 1, "MECMAS21", "Fmax" }
  ,{ 102, 1, 1, 1, "MECMAS21", "Fvisc" }
  ,{ 103, 1, 1, 1, "MECMAS21", "Ffric" }
  ,{ 104, 1, 1, 1, "MECMAS21", "stick" }
  ,{ 105, 0, 1, 1, "MECMAS21", "powerRfrict" }
  ,{ 106, 0, 1, 1, "MECMAS21", "powerImass" }
  ,{ 107, 0, 1, 1, "MECMAS21", "powerCendstop" }
  ,{ 108, 0, 1, 1, "MECMAS21", "powerSgrav" }
  ,{ 109, 0, 1, 1, "MECMAS21", "powerRendstop" }
  ,{ 110, 1, 1, 1, "F000", "fzero" }
  ,{ 111, 1, 1, 1, "MECDS0A", "vdup" }
  ,{ 112, 1, 1, 1, "MECDS0A", "xdup" }
  ,{ 113, 1, 1, 1, "MECDS0A", "xsig" }
  ,{ 114, 1, 1, 2, "PID001", "v" }
  ,{ 115, 1, 1, 2, "GA00", "output" }
  ,{ 116, 1, 1, 2, "PID001", "w" }
  ,{ 117, 1, 1, 2, "PID001", "ipart" }
  ,{ 118, 1, 1, 2, "PID001", "ppart" }
  ,{ 119, 1, 1, 2, "PID001", "dpart" }
  ,{ 120, 1, 1, 2, "PID001", "outlim0GEN" }
  ,{ 121, 1, 1, 2, "PID001", "antiwind0GEN" }
  ,{ 122, 1, 1, 2, "PID001", "Kp0GEN" }
  ,{ 123, 1, 1, 2, "PID001", "Ki0GEN" }
  ,{ 124, 1, 1, 2, "PID001", "Kd0GEN" }
  ,{ 125, 1, 1, 2, "PID001", "tau0GEN" }
  ,{ 126, 1, 1, 2, "PID001", "outmin0GEN" }
  ,{ 127, 1, 1, 2, "PID001", "outmax0GEN" }
  ,{ 128, 1, 1, 2, "PID001", "Ks0GEN" }
  ,{ 129, 1, 1, 2, "PID001", "hyst0GEN" }
  ,{ 130, 1, 1, 1, "GA00", "output" }
  ,{ 131, 1, 1, 2, "JUN3M", "output" }
  ,{ 132, 1, 1, 1, "UD00", "output" }
  ,{ 133, 1, 1, 1, "GA00", "k0GEN" }
  ,{ 134, 1, 1, 2, "GA00", "k0GEN" }
  ,{ 135, 1, 1, 2, "MECRL0A", "wdup" }
  ,{ 136, 1, 1, 2, "MECRL0A", "thetadup" }
  ,{ 137, 1, 1, 2, "EMDPMDC01", "torqrot" }
  ,{ 138, 1, 1, 2, "EBCT00", "V3" }
  ,{ 139, 1, 1, 2, "EMDPMDC01", "I2" }
  ,{ 140, 1, 1, 2, "EB3N03", "V1" }
  ,{ 141, 0, 1, 2, "EMDPMDC01", "I3" }
  ,{ 142, 1, 1, 2, "THTC0", "t2" }
  ,{ 143, 1, 1, 2, "EMDPMDC01", "dh" }
  ,{ 144, 1, 1, 2, "W000", "wzero" }
  ,{ 145, 1, 1, 2, "W000", "theta" }
  ,{ 146, 1, 1, 2, "EMDPMDC01", "torqstat" }
  ,{ 147, 1, 1, 2, "EMDPMDC01", "Ua" }
  ,{ 148, 1, 1, 2, "EMDPMDC01", "Ia" }
  ,{ 149, 1, 1, 2, "EMDPMDC01", "wdif" }
  ,{ 150, 1, 1, 2, "EMDPMDC01", "Tem" }
  ,{ 151, 1, 1, 2, "EMDPMDC01", "bemf" }
  ,{ 152, 1, 1, 2, "EMDPMDC01", "Ra" }
  ,{ 153, 1, 1, 2, "EMDPMDC01", "Kt" }
  ,{ 154, 0, 1, 2, "EMDPMDC01", "powerRSelec" }
  ,{ 155, 0, 1, 2, "EMDPMDC01", "powerIelec" }
  ,{ 156, 1, 1, 4, "CONS00", "out" }
  ,{ 157, 1, 1, 4, "CONS00", "k0GEN" }
  ,{ 158, 1, 1, 2, "MECADS1B", "tordup" }
  ,{ 159, 1, 1, 2, "MECRL0A", "w" }
  ,{ 160, 1, 1, 2, "MECRL0A", "theta" }
  ,{ 161, 1, 1, 2, "MECRL0A", "accel" }
  ,{ 162, 1, 1, 2, "MECRL0A", "thetamodulo" }
  ,{ 163, 1, 1, 2, "MECRL0A", "thetancycle" }
  ,{ 164, 1, 1, 2, "MECRL0A", "Tvisc" }
  ,{ 165, 1, 1, 2, "MECRL0A", "Tcont" }
  ,{ 166, 1, 1, 2, "MECRL0A", "TcontMin" }
  ,{ 167, 1, 1, 2, "MECRL0A", "TcontMax" }
  ,{ 168, 0, 1, 2, "MECRL0A", "powerRfrict" }
  ,{ 169, 0, 1, 2, "MECRL0A", "powerIrmass" }
  ,{ 170, 0, 1, 2, "MECRL0A", "powerCendstop" }
  ,{ 171, 0, 1, 2, "MECRL0A", "powerRendstop" }
  ,{ 172, 1, 1, 2, "EB3N03", "V2" }
  ,{ 173, 1, 1, 2, "EBVS01", "I1" }
  ,{ 174, 1, 1, 2, "EBCT00", "I1" }
  ,{ 175, 1, 1, 2, "EBVS01", "V2" }
  ,{ 176, 1, 1, 3, "SIGVSAT0", "output" }
  ,{ 177, 1, 1, 2, "EBVS01", "U" }
  ,{ 178, 1, 1, 2, "EBVS01", "I" }
  ,{ 179, 1, 1, 2, "EB3N03", "I3" }
  ,{ 180, 1, 1, 2, "EBZV01", "V1" }
  ,{ 181, 1, 1, 3, "SPLT0", "out2" }
  ,{ 182, 1, 1, 3, "PID001", "v" }
  ,{ 183, 1, 1, 3, "SGN10", "outp" }
  ,{ 184, 1, 1, 5, "CONS00", "out" }
  ,{ 185, 1, 1, 5, "CONS00", "k0GEN" }
  ,{ 186, 1, 1, 3, "SPLT0", "out1" }
  ,{ 187, 1, 1, 2, "EBCT00", "vsig" }
  ,{ 188, 1, 1, 2, "EBCT00", "U" }
  ,{ 189, 1, 1, 2, "EBCT00", "I" }
  ,{ 190, 1, 1, 3, "JUN3M", "output" }
  ,{ 191, 1, 1, 4, "SIGVSAT0", "output" }
  ,{ 192, 1, 1, 3, "PID001", "ipart" }
  ,{ 193, 1, 1, 3, "PID001", "ppart" }
  ,{ 194, 1, 1, 3, "PID001", "outlim0GEN" }
  ,{ 195, 1, 1, 3, "PID001", "antiwind0GEN" }
  ,{ 196, 1, 1, 3, "PID001", "Kp0GEN" }
  ,{ 197, 1, 1, 3, "PID001", "Ki0GEN" }
  ,{ 198, 1, 1, 3, "PID001", "Kd0GEN" }
  ,{ 199, 1, 1, 3, "PID001", "tau0GEN" }
  ,{ 200, 1, 1, 3, "PID001", "outmin0GEN" }
  ,{ 201, 1, 1, 3, "PID001", "outmax0GEN" }
  ,{ 202, 1, 1, 3, "PID001", "Ks0GEN" }
  ,{ 203, 1, 1, 3, "PID001", "hyst0GEN" }
  ,{ 204, 1, 1, 6, "CONS00", "out" }
  ,{ 205, 1, 1, 6, "CONS00", "k0GEN" }
  ,{ 206, 1, 1, 4, "SGN10", "outp" }
  ,{ 207, 1, 1, 4, "SPLT0", "out1" }
  ,{ 208, 1, 1, 4, "SPLT0", "out2" }
  ,{ 209, 1, 1, 2, "MECADS1B", "output" }
  ,{ 210, 1, 1, 2, "MECRN10A", "t1" }
  ,{ 211, 1, 1, 2, "MECADS1B", "wdup" }
  ,{ 212, 1, 1, 2, "MECADS1B", "thetadup" }
  ,{ 213, 1, 1, 2, "SD0000A", "force1" }
  ,{ 214, 1, 1, 2, "RACK01A", "v1" }
  ,{ 215, 1, 1, 2, "RACK01A", "x1" }
  ,{ 216, 1, 1, 2, "MECRN10A", "w2" }
  ,{ 217, 1, 1, 2, "MECRN10A", "theta2" }
  ,{ 218, 1, 1, 2, "RACK01A", "t2" }
  ,{ 219, 1, 1, 2, "MECRN10A", "eff" }
  ,{ 220, 0, 1, 2, "MECRN10A", "powerRgear" }
  ,{ 221, 1, 1, 2, "MECMAS21", "v1dup" }
  ,{ 222, 1, 1, 2, "MECMAS21", "x1dup" }
  ,{ 223, 1, 1, 2, "MECMAS21", "acc1dup" }
  ,{ 224, 1, 1, 2, "SD0000A", "force2" }
  ,{ 225, 1, 1, 2, "SD0000A", "force" }
  ,{ 226, 1, 1, 2, "SD0000A", "damperforce" }
  ,{ 227, 1, 1, 2, "SD0000A", "x" }
  ,{ 228, 1, 1, 2, "SD0000A", "kval" }
  ,{ 229, 0, 1, 2, "SD0000A", "powerCspring" }
  ,{ 230, 0, 1, 2, "SD0000A", "powerRdamp" }
  ,{ 231, 1, 1, 2, "MECDS0A", "fordup" }
  ,{ 232, 1, 1, 2, "MECMAS21", "v1" }
  ,{ 233, 1, 1, 2, "MECMAS21", "x1" }
  ,{ 234, 1, 1, 2, "MECMAS21", "acc1" }
  ,{ 235, 1, 1, 2, "MECMAS21", "Fmin" }
  ,{ 236, 1, 1, 2, "MECMAS21", "Fmax" }
  ,{ 237, 1, 1, 2, "MECMAS21", "Fvisc" }
  ,{ 238, 1, 1, 2, "MECMAS21", "Ffric" }
  ,{ 239, 1, 1, 2, "MECMAS21", "stick" }
  ,{ 240, 0, 1, 2, "MECMAS21", "powerRfrict" }
  ,{ 241, 0, 1, 2, "MECMAS21", "powerImass" }
  ,{ 242, 0, 1, 2, "MECMAS21", "powerCendstop" }
  ,{ 243, 0, 1, 2, "MECMAS21", "powerSgrav" }
  ,{ 244, 0, 1, 2, "MECMAS21", "powerRendstop" }
  ,{ 245, 1, 1, 2, "F000", "fzero" }
  ,{ 246, 1, 1, 2, "MECDS0A", "vdup" }
  ,{ 247, 1, 1, 2, "MECDS0A", "xdup" }
  ,{ 248, 1, 1, 2, "MECDS0A", "xsig" }
  ,{ 249, 1, 1, 4, "PID001", "v" }
  ,{ 250, 1, 1, 4, "GA00", "output" }
  ,{ 251, 1, 1, 4, "PID001", "w" }
  ,{ 252, 1, 1, 4, "PID001", "ipart" }
  ,{ 253, 1, 1, 4, "PID001", "ppart" }
  ,{ 254, 1, 1, 4, "PID001", "dpart" }
  ,{ 255, 1, 1, 4, "PID001", "outlim0GEN" }
  ,{ 256, 1, 1, 4, "PID001", "antiwind0GEN" }
  ,{ 257, 1, 1, 4, "PID001", "Kp0GEN" }
  ,{ 258, 1, 1, 4, "PID001", "Ki0GEN" }
  ,{ 259, 1, 1, 4, "PID001", "Kd0GEN" }
  ,{ 260, 1, 1, 4, "PID001", "tau0GEN" }
  ,{ 261, 1, 1, 4, "PID001", "outmin0GEN" }
  ,{ 262, 1, 1, 4, "PID001", "outmax0GEN" }
  ,{ 263, 1, 1, 4, "PID001", "Ks0GEN" }
  ,{ 264, 1, 1, 4, "PID001", "hyst0GEN" }
  ,{ 265, 1, 1, 3, "GA00", "output" }
  ,{ 266, 1, 1, 4, "JUN3M", "output" }
  ,{ 267, 1, 1, 2, "UD00", "output" }
  ,{ 268, 1, 1, 3, "GA00", "k0GEN" }
  ,{ 269, 1, 1, 4, "GA00", "k0GEN" }
  ,{ 270, 1, 1, 3, "MECRL0A", "wdup" }
  ,{ 271, 1, 1, 3, "MECRL0A", "thetadup" }
  ,{ 272, 1, 1, 3, "EMDPMDC01", "torqrot" }
  ,{ 273, 1, 1, 3, "EBCT00", "V3" }
  ,{ 274, 1, 1, 3, "EMDPMDC01", "I2" }
  ,{ 275, 1, 1, 3, "EB3N03", "V1" }
  ,{ 276, 0, 1, 3, "EMDPMDC01", "I3" }
  ,{ 277, 1, 1, 3, "THTC0", "t2" }
  ,{ 278, 1, 1, 3, "EMDPMDC01", "dh" }
  ,{ 279, 1, 1, 3, "W000", "wzero" }
  ,{ 280, 1, 1, 3, "W000", "theta" }
  ,{ 281, 1, 1, 3, "EMDPMDC01", "torqstat" }
  ,{ 282, 1, 1, 3, "EMDPMDC01", "Ua" }
  ,{ 283, 1, 1, 3, "EMDPMDC01", "Ia" }
  ,{ 284, 1, 1, 3, "EMDPMDC01", "wdif" }
  ,{ 285, 1, 1, 3, "EMDPMDC01", "Tem" }
  ,{ 286, 1, 1, 3, "EMDPMDC01", "bemf" }
  ,{ 287, 1, 1, 3, "EMDPMDC01", "Ra" }
  ,{ 288, 1, 1, 3, "EMDPMDC01", "Kt" }
  ,{ 289, 0, 1, 3, "EMDPMDC01", "powerRSelec" }
  ,{ 290, 0, 1, 3, "EMDPMDC01", "powerIelec" }
  ,{ 291, 1, 1, 7, "CONS00", "out" }
  ,{ 292, 1, 1, 7, "CONS00", "k0GEN" }
  ,{ 293, 1, 1, 3, "MECADS1B", "tordup" }
  ,{ 294, 1, 1, 3, "MECRL0A", "w" }
  ,{ 295, 1, 1, 3, "MECRL0A", "theta" }
  ,{ 296, 1, 1, 3, "MECRL0A", "accel" }
  ,{ 297, 1, 1, 3, "MECRL0A", "thetamodulo" }
  ,{ 298, 1, 1, 3, "MECRL0A", "thetancycle" }
  ,{ 299, 1, 1, 3, "MECRL0A", "Tvisc" }
  ,{ 300, 1, 1, 3, "MECRL0A", "Tcont" }
  ,{ 301, 1, 1, 3, "MECRL0A", "TcontMin" }
  ,{ 302, 1, 1, 3, "MECRL0A", "TcontMax" }
  ,{ 303, 0, 1, 3, "MECRL0A", "powerRfrict" }
  ,{ 304, 0, 1, 3, "MECRL0A", "powerIrmass" }
  ,{ 305, 0, 1, 3, "MECRL0A", "powerCendstop" }
  ,{ 306, 0, 1, 3, "MECRL0A", "powerRendstop" }
  ,{ 307, 1, 1, 3, "EB3N03", "V2" }
  ,{ 308, 1, 1, 3, "EBVS01", "I1" }
  ,{ 309, 1, 1, 3, "EBCT00", "I1" }
  ,{ 310, 1, 1, 3, "EBVS01", "V2" }
  ,{ 311, 1, 1, 5, "SIGVSAT0", "output" }
  ,{ 312, 1, 1, 3, "EBVS01", "U" }
  ,{ 313, 1, 1, 3, "EBVS01", "I" }
  ,{ 314, 1, 1, 3, "EB3N03", "I3" }
  ,{ 315, 1, 1, 3, "EBZV01", "V1" }
  ,{ 316, 1, 1, 5, "SPLT0", "out2" }
  ,{ 317, 1, 1, 5, "PID001", "v" }
  ,{ 318, 1, 1, 5, "SGN10", "outp" }
  ,{ 319, 1, 1, 8, "CONS00", "out" }
  ,{ 320, 1, 1, 8, "CONS00", "k0GEN" }
  ,{ 321, 1, 1, 5, "SPLT0", "out1" }
  ,{ 322, 1, 1, 3, "EBCT00", "vsig" }
  ,{ 323, 1, 1, 3, "EBCT00", "U" }
  ,{ 324, 1, 1, 3, "EBCT00", "I" }
  ,{ 325, 1, 1, 5, "JUN3M", "output" }
  ,{ 326, 1, 1, 6, "SIGVSAT0", "output" }
  ,{ 327, 1, 1, 5, "PID001", "ipart" }
  ,{ 328, 1, 1, 5, "PID001", "ppart" }
  ,{ 329, 1, 1, 5, "PID001", "outlim0GEN" }
  ,{ 330, 1, 1, 5, "PID001", "antiwind0GEN" }
  ,{ 331, 1, 1, 5, "PID001", "Kp0GEN" }
  ,{ 332, 1, 1, 5, "PID001", "Ki0GEN" }
  ,{ 333, 1, 1, 5, "PID001", "Kd0GEN" }
  ,{ 334, 1, 1, 5, "PID001", "tau0GEN" }
  ,{ 335, 1, 1, 5, "PID001", "outmin0GEN" }
  ,{ 336, 1, 1, 5, "PID001", "outmax0GEN" }
  ,{ 337, 1, 1, 5, "PID001", "Ks0GEN" }
  ,{ 338, 1, 1, 5, "PID001", "hyst0GEN" }
  ,{ 339, 1, 1, 9, "CONS00", "out" }
  ,{ 340, 1, 1, 9, "CONS00", "k0GEN" }
  ,{ 341, 1, 1, 6, "SGN10", "outp" }
  ,{ 342, 1, 1, 6, "SPLT0", "out1" }
  ,{ 343, 1, 1, 6, "SPLT0", "out2" }
  ,{ 344, 1, 1, 3, "MECADS1B", "output" }
  ,{ 345, 1, 1, 3, "MECRN10A", "t1" }
  ,{ 346, 1, 1, 3, "MECADS1B", "wdup" }
  ,{ 347, 1, 1, 3, "MECADS1B", "thetadup" }
  ,{ 348, 1, 1, 3, "SD0000A", "force1" }
  ,{ 349, 1, 1, 3, "RACK01A", "v1" }
  ,{ 350, 1, 1, 3, "RACK01A", "x1" }
  ,{ 351, 1, 1, 3, "MECRN10A", "w2" }
  ,{ 352, 1, 1, 3, "MECRN10A", "theta2" }
  ,{ 353, 1, 1, 3, "RACK01A", "t2" }
  ,{ 354, 1, 1, 3, "MECMAS21", "v1dup" }
  ,{ 355, 1, 1, 3, "MECMAS21", "x1dup" }
  ,{ 356, 1, 1, 3, "MECMAS21", "acc1dup" }
  ,{ 357, 1, 1, 3, "SD0000A", "force2" }
  ,{ 358, 1, 1, 3, "SD0000A", "force" }
  ,{ 359, 1, 1, 3, "SD0000A", "damperforce" }
  ,{ 360, 1, 1, 3, "SD0000A", "x" }
  ,{ 361, 1, 1, 3, "SD0000A", "kval" }
  ,{ 362, 0, 1, 3, "SD0000A", "powerCspring" }
  ,{ 363, 0, 1, 3, "SD0000A", "powerRdamp" }
  ,{ 364, 1, 1, 3, "MECDS0A", "fordup" }
  ,{ 365, 1, 1, 3, "MECMAS21", "v1" }
  ,{ 366, 1, 1, 3, "MECMAS21", "x1" }
  ,{ 367, 1, 1, 3, "MECMAS21", "acc1" }
  ,{ 368, 1, 1, 3, "MECMAS21", "Fmin" }
  ,{ 369, 1, 1, 3, "MECMAS21", "Fmax" }
  ,{ 370, 1, 1, 3, "MECMAS21", "Fvisc" }
  ,{ 371, 1, 1, 3, "MECMAS21", "Ffric" }
  ,{ 372, 1, 1, 3, "MECMAS21", "stick" }
  ,{ 373, 0, 1, 3, "MECMAS21", "powerRfrict" }
  ,{ 374, 0, 1, 3, "MECMAS21", "powerImass" }
  ,{ 375, 0, 1, 3, "MECMAS21", "powerCendstop" }
  ,{ 376, 0, 1, 3, "MECMAS21", "powerSgrav" }
  ,{ 377, 0, 1, 3, "MECMAS21", "powerRendstop" }
  ,{ 378, 1, 1, 3, "F000", "fzero" }
  ,{ 379, 1, 1, 3, "MECDS0A", "vdup" }
  ,{ 380, 1, 1, 3, "MECDS0A", "xdup" }
  ,{ 381, 1, 1, 3, "MECDS0A", "xsig" }
  ,{ 382, 1, 1, 6, "PID001", "v" }
  ,{ 383, 1, 1, 6, "GA00", "output" }
  ,{ 384, 1, 1, 6, "PID001", "w" }
  ,{ 385, 1, 1, 6, "PID001", "ipart" }
  ,{ 386, 1, 1, 6, "PID001", "ppart" }
  ,{ 387, 1, 1, 6, "PID001", "dpart" }
  ,{ 388, 1, 1, 6, "PID001", "outlim0GEN" }
  ,{ 389, 1, 1, 6, "PID001", "antiwind0GEN" }
  ,{ 390, 1, 1, 6, "PID001", "Kp0GEN" }
  ,{ 391, 1, 1, 6, "PID001", "Ki0GEN" }
  ,{ 392, 1, 1, 6, "PID001", "Kd0GEN" }
  ,{ 393, 1, 1, 6, "PID001", "tau0GEN" }
  ,{ 394, 1, 1, 6, "PID001", "outmin0GEN" }
  ,{ 395, 1, 1, 6, "PID001", "outmax0GEN" }
  ,{ 396, 1, 1, 6, "PID001", "Ks0GEN" }
  ,{ 397, 1, 1, 6, "PID001", "hyst0GEN" }
  ,{ 398, 1, 1, 5, "GA00", "output" }
  ,{ 399, 1, 1, 6, "JUN3M", "output" }
  ,{ 400, 1, 1, 3, "UD00", "output" }
  ,{ 401, 1, 1, 5, "GA00", "k0GEN" }
  ,{ 402, 1, 1, 6, "GA00", "k0GEN" }
  ,{ 403, 1, 1, 3, "MECRN10A", "eff" }
  ,{ 404, 0, 1, 3, "MECRN10A", "powerRgear" }
};

/* For memory access in case of RT target such as dSpace targets */
#ifdef AME_MEMORY_ACCESS_RT_EXPORT
#if(AME_NBOF_VARS>0)
static double RT_Export_Vars[AME_NBOF_VARS];
#endif
#if(AME_NBOF_REAL_PARAMS>0)
static double RT_Export_RealParam[AME_NBOF_REAL_PARAMS];
#endif
#if(AME_NBOF_INT_PARAMS>0)
static int RT_Export_IntParam[AME_NBOF_INT_PARAMS];
#endif
#endif


#if !defined(AME_IMPLICIT_MODEL_ACCEPTED) && (AME_MODEL_ISEXPLICIT == 0)
#error "Implicit model not supported for the current interface."
#endif


/* ============================================================== */
/* If the interface needs linearisation (cosim and Amesim) */

#ifndef AME_NO_LA
#ifndef AME_NEED_LINEAR_ANALYSIS
#define AME_NEED_LINEAR_ANALYSIS
#endif
#endif

#if( AME_MODEL_ISEXPLICIT == 1)
#define AMEfuncPerturb LPerturbIfNecessary
#else
#define AMEfuncPerturb DPerturbIfNecessary
#endif

#ifdef AME_ADVANCEDDEBUG
static void AME_POST_SUBMODCALL_WITH_DISCON(AMESIMSYSTEM *amesys, int *flag, int *sflag, int *oflag, int *panic, char *submodelname, int instancenum)
{
   if(*sflag < 3)*sflag = getnfg_();
#ifdef AME_NEED_LINEAR_ANALYSIS
   if(*flag == 5)
   {
      AMEfuncPerturb(amesys, flag);
   }
   else if(*oflag != 5)
   {
      resdis(amesys, flag, sflag, oflag, submodelname, instancenum, panic);
   }
#else
   resdis(amesys, flag, sflag, oflag, submodelname, instancenum, panic);
#endif
}

static void AME_POST_SUBMODCALL_NO_DISCON(AMESIMSYSTEM *amesys, int *flag)
{
#ifdef AME_NEED_LINEAR_ANALYSIS
   if(*flag == 5)
   {
      AMEfuncPerturb(amesys, flag);
   }
#endif
}
#endif


#ifndef AME_ADVANCEDDEBUG
#ifdef AME_NEED_LINEAR_ANALYSIS
/* Typically for normal runs and cosim */
#define AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,sflag,oflag,panic,submodelname,instancenum) if(*sflag < 3)*sflag = getnfg_(); if(*flag == 5) AMEfuncPerturb(amesys, flag); else if(*oflag != 5) resdis(amesys, flag, sflag, oflag, submodelname, instancenum, panic)
#define AME_POST_SUBMODCALL_NO_DISCON(amesys,flag) if(*flag == 5) AMEfuncPerturb(amesys, flag)
#else
/* Typically for code exchange (simulink for instance) */
#define AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,sflag,oflag,panic,submodelname,instancenum) if(*sflag < 3)*sflag = getnfg_(); resdis(amesys, flag, sflag, oflag,submodelname,instancenum,panic)
#define AME_POST_SUBMODCALL_NO_DISCON(amesys,flag)
#endif
#endif

#ifdef AMERT
/* We dont need LA nor resdis for real-time - so set them to (almost) empty macros. (set sflag=0) */
#undef AME_POST_SUBMODCALL_WITH_DISCON
#undef AME_POST_SUBMODCALL_NO_DISCON
#define AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,sflag,oflag,panic,submodelname,instancenum) *sflag = 0
#define AME_POST_SUBMODCALL_NO_DISCON(amesys,flag) 
#endif

/* ============================================================== */

#ifdef AMERT
double IL_SIMOTICS_1_Test_step_ratio=0;
#endif

#ifdef AME_MEMORY_ACCESS_RT_EXPORT
#include "SIMOTICS_1_Test_.export.h"
#endif

#ifdef AME_INPUT_IN_MEMORY
#include "SIMOTICS_1_Test_.sim.h"
#endif

#if( AME_MODEL_ISEXPLICIT == 0 )
#define LRW (40+9*AME_NBOF_SOLVER_STATES+AME_NBOF_SOLVER_STATES*AME_NBOF_SOLVER_STATES)
#define LIW (21+AME_NBOF_SOLVER_STATES)
#endif

/* *******************  Function prototypes ************ */


extern void emdpmdc01in_(int *n, double *RP, int *IS, double *y0
                   , double *y1, double *y2, double *y3, double *y4
                   );
extern void emdpmdc01_(int *n, double *ve0, double *ve1, double *ve2
                   , double *ve3, double *ve4, double *vi5, double *vi6
                   , double *vidot6, double *vi7, double *vi8
                   , double *vi9, double *vi10, double *vi11, double *vi12
                   , double *vidot12, double *vi13, double *vi14
                   , double *vidot14, double *vi15, double *vidot15
                   , double *vi16, double *vi17, double *vidot17
                   , double *RP, int *IS);
extern double emdpmdc01_macro0_(int *n, double *m0, double *m1
              , double *RP, int *IS);
extern void w000in_(int *n, double *y0, double *y1);
extern void thtc0in_(int *n);
extern void cons00in_(int *n, double *RP, double *y0);
extern void cons00_(int *n, double *ve0, double *vi1, double *RP
                 , int *flag);
extern void mecrl0ain_(int *n, double *RP, int *IP, double *RS
                 , int *IS, double *y0, double *y1, double *y2
                   , double *y3, double *y4, double *y5, double *y6
                   , double *y7, double *y8, double *y9);
extern void mecrl0a_(int *n, double *ve0, double *ve1, double *vedot1
                   , double *ve2, double *vedot2, double *ve3
                   , double *vi4, double *vi5, double *vi6, double *vi7
                   , double *vi8, double *vi9, double *vi10, double *vi11
                   , double *vidot11, double *vi12, double *vi13
                   , double *vidot13, double *vi14, double *vidot14
                   , double *vi15, double *vi16, double *vidot16
                   , double *vi17, double *vidot17, double *vi18
                   , double *vi19, double *vidot19, double *vi20
                   , double *vidot20, double *vi21, double *vi22
                   , double *vidot22, double *RP, int *IP, double *RS
                 , int *IS);
extern void ebvs01in_(int *n);
extern void ebvs01_(int *n, double *ve0, double *ve1, double *ve2
                   , double *vi3, double *vi4);
extern void ebzv01in_(int *n, double *y0);
extern void eb3n03in_(int *n);
extern void sigvsat0in_(int *n, int *IS);
extern void sigvsat0_(int *n, double *ve0, double *ve1, double *ve2
                   , double *ve3, int *IS, int *flag);
extern void sgn10in_(int *n);
extern void splt0in_(int *n);
extern void ebct00in_(int *n, double *RP, int *IP, double *RS
                 , double *y0);
extern void ebct00_(int *n, double *ve0, double *ve1, double *vi2
                   , double *RP, int *IP, double *RS);
extern void jun3min_(int *n);
extern void pid001in_(int *n, double *RP, int *IP, int *IS, double *y0
                   , double *y1, double *y2, double *y3, double *y4
                   , double *y5, double *y6, double *y7, double *y8
                   , double *y9, double *y10, double *y11);
extern void pid001_(int *n, double *port_1_v1, double *port_2_v1
                   , double *int_v1, double *int_dv1, double *int_v2
                   , double *int_dv2, double *int_v3, double *int_v4
                   , double *int_v5, double *int_v6, double *int_v7
                   , double *int_v8, double *int_v9, double *int_v10
                   , double *int_v11, double *int_v12, double *int_v13
                   , double *int_v14, double *RP, int *IP, int *IS
                 , int *flag);
extern void mecads1bin_(int *n, double *RP, int *IP, double *RS
                 );
extern void rack01ain_(int *n, double *RP);
extern void mecrn10ain_(int *n, double *RP, int *IP, char **TP
                 , int *IS, double *y0, double *y1);
extern void mecrn10a_(int *n, double *ve0, double *ve1, double *ve2
                   , double *ve3, double *vi4, double *vi5, double *vidot5
                   , double *vi6, double *vi7, double *vidot7
                   , double *RP, int *IP, char **TP, int *IS);
extern void sd0000ain_(int *n, double *RP, int *IP, double *RS
                 , int *IS, double *y0, double *y1, double *y2
                   , double *y3);
extern void sd0000a_(int *n, double *ve0, double *ve1, double *ve2
                   , double *ve3, double *ve4, double *vi5, double *vi6
                   , double *vi7, double *vi8, double *vi9, double *vidot9
                   , double *vi10, double *vi11, double *vidot11
                   , double *vi12, double *vidot12, double *vi13
                   , double *vi14, double *vidot14, double *RP
                 , int *IP, double *RS, int *IS);
extern void mecmas21in_(int *n, double *RP, int *IP, double *RS
                 , int *IS, double *y0, double *y1, double *y2
                   , double *y3, double *y4, double *y5, double *y6
                   , double *y7, double *y8, double *y9, double *y10
                   , double *y11);
extern void mecmas21_(int *n, double *ve0, double *vedot0, double *ve1
                   , double *vedot1, double *ve2, double *ve3
                   , double *ve4, double *vi5, double *vi6, double *vi7
                   , double *vi8, double *vi9, double *vi10, double *vidot10
                   , double *vi11, double *vi12, double *vidot12
                   , double *vi13, double *vidot13, double *vi14
                   , double *vi15, double *vidot15, double *vi16
                   , double *vidot16, double *vi17, double *vi18
                   , double *vidot18, double *vi19, double *vidot19
                   , double *vi20, double *vi21, double *vidot21
                   , double *vi22, double *vi23, double *vidot23
                   , double *vi24, double *vidot24, double *RP
                 , int *IP, double *RS, int *IS);
extern void mecds0ain_(int *n, double *RP, int *IP, double *RS
                 );
extern void f000in_(int *n, double *y0);
extern void ssinkin_(int *n);
extern void ga00in_(int *n, double *RP, double *y0);
extern void ga00_(int *n, double *ve0, double *ve1, double *vi2
                   , double *RP, int *flag);
extern void ud00in_(int *n, double *RP, int *IP, double *RS, int *IS
                 );
extern void ud00_(int *n, double *ve0, double *RP, int *IP, double *RS
                 , int *IS, int *flag, double *t);



/* ******************    Here comes the functions ************ */
static void PreInitialize(AMESIMSYSTEM *amesys, double *y)
{
   int n = 0;
   double *v = amesys->v;
   double *Z = amesys->discrete_states;
   double *dbk_wk = amesys->pModel->dbk_wk;


}

static void Initialize(AMESIMSYSTEM *amesys, double *y)
{
   int n;
   double *v = amesys->v;
   double *Z = amesys->discrete_states;
   double *dbk_wk = amesys->pModel->dbk_wk;


   n = 1;
   w000in_(&n, &v[9], &v[10]);

   n = 1;
   cons00in_(&n, RP3, &Z[0]);

   n = 1;
   ebzv01in_(&n, &v[45]);

   n = 2;
   cons00in_(&n, RP9, &Z[1]);

   n = 1;
   splt0in_(&n);

   n = 3;
   cons00in_(&n, RP15, &Z[12]);

   n = 2;
   splt0in_(&n);

   n = 1;
   f000in_(&n, &v[110]);

   n = 1;
   ud00in_(&n, RP30, IP30, RS30, IS30);

   n = 2;
   w000in_(&n, &v[144], &v[145]);

   n = 4;
   cons00in_(&n, RP35, &Z[25]);

   n = 2;
   ebzv01in_(&n, &v[180]);

   n = 5;
   cons00in_(&n, RP41, &Z[26]);

   n = 3;
   splt0in_(&n);

   n = 6;
   cons00in_(&n, RP47, &Z[37]);

   n = 4;
   splt0in_(&n);

   n = 2;
   f000in_(&n, &v[245]);

   n = 2;
   ud00in_(&n, RP63, IP63, RS63, IS63);

   n = 3;
   w000in_(&n, &v[279], &v[280]);

   n = 7;
   cons00in_(&n, RP67, &Z[50]);

   n = 3;
   ebzv01in_(&n, &v[315]);

   n = 8;
   cons00in_(&n, RP73, &Z[51]);

   n = 5;
   splt0in_(&n);

   n = 9;
   cons00in_(&n, RP79, &Z[62]);

   n = 6;
   splt0in_(&n);

   n = 3;
   f000in_(&n, &v[378]);

   n = 3;
   ud00in_(&n, RP94, IP94, RS94, IS94);

   n = 1;
   thtc0in_(&n);

   n = 1;
   eb3n03in_(&n);

   n = 1;
   sgn10in_(&n);

   n = 2;
   sgn10in_(&n);

   n = 2;
   thtc0in_(&n);

   n = 2;
   eb3n03in_(&n);

   n = 3;
   sgn10in_(&n);

   n = 4;
   sgn10in_(&n);

   n = 3;
   thtc0in_(&n);

   n = 3;
   eb3n03in_(&n);

   n = 5;
   sgn10in_(&n);

   n = 6;
   sgn10in_(&n);

   n = 1;
   mecds0ain_(&n, RP23, IP23, RS23);

   n = 1;
   ssinkin_(&n);

   n = 1;
   ga00in_(&n, RP28, &Z[23]);

   n = 2;
   mecds0ain_(&n, RP55, IP55, RS55);

   n = 2;
   ssinkin_(&n);

   n = 3;
   ga00in_(&n, RP60, &Z[48]);

   n = 3;
   mecds0ain_(&n, RP86, IP86, RS86);

   n = 3;
   ssinkin_(&n);

   n = 5;
   ga00in_(&n, RP91, &Z[73]);

   n = 1;
   sd0000ain_(&n, RP21, IP21, RS21, IS21, NULL, NULL, NULL, NULL
      );

   n = 1;
   mecmas21in_(&n, RP22, IP22, RS22, IS22, &y[4], &y[5], NULL
      , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   n = 2;
   jun3min_(&n);

   n = 2;
   ga00in_(&n, RP29, &Z[24]);

   n = 2;
   sd0000ain_(&n, RP53, IP53, RS53, IS53, NULL, NULL, NULL, NULL
      );

   n = 2;
   mecmas21in_(&n, RP54, IP54, RS54, IS54, &y[12], &y[13], NULL
      , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   n = 4;
   jun3min_(&n);

   n = 4;
   ga00in_(&n, RP61, &Z[49]);

   n = 3;
   sd0000ain_(&n, RP84, IP84, RS84, IS84, NULL, NULL, NULL, NULL
      );

   n = 3;
   mecmas21in_(&n, RP85, IP85, RS85, IS85, &y[20], &y[21], NULL
      , NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   n = 6;
   jun3min_(&n);

   n = 6;
   ga00in_(&n, RP92, &Z[74]);

   n = 1;
   rack01ain_(&n, RP19);

   n = 1;
   mecrn10ain_(&n, RP20, IP20, TP20, IS20, NULL, NULL);

   n = 2;
   {


      pid001in_(&n, RP26, IP26, IS26, &y[6], &y[7], &Z[13], &Z[14]
         , &Z[15], &Z[16], &Z[17], &Z[18], &Z[19], &Z[20], &Z[21]
         , &Z[22]);
   }

   n = 2;
   sigvsat0in_(&n, IS31);

   n = 2;
   rack01ain_(&n, RP51);

   n = 2;
   mecrn10ain_(&n, RP52, IP52, TP52, IS52, NULL, NULL);

   n = 4;
   {


      pid001in_(&n, RP58, IP58, IS58, &y[14], &y[15], &Z[38], &Z[39]
         , &Z[40], &Z[41], &Z[42], &Z[43], &Z[44], &Z[45], &Z[46]
         , &Z[47]);
   }

   n = 4;
   sigvsat0in_(&n, IS62);

   n = 3;
   rack01ain_(&n, RP83);

   n = 6;
   {


      pid001in_(&n, RP89, IP89, IS89, &y[22], &y[23], &Z[63], &Z[64]
         , &Z[65], &Z[66], &Z[67], &Z[68], &Z[69], &Z[70], &Z[71]
         , &Z[72]);
   }

   n = 6;
   sigvsat0in_(&n, IS93);

   n = 3;
   mecrn10ain_(&n, RP95, IP95, TP95, IS95, NULL, NULL);

   n = 1;
   jun3min_(&n);

   n = 1;
   {


      pid001in_(&n, RP14, IP14, IS14, NULL, &y[3], &Z[2], &Z[3]
         , &Z[4], &Z[5], &Z[6], &Z[7], &Z[8], &Z[9], &Z[10], &Z[11]
         );
   }

   n = 3;
   jun3min_(&n);

   n = 3;
   {


      pid001in_(&n, RP46, IP46, IS46, NULL, &y[11], &Z[27], &Z[28]
         , &Z[29], &Z[30], &Z[31], &Z[32], &Z[33], &Z[34], &Z[35]
         , &Z[36]);
   }

   n = 5;
   jun3min_(&n);

   n = 5;
   {


      pid001in_(&n, RP78, IP78, IS78, NULL, &y[19], &Z[52], &Z[53]
         , &Z[54], &Z[55], &Z[56], &Z[57], &Z[58], &Z[59], &Z[60]
         , &Z[61]);
   }

   n = 1;
   sigvsat0in_(&n, IS8);

   n = 3;
   sigvsat0in_(&n, IS40);

   n = 5;
   sigvsat0in_(&n, IS72);

   n = 1;
   ebvs01in_(&n);

   n = 1;
   ebct00in_(&n, RP12, IP12, RS12, &v[53]);

   n = 2;
   ebvs01in_(&n);

   n = 2;
   ebct00in_(&n, RP44, IP44, RS44, &v[188]);

   n = 3;
   ebvs01in_(&n);

   n = 3;
   ebct00in_(&n, RP76, IP76, RS76, &v[323]);

   n = 1;
   emdpmdc01in_(&n, RP0, IS0, &y[0], NULL, NULL, NULL, NULL);

   n = 1;
   mecrl0ain_(&n, RP4, IP4, RS4, IS4, &y[1], &y[2], NULL, NULL
      , NULL, NULL, NULL, NULL, NULL, NULL);

   n = 1;
   mecads1bin_(&n, RP18, IP18, RS18);

   n = 2;
   emdpmdc01in_(&n, RP32, IS32, &y[8], NULL, NULL, NULL, NULL
      );

   n = 2;
   mecrl0ain_(&n, RP36, IP36, RS36, IS36, &y[9], &y[10], NULL
      , NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   n = 2;
   mecads1bin_(&n, RP50, IP50, RS50);

   n = 3;
   emdpmdc01in_(&n, RP64, IS64, &y[16], NULL, NULL, NULL, NULL
      );

   n = 3;
   mecrl0ain_(&n, RP68, IP68, RS68, IS68, &y[17], &y[18], NULL
      , NULL, NULL, NULL, NULL, NULL, NULL, NULL);

   n = 3;
   mecads1bin_(&n, RP82, IP82, RS82);

}

static void localFuncEval(AMESIMSYSTEM *amesys, double t, double *y, double *yprime, double *delta, int *flag)
{
   int sflag, oflag, n, panic, i=0;
   int *oldflag, *newflag;
   double *v = amesys->v;
   double *Z = amesys->discrete_states;
   double *input = amesys->inputs;
   double *output = amesys->outputs;
   double *dbk_wk = amesys->pModel->dbk_wk;
   
#if(AME_MODEL_ISEXPLICIT == 1)
   double *dot = yprime;
   
#if(AME_HAS_ENABLED_SUBMODEL == 1)
   memset(dot,0,AME_NBOF_SOLVER_STATES*sizeof(double));
#elif (AME_NBOF_EXPLICIT_STATE == 0)
   dot[0] = 0.0;
#endif

#elif( AME_NBOF_EXPLICIT_STATE > 0 )  
   double dot[AME_NBOF_EXPLICIT_STATE];
   
   /* Initialize the dot vector to the yprime vector. */
   memcpy((void *)dot, (void *)yprime, AME_NBOF_EXPLICIT_STATE*sizeof(double));  
#endif    
   
   SetGlobalSystem(amesys);

#if(defined(AME_COSIM) && (AME_NBOF_INPUTS > 0))
   if(amesys->doInterpol) {
      if( getInputsCosimSlave(amesys->csSlave, t, input) != AME_NO_ERROR ) {
         AmeExit(1);
      }
   }
#endif
   
#if(AME_MODEL_ISEXPLICIT == 0)
   /* Initialize the residuals for the implicits to the derivatives of the implicits. */
   for(i=AME_NBOF_EXPLICIT_STATE;i<AME_NBOF_SOLVER_STATES;i++)
   {
      delta[i] = yprime[i];
   }
#endif
   
   /* Record old value of flag (oflag) and set 
      flag value for use in submodels (sflag).
      Also get addresses of main discontinuity flags. */

   oflag = *flag;
   sflag = *flag;

   if(amesys->first_call)
   {
      GetFlagAddresses(&amesys->oldflag, &amesys->newflag);
   }
   oldflag = amesys->oldflag;
   newflag = amesys->newflag;

   /* Initialize everything ready for potential calls to stepdn
      in submodels. */

   panic = 0;
   getredstep();

   if(isstabrun_())
   {
      t = amesys->simOptions->fixedTime;
   }
   else if(*flag == 2)
   {
      /* Record current simulation time for message passing. */
 
      SetSimTime(t);
   }
   /* Record current simulation time for ametim_(). */

   SetTimeAtThisStep(t);

   if (holdinputs_())
   {
      /* We reset artificially the time to the initial value
         to give the illusion of held inputs. */

      t = getstarttime_();
   }
   /* Assign the state variables y[] calculated by the integrator 
      to the appropriate variables v[]. */

   /* Assign continuous state variables calculated by the integrator */
#if( (AME_MODEL_ISEXPLICIT == 0) && (AME_NBOF_SOLVER_STATES > 0) )
   {
      int idxState;
      for(idxState = 0; idxState < AME_NBOF_SOLVER_STATES; idxState++) {
         v[GcontStateVarNum[idxState]] = y[idxState];
      }
   }
#elif( (AME_MODEL_ISEXPLICIT == 1) && (AME_NBOF_EXPLICIT_STATE > 0) )
   {
      int idxState;
      for(idxState = 0; idxState < AME_NBOF_EXPLICIT_STATE; idxState++) {
         v[GcontStateVarNum[idxState]] = y[idxState];
      }
   }
#endif

   /* Assign discrete state variables */
#if( AME_NBOF_DISCRETE_STATE > 0 )
   {
      int idxState;
      for(idxState = 0; idxState < AME_NBOF_DISCRETE_STATE; idxState++) {
         v[GdiscStateVarNum[idxState]] = Z[idxState];
      }
   }
#endif
   
   /* Assign the interface input variables to the appropriate variable v(). */
#if(AME_NBOF_INPUTS > 0)
   {
      int idxInput;
      for(idxInput = 0; idxInput < AME_NBOF_INPUTS; idxInput++) {
         v[GInputVarNum[idxInput]] = input[idxInput];
      }
   }
#endif

#if(AME_MODEL_ISEXPLICIT == 1)
  /* The following call ensures that lsoda does not integrate past
      time amesys->t_end_of_time_slice. This does not matter in a standard AMESim run but is
      very important with cosimulation. */
  
#ifdef AME_COSIM  
   *oldflag = *newflag = sflag;
   sdistim_(&amesys->t_end_of_time_slice);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys, flag,&sflag,&oflag,&panic,"_Cosimulation",1);
#endif

#else
   if(*flag == 5)
   {
      DPerturbIfNecessary(amesys, flag);
   }
#endif   
	 
   /* Call submodel calculation subroutine in the order 
      that ensures the input requirements of each submodel
      are satisfied. */

   v[4] = -v[13];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[6] = v[13];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[21], &v[22], RP3, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",1);

   v[5] = v[45] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[37] = v[45] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[49], &v[50], RP9, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",2);

   v[51] = v[49] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[46] = v[49] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[39] = v[4] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[52] = -RP12[1]*(RS12[0]*v[4]-RP12[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[69], &v[70], RP15, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",3);

   v[72] = v[69] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[73] = v[69] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[96] = v[110];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   ud00_(&n, &v[132], RP30, IP30, RS30, IS30, &sflag, &t);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"UD00",1);

   v[139] = -v[148];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[141] = v[148];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 4;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[156], &v[157], RP35, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",4);

   v[140] = v[180] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[172] = v[180] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 5;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[184], &v[185], RP41, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",5);

   v[186] = v[184] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[181] = v[184] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[174] = v[139] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[187] = -RP44[1]*(RS44[0]*v[139]-RP44[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 6;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[204], &v[205], RP47, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",6);

   v[207] = v[204] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[208] = v[204] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[231] = v[245];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   ud00_(&n, &v[267], RP63, IP63, RS63, IS63, &sflag, &t);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"UD00",2);

   v[274] = -v[283];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[276] = v[283];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 7;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[291], &v[292], RP67, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",7);

   v[275] = v[315] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[307] = v[315] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 8;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[319], &v[320], RP73, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",8);

   v[321] = v[319] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[316] = v[319] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[309] = v[274] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[322] = -RP76[1]*(RS76[0]*v[274]-RP76[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 9;
   *oldflag = *newflag = sflag;
   cons00_(&n, &v[339], &v[340], RP79, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"CONS00",9);

   v[342] = v[339] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[343] = v[339] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[364] = v[378];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   ud00_(&n, &v[400], RP94, IP94, RS94, IS94, &sflag, &t);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"UD00",3);

   v[7] = v[21];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[38] = v[39] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[44] = v[6]+v[38];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[48] = -v[51] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[71] = -v[72] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[142] = v[156];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[173] = v[174] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[179] = v[141]+v[173];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[183] = -v[186] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[206] = -v[207] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[277] = v[291];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[308] = v[309] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[314] = v[276]+v[308];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[318] = -v[321] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[341] = -v[342] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[8] = RP0[1]*(1+RP0[2]*(v[7]-RP0[0]))*v[13]*v[13];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[143] = RP32[1]*(1+RP32[2]*(v[142]-RP32[0]))*v[148]*v[148];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[278] = RP64[1]*(1+RP64[2]*(v[277]-RP64[0]))*v[283]*v[283];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[0] = -v[24] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[1] = -v[25] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[74] = RP18[1]*(RS18[0]*v[25]-RP18[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[76] = v[24];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[77] = v[25];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[81] = (RP20[0]*((v[76]*1.0471975511966e-01)))/1.0471975511966e-01;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[82] = (RP20[0]*((v[77]*1.74532925199433e-02)))/1.74532925199433e-02;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[86] = -v[97] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[87] = -v[98] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[111] = v[97];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[112] = v[98];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[113] = RP23[1]*(RS23[0]*v[98]-RP23[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   ga00_(&n, &v[130], &v[74], &v[133], RP28, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"GA00",1);

   v[135] = -v[159] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[136] = -v[160] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[209] = RP50[1]*(RS50[0]*v[160]-RP50[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[211] = v[159];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[212] = v[160];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[216] = (RP52[0]*((v[211]*1.0471975511966e-01)))/1.0471975511966e-01;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[217] = (RP52[0]*((v[212]*1.74532925199433e-02)))/1.74532925199433e-02;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[221] = -v[232] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[222] = -v[233] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[246] = v[232];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[247] = v[233];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[248] = RP55[1]*(RS55[0]*v[233]-RP55[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   ga00_(&n, &v[265], &v[209], &v[268], RP60, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"GA00",3);

   v[270] = -v[294] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[271] = -v[295] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[344] = RP82[1]*(RS82[0]*v[295]-RP82[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[346] = v[294];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[347] = v[295];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[354] = -v[365] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[355] = -v[366] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[379] = v[365];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[380] = v[366];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[381] = RP86[1]*(RS86[0]*v[366]-RP86[0]);
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 5;
   *oldflag = *newflag = sflag;
   ga00_(&n, &v[398], &v[344], &v[401], RP91, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"GA00",5);

   v[351] = (RP95[0]*((v[346]*1.0471975511966e-01)))/1.0471975511966e-01;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[352] = (RP95[0]*((v[347]*1.74532925199433e-02)))/1.74532925199433e-02;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   v[14] = emdpmdc01_macro0_(&n, &v[0], &v[9], RP0, IS0);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EMDPMDC01",1);

   v[79] = (v[81]*2*3.141592653589793/60)*RP19[0]*1e-3;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[80] = ((v[82])-RP19[1])*3.141592653589793/180*RP19[0]*1e-3;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   sd0000a_(&n, &v[78], &v[79], &v[80], &v[86], &v[87], &v[90]
      , &v[91], &v[92], &v[93], NULL, NULL, &v[94], NULL, NULL
      , NULL, NULL, &v[95], NULL, NULL, RP21, IP21, RS21, IS21
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SD0000A",1);

   v[89] = v[78] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   mecmas21_(&n, &v[97], &dot[4], &v[98], &dot[5], &v[99], &v[96]
      , &v[89], &v[100], &v[101], &v[102], &v[103], &v[104], NULL
      , NULL, &v[105], NULL, NULL, NULL, NULL, &v[106], NULL, NULL
      , NULL, NULL, &v[107], NULL, NULL, NULL, NULL, &v[108], NULL
      , NULL, &v[109], NULL, NULL, NULL, NULL, RP22, IP22, RS22
      , IS22);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECMAS21",1);

   v[88] = -v[99] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[131] = v[132]-v[130];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   ga00_(&n, &v[115], &v[131], &v[134], RP29, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"GA00",2);

   n = 2;
   *oldflag = *newflag = sflag;
   v[149] = emdpmdc01_macro0_(&n, &v[135], &v[144], RP32, IS32
         );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EMDPMDC01",2);

   v[214] = (v[216]*2*3.141592653589793/60)*RP51[0]*1e-3;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[215] = ((v[217])-RP51[1])*3.141592653589793/180*RP51[0]*1e-3;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   sd0000a_(&n, &v[213], &v[214], &v[215], &v[221], &v[222], &v[225]
      , &v[226], &v[227], &v[228], NULL, NULL, &v[229], NULL, NULL
      , NULL, NULL, &v[230], NULL, NULL, RP53, IP53, RS53, IS53
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SD0000A",2);

   v[224] = v[213] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   mecmas21_(&n, &v[232], &dot[12], &v[233], &dot[13], &v[234]
      , &v[231], &v[224], &v[235], &v[236], &v[237], &v[238], &v[239]
      , NULL, NULL, &v[240], NULL, NULL, NULL, NULL, &v[241], NULL
      , NULL, NULL, NULL, &v[242], NULL, NULL, NULL, NULL, &v[243]
      , NULL, NULL, &v[244], NULL, NULL, NULL, NULL, RP54, IP54
      , RS54, IS54);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECMAS21",2);

   v[223] = -v[234] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[266] = v[267]-v[265];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 4;
   *oldflag = *newflag = sflag;
   ga00_(&n, &v[250], &v[266], &v[269], RP61, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"GA00",4);

   n = 3;
   *oldflag = *newflag = sflag;
   v[284] = emdpmdc01_macro0_(&n, &v[270], &v[279], RP64, IS64
         );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EMDPMDC01",3);

   v[349] = (v[351]*2*3.141592653589793/60)*RP83[0]*1e-3;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[350] = ((v[352])-RP83[1])*3.141592653589793/180*RP83[0]*1e-3;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   sd0000a_(&n, &v[348], &v[349], &v[350], &v[354], &v[355], &v[358]
      , &v[359], &v[360], &v[361], NULL, NULL, &v[362], NULL, NULL
      , NULL, NULL, &v[363], NULL, NULL, RP84, IP84, RS84, IS84
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SD0000A",3);

   v[357] = v[348] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   mecmas21_(&n, &v[365], &dot[20], &v[366], &dot[21], &v[367]
      , &v[364], &v[357], &v[368], &v[369], &v[370], &v[371], &v[372]
      , NULL, NULL, &v[373], NULL, NULL, NULL, NULL, &v[374], NULL
      , NULL, NULL, NULL, &v[375], NULL, NULL, NULL, NULL, &v[376]
      , NULL, NULL, &v[377], NULL, NULL, NULL, NULL, RP85, IP85
      , RS85, IS85);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECMAS21",3);

   v[356] = -v[367] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[399] = v[400]-v[398];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 6;
   *oldflag = *newflag = sflag;
   ga00_(&n, &v[383], &v[399], &v[402], RP92, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"GA00",6);

   v[83] = RP19[0]*1e-3*v[78];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   mecrn10a_(&n, &v[75], &v[76], &v[77], &v[83], &v[84], NULL
      , NULL, &v[85], NULL, NULL, RP20, IP20, TP20, IS20);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECRN10A",1);

   n = 2;
   *oldflag = *newflag = sflag;
   {  /* DBK specific start. */


      pid001_(&n, &v[114], &v[115], &v[116], &dot[6], &v[117]
      , &dot[7], &v[118], &v[119], &v[120], &v[121], &v[122], &v[123]
      , &v[124], &v[125], &v[126], &v[127], &v[128], &v[129], RP26
      , IP26, IS26, &sflag);
   }
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"PID001",2);

   n = 2;
   *oldflag = *newflag = sflag;
   sigvsat0_(&n, &v[56], &v[73], &v[114], &v[71], IS31, &sflag
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SIGVSAT0",2);

   v[218] = RP51[0]*1e-3*v[213];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   mecrn10a_(&n, &v[210], &v[211], &v[212], &v[218], &v[219], NULL
      , NULL, &v[220], NULL, NULL, RP52, IP52, TP52, IS52);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECRN10A",2);

   n = 4;
   *oldflag = *newflag = sflag;
   {  /* DBK specific start. */


      pid001_(&n, &v[249], &v[250], &v[251], &dot[14], &v[252]
      , &dot[15], &v[253], &v[254], &v[255], &v[256], &v[257]
      , &v[258], &v[259], &v[260], &v[261], &v[262], &v[263], &v[264]
      , RP58, IP58, IS58, &sflag);
   }
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"PID001",4);

   n = 4;
   *oldflag = *newflag = sflag;
   sigvsat0_(&n, &v[191], &v[208], &v[249], &v[206], IS62, &sflag
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SIGVSAT0",4);

   v[353] = RP83[0]*1e-3*v[348];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 6;
   *oldflag = *newflag = sflag;
   {  /* DBK specific start. */


      pid001_(&n, &v[382], &v[383], &v[384], &dot[22], &v[385]
      , &dot[23], &v[386], &v[387], &v[388], &v[389], &v[390]
      , &v[391], &v[392], &v[393], &v[394], &v[395], &v[396], &v[397]
      , RP89, IP89, IS89, &sflag);
   }
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"PID001",6);

   n = 6;
   *oldflag = *newflag = sflag;
   sigvsat0_(&n, &v[326], &v[343], &v[382], &v[341], IS93, &sflag
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SIGVSAT0",6);

   n = 3;
   *oldflag = *newflag = sflag;
   mecrn10a_(&n, &v[345], &v[346], &v[347], &v[353], &v[403], NULL
      , NULL, &v[404], NULL, NULL, RP95, IP95, TP95, IS95);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECRN10A",3);

   v[55] = v[56]-v[52];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   {  /* DBK specific start. */


      pid001_(&n, &v[47], &v[55], NULL, NULL, &v[57], &dot[3]
      , &v[58], NULL, &v[59], &v[60], &v[61], &v[62], &v[63], &v[64]
      , &v[65], &v[66], &v[67], &v[68], RP14, IP14, IS14, &sflag
      );
   }
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"PID001",1);

   v[23] = v[75];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[190] = v[191]-v[187];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   {  /* DBK specific start. */


      pid001_(&n, &v[182], &v[190], NULL, NULL, &v[192], &dot[11]
      , &v[193], NULL, &v[194], &v[195], &v[196], &v[197], &v[198]
      , &v[199], &v[200], &v[201], &v[202], &v[203], RP46, IP46
      , IS46, &sflag);
   }
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"PID001",3);

   v[158] = v[210];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[325] = v[326]-v[322];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 5;
   *oldflag = *newflag = sflag;
   {  /* DBK specific start. */


      pid001_(&n, &v[317], &v[325], NULL, NULL, &v[327], &dot[19]
      , &v[328], NULL, &v[329], &v[330], &v[331], &v[332], &v[333]
      , &v[334], &v[335], &v[336], &v[337], &v[338], RP78, IP78
      , IS78, &sflag);
   }
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"PID001",5);

   v[293] = v[345];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   sigvsat0_(&n, &v[41], &v[46], &v[47], &v[48], IS8, &sflag);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SIGVSAT0",1);

   n = 3;
   *oldflag = *newflag = sflag;
   sigvsat0_(&n, &v[176], &v[181], &v[182], &v[183], IS40, &sflag
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SIGVSAT0",3);

   n = 5;
   *oldflag = *newflag = sflag;
   sigvsat0_(&n, &v[311], &v[316], &v[317], &v[318], IS72, &sflag
      );
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"SIGVSAT0",5);

   n = 1;
   *oldflag = *newflag = sflag;
   ebvs01_(&n, &v[37], &v[39], &v[41], &v[42], &v[43]);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EBVS01",1);

   v[40] = v[41]+v[37];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   ebct00_(&n, &v[40], &v[4], &v[54], RP12, IP12, RS12);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EBCT00",1);

   v[3] = v[40] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   ebvs01_(&n, &v[172], &v[174], &v[176], &v[177], &v[178]);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EBVS01",2);

   v[175] = v[176]+v[172];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   ebct00_(&n, &v[175], &v[139], &v[189], RP44, IP44, RS44);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EBCT00",2);

   v[138] = v[175] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   ebvs01_(&n, &v[307], &v[309], &v[311], &v[312], &v[313]);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EBVS01",3);

   v[310] = v[311]+v[307];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   ebct00_(&n, &v[310], &v[274], &v[324], RP76, IP76, RS76);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EBCT00",3);

   v[273] = v[310] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   emdpmdc01_(&n, &v[0], &v[3], &v[5], &v[7], &v[9], &v[12], &v[13]
      , &dot[0], &v[14], &v[15], &v[16], &v[17], &v[18], NULL
      , NULL, &v[19], NULL, NULL, NULL, NULL, &v[20], NULL, NULL
      , RP0, IS0);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EMDPMDC01",1);

   v[2] = v[15];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[11] = v[2] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 1;
   *oldflag = *newflag = sflag;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECRL0A",1);

   n = 2;
   *oldflag = *newflag = sflag;
   emdpmdc01_(&n, &v[135], &v[138], &v[140], &v[142], &v[144]
      , &v[147], &v[148], &dot[8], &v[149], &v[150], &v[151], &v[152]
      , &v[153], NULL, NULL, &v[154], NULL, NULL, NULL, NULL, &v[155]
      , NULL, NULL, RP32, IS32);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EMDPMDC01",2);

   v[137] = v[150];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[146] = v[137] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 2;
   *oldflag = *newflag = sflag;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECRL0A",2);

   n = 3;
   *oldflag = *newflag = sflag;
   emdpmdc01_(&n, &v[270], &v[273], &v[275], &v[277], &v[279]
      , &v[282], &v[283], &dot[16], &v[284], &v[285], &v[286]
      , &v[287], &v[288], NULL, NULL, &v[289], NULL, NULL, NULL
      , NULL, &v[290], NULL, NULL, RP64, IS64);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"EMDPMDC01",3);

   v[272] = v[285];
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   v[281] = v[272] /* Duplicate variable. */;
   AME_POST_SUBMODCALL_NO_DISCON(amesys,flag);

   n = 3;
   *oldflag = *newflag = sflag;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
   AME_POST_SUBMODCALL_WITH_DISCON(amesys,flag,&sflag,&oflag,&panic,"MECRL0A",3);


   /* Set interface outputs here. */
#if(AME_NBOF_OUTPUTS > 0)
   {
      int idxOutput;
      for(idxOutput = 0; idxOutput < AME_NBOF_OUTPUTS; idxOutput++) {
         output[idxOutput] = v[GOutputVarNum[idxOutput]];
      }
   }
#endif

#if(AME_MODEL_ISEXPLICIT == 1)
   applyStateStatus(dot,AME_NBOF_SOLVER_STATES);
#elif( AME_NBOF_EXPLICIT_STATE > 0)
   applyStateStatus(dot,AME_NBOF_EXPLICIT_STATE);

   for(i=0;i<AME_NBOF_EXPLICIT_STATE;i++)
   {
      delta[i] = dot[i] - yprime[i];
   }
#endif

   if(*flag == 0)
   {
      /* It is an initialization call and the user
         is permitted to change the state variables
         and discrete variables. */
      updateStatesFromModel(amesys, y, AME_CONTINUOUS_STATE|AME_DISCRETE_STATE);
   }

#if( AME_NBOF_DISCRETE_STATE > 0)
   if(is_sample_time()) {
      /* Change discrete variables */
      updateStatesFromModel(amesys, y, AME_DISCRETE_STATE);
   }
#endif

   UpFECount(amesys);

   amesys->first_call = 0;
}

static void localJFuncEval_0(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[13] = y[col];
   v[4] = -v[13];
   v[6] = v[13];
   v[39] = v[4] /* Duplicate variable. */;
   v[52] = -RP12[1]*(RS12[0]*v[4]-RP12[0]);
   v[38] = v[39] /* Duplicate variable. */;
   v[44] = v[6]+v[38];
   v[8] = RP0[1]*(1+RP0[2]*(v[7]-RP0[0]))*v[13]*v[13];
   v[55] = v[56]-v[52];
   n = 1;
   {  /* DBK specific start. */


      pid001_(&n, &v[47], &v[55], NULL, NULL, &v[57], &dot[3]
      , &v[58], NULL, &v[59], &v[60], &v[61], &v[62], &v[63], &v[64]
      , &v[65], &v[66], &v[67], &v[68], RP14, IP14, IS14, &sflag
      );
   }
   n = 1;
   sigvsat0_(&n, &v[41], &v[46], &v[47], &v[48], IS8, &sflag);
   n = 1;
   ebvs01_(&n, &v[37], &v[39], &v[41], &v[42], &v[43]);
   v[40] = v[41]+v[37];
   n = 1;
   ebct00_(&n, &v[40], &v[4], &v[54], RP12, IP12, RS12);
   v[3] = v[40] /* Duplicate variable. */;
   n = 1;
   emdpmdc01_(&n, &v[0], &v[3], &v[5], &v[7], &v[9], &v[12], &v[13]
      , &dot[0], &v[14], &v[15], &v[16], &v[17], &v[18], NULL
      , NULL, &v[19], NULL, NULL, NULL, NULL, &v[20], NULL, NULL
      , RP0, IS0);
   v[2] = v[15];
   v[11] = v[2] /* Duplicate variable. */;
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_1(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[24] = y[col];
   v[0] = -v[24] /* Duplicate variable. */;
   v[76] = v[24];
   v[81] = (RP20[0]*((v[76]*1.0471975511966e-01)))/1.0471975511966e-01;
   n = 1;
   v[14] = emdpmdc01_macro0_(&n, &v[0], &v[9], RP0, IS0);
   v[79] = (v[81]*2*3.141592653589793/60)*RP19[0]*1e-3;
   n = 1;
   sd0000a_(&n, &v[78], &v[79], &v[80], &v[86], &v[87], &v[90]
      , &v[91], &v[92], &v[93], NULL, NULL, &v[94], NULL, NULL
      , NULL, NULL, &v[95], NULL, NULL, RP21, IP21, RS21, IS21
      );
   v[89] = v[78] /* Duplicate variable. */;
   n = 1;
   mecmas21_(&n, &v[97], &dot[4], &v[98], &dot[5], &v[99], &v[96]
      , &v[89], &v[100], &v[101], &v[102], &v[103], &v[104], NULL
      , NULL, &v[105], NULL, NULL, NULL, NULL, &v[106], NULL, NULL
      , NULL, NULL, &v[107], NULL, NULL, NULL, NULL, &v[108], NULL
      , NULL, &v[109], NULL, NULL, NULL, NULL, RP22, IP22, RS22
      , IS22);
   v[88] = -v[99] /* Duplicate variable. */;
   v[83] = RP19[0]*1e-3*v[78];
   n = 1;
   mecrn10a_(&n, &v[75], &v[76], &v[77], &v[83], &v[84], NULL
      , NULL, &v[85], NULL, NULL, RP20, IP20, TP20, IS20);
   v[23] = v[75];
   n = 1;
   emdpmdc01_(&n, &v[0], &v[3], &v[5], &v[7], &v[9], &v[12], &v[13]
      , &dot[0], &v[14], &v[15], &v[16], &v[17], &v[18], NULL
      , NULL, &v[19], NULL, NULL, NULL, NULL, &v[20], NULL, NULL
      , RP0, IS0);
   v[2] = v[15];
   v[11] = v[2] /* Duplicate variable. */;
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_2(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[25] = y[col];
   v[1] = -v[25] /* Duplicate variable. */;
   v[74] = RP18[1]*(RS18[0]*v[25]-RP18[0]);
   v[77] = v[25];
   v[82] = (RP20[0]*((v[77]*1.74532925199433e-02)))/1.74532925199433e-02;
   n = 1;
   ga00_(&n, &v[130], &v[74], &v[133], RP28, &sflag);
   v[80] = ((v[82])-RP19[1])*3.141592653589793/180*RP19[0]*1e-3;
   n = 1;
   sd0000a_(&n, &v[78], &v[79], &v[80], &v[86], &v[87], &v[90]
      , &v[91], &v[92], &v[93], NULL, NULL, &v[94], NULL, NULL
      , NULL, NULL, &v[95], NULL, NULL, RP21, IP21, RS21, IS21
      );
   v[89] = v[78] /* Duplicate variable. */;
   n = 1;
   mecmas21_(&n, &v[97], &dot[4], &v[98], &dot[5], &v[99], &v[96]
      , &v[89], &v[100], &v[101], &v[102], &v[103], &v[104], NULL
      , NULL, &v[105], NULL, NULL, NULL, NULL, &v[106], NULL, NULL
      , NULL, NULL, &v[107], NULL, NULL, NULL, NULL, &v[108], NULL
      , NULL, &v[109], NULL, NULL, NULL, NULL, RP22, IP22, RS22
      , IS22);
   v[88] = -v[99] /* Duplicate variable. */;
   v[131] = v[132]-v[130];
   n = 2;
   ga00_(&n, &v[115], &v[131], &v[134], RP29, &sflag);
   v[83] = RP19[0]*1e-3*v[78];
   n = 1;
   mecrn10a_(&n, &v[75], &v[76], &v[77], &v[83], &v[84], NULL
      , NULL, &v[85], NULL, NULL, RP20, IP20, TP20, IS20);
   n = 2;
   {  /* DBK specific start. */


      pid001_(&n, &v[114], &v[115], &v[116], &dot[6], &v[117]
      , &dot[7], &v[118], &v[119], &v[120], &v[121], &v[122], &v[123]
      , &v[124], &v[125], &v[126], &v[127], &v[128], &v[129], RP26
      , IP26, IS26, &sflag);
   }
   n = 2;
   sigvsat0_(&n, &v[56], &v[73], &v[114], &v[71], IS31, &sflag
      );
   v[55] = v[56]-v[52];
   n = 1;
   {  /* DBK specific start. */


      pid001_(&n, &v[47], &v[55], NULL, NULL, &v[57], &dot[3]
      , &v[58], NULL, &v[59], &v[60], &v[61], &v[62], &v[63], &v[64]
      , &v[65], &v[66], &v[67], &v[68], RP14, IP14, IS14, &sflag
      );
   }
   v[23] = v[75];
   n = 1;
   sigvsat0_(&n, &v[41], &v[46], &v[47], &v[48], IS8, &sflag);
   n = 1;
   ebvs01_(&n, &v[37], &v[39], &v[41], &v[42], &v[43]);
   v[40] = v[41]+v[37];
   n = 1;
   ebct00_(&n, &v[40], &v[4], &v[54], RP12, IP12, RS12);
   v[3] = v[40] /* Duplicate variable. */;
   n = 1;
   emdpmdc01_(&n, &v[0], &v[3], &v[5], &v[7], &v[9], &v[12], &v[13]
      , &dot[0], &v[14], &v[15], &v[16], &v[17], &v[18], NULL
      , NULL, &v[19], NULL, NULL, NULL, NULL, &v[20], NULL, NULL
      , RP0, IS0);
   v[2] = v[15];
   v[11] = v[2] /* Duplicate variable. */;
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_3(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[57] = y[col];
   n = 1;
   {  /* DBK specific start. */


      pid001_(&n, &v[47], &v[55], NULL, NULL, &v[57], &dot[3]
      , &v[58], NULL, &v[59], &v[60], &v[61], &v[62], &v[63], &v[64]
      , &v[65], &v[66], &v[67], &v[68], RP14, IP14, IS14, &sflag
      );
   }
   n = 1;
   sigvsat0_(&n, &v[41], &v[46], &v[47], &v[48], IS8, &sflag);
   n = 1;
   ebvs01_(&n, &v[37], &v[39], &v[41], &v[42], &v[43]);
   v[40] = v[41]+v[37];
   n = 1;
   ebct00_(&n, &v[40], &v[4], &v[54], RP12, IP12, RS12);
   v[3] = v[40] /* Duplicate variable. */;
   n = 1;
   emdpmdc01_(&n, &v[0], &v[3], &v[5], &v[7], &v[9], &v[12], &v[13]
      , &dot[0], &v[14], &v[15], &v[16], &v[17], &v[18], NULL
      , NULL, &v[19], NULL, NULL, NULL, NULL, &v[20], NULL, NULL
      , RP0, IS0);
   v[2] = v[15];
   v[11] = v[2] /* Duplicate variable. */;
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_4(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[97] = y[col];
   v[86] = -v[97] /* Duplicate variable. */;
   v[111] = v[97];
   n = 1;
   sd0000a_(&n, &v[78], &v[79], &v[80], &v[86], &v[87], &v[90]
      , &v[91], &v[92], &v[93], NULL, NULL, &v[94], NULL, NULL
      , NULL, NULL, &v[95], NULL, NULL, RP21, IP21, RS21, IS21
      );
   v[89] = v[78] /* Duplicate variable. */;
   n = 1;
   mecmas21_(&n, &v[97], &dot[4], &v[98], &dot[5], &v[99], &v[96]
      , &v[89], &v[100], &v[101], &v[102], &v[103], &v[104], NULL
      , NULL, &v[105], NULL, NULL, NULL, NULL, &v[106], NULL, NULL
      , NULL, NULL, &v[107], NULL, NULL, NULL, NULL, &v[108], NULL
      , NULL, &v[109], NULL, NULL, NULL, NULL, RP22, IP22, RS22
      , IS22);
   v[88] = -v[99] /* Duplicate variable. */;
   v[83] = RP19[0]*1e-3*v[78];
   n = 1;
   mecrn10a_(&n, &v[75], &v[76], &v[77], &v[83], &v[84], NULL
      , NULL, &v[85], NULL, NULL, RP20, IP20, TP20, IS20);
   v[23] = v[75];
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_5(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[98] = y[col];
   v[87] = -v[98] /* Duplicate variable. */;
   v[112] = v[98];
   v[113] = RP23[1]*(RS23[0]*v[98]-RP23[0]);
   n = 1;
   sd0000a_(&n, &v[78], &v[79], &v[80], &v[86], &v[87], &v[90]
      , &v[91], &v[92], &v[93], NULL, NULL, &v[94], NULL, NULL
      , NULL, NULL, &v[95], NULL, NULL, RP21, IP21, RS21, IS21
      );
   v[89] = v[78] /* Duplicate variable. */;
   n = 1;
   mecmas21_(&n, &v[97], &dot[4], &v[98], &dot[5], &v[99], &v[96]
      , &v[89], &v[100], &v[101], &v[102], &v[103], &v[104], NULL
      , NULL, &v[105], NULL, NULL, NULL, NULL, &v[106], NULL, NULL
      , NULL, NULL, &v[107], NULL, NULL, NULL, NULL, &v[108], NULL
      , NULL, &v[109], NULL, NULL, NULL, NULL, RP22, IP22, RS22
      , IS22);
   v[88] = -v[99] /* Duplicate variable. */;
   v[83] = RP19[0]*1e-3*v[78];
   n = 1;
   mecrn10a_(&n, &v[75], &v[76], &v[77], &v[83], &v[84], NULL
      , NULL, &v[85], NULL, NULL, RP20, IP20, TP20, IS20);
   v[23] = v[75];
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_6(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[116] = y[col];
   n = 2;
   {  /* DBK specific start. */


      pid001_(&n, &v[114], &v[115], &v[116], &dot[6], &v[117]
      , &dot[7], &v[118], &v[119], &v[120], &v[121], &v[122], &v[123]
      , &v[124], &v[125], &v[126], &v[127], &v[128], &v[129], RP26
      , IP26, IS26, &sflag);
   }
   n = 2;
   sigvsat0_(&n, &v[56], &v[73], &v[114], &v[71], IS31, &sflag
      );
   v[55] = v[56]-v[52];
   n = 1;
   {  /* DBK specific start. */


      pid001_(&n, &v[47], &v[55], NULL, NULL, &v[57], &dot[3]
      , &v[58], NULL, &v[59], &v[60], &v[61], &v[62], &v[63], &v[64]
      , &v[65], &v[66], &v[67], &v[68], RP14, IP14, IS14, &sflag
      );
   }
   n = 1;
   sigvsat0_(&n, &v[41], &v[46], &v[47], &v[48], IS8, &sflag);
   n = 1;
   ebvs01_(&n, &v[37], &v[39], &v[41], &v[42], &v[43]);
   v[40] = v[41]+v[37];
   n = 1;
   ebct00_(&n, &v[40], &v[4], &v[54], RP12, IP12, RS12);
   v[3] = v[40] /* Duplicate variable. */;
   n = 1;
   emdpmdc01_(&n, &v[0], &v[3], &v[5], &v[7], &v[9], &v[12], &v[13]
      , &dot[0], &v[14], &v[15], &v[16], &v[17], &v[18], NULL
      , NULL, &v[19], NULL, NULL, NULL, NULL, &v[20], NULL, NULL
      , RP0, IS0);
   v[2] = v[15];
   v[11] = v[2] /* Duplicate variable. */;
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_7(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[117] = y[col];
   n = 2;
   {  /* DBK specific start. */


      pid001_(&n, &v[114], &v[115], &v[116], &dot[6], &v[117]
      , &dot[7], &v[118], &v[119], &v[120], &v[121], &v[122], &v[123]
      , &v[124], &v[125], &v[126], &v[127], &v[128], &v[129], RP26
      , IP26, IS26, &sflag);
   }
   n = 2;
   sigvsat0_(&n, &v[56], &v[73], &v[114], &v[71], IS31, &sflag
      );
   v[55] = v[56]-v[52];
   n = 1;
   {  /* DBK specific start. */


      pid001_(&n, &v[47], &v[55], NULL, NULL, &v[57], &dot[3]
      , &v[58], NULL, &v[59], &v[60], &v[61], &v[62], &v[63], &v[64]
      , &v[65], &v[66], &v[67], &v[68], RP14, IP14, IS14, &sflag
      );
   }
   n = 1;
   sigvsat0_(&n, &v[41], &v[46], &v[47], &v[48], IS8, &sflag);
   n = 1;
   ebvs01_(&n, &v[37], &v[39], &v[41], &v[42], &v[43]);
   v[40] = v[41]+v[37];
   n = 1;
   ebct00_(&n, &v[40], &v[4], &v[54], RP12, IP12, RS12);
   v[3] = v[40] /* Duplicate variable. */;
   n = 1;
   emdpmdc01_(&n, &v[0], &v[3], &v[5], &v[7], &v[9], &v[12], &v[13]
      , &dot[0], &v[14], &v[15], &v[16], &v[17], &v[18], NULL
      , NULL, &v[19], NULL, NULL, NULL, NULL, &v[20], NULL, NULL
      , RP0, IS0);
   v[2] = v[15];
   v[11] = v[2] /* Duplicate variable. */;
   n = 1;
   mecrl0a_(&n, &v[2], &v[24], &dot[1], &v[25], &dot[2], &v[23]
      , &v[26], &v[27], &v[28], &v[29], &v[30], &v[31], &v[32]
      , NULL, NULL, &v[33], NULL, NULL, NULL, NULL, &v[34], NULL
      , NULL, NULL, NULL, &v[35], NULL, NULL, NULL, NULL, &v[36]
      , NULL, NULL, RP4, IP4, RS4, IS4);
}


static void localJFuncEval_8(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[148] = y[col];
   v[139] = -v[148];
   v[141] = v[148];
   v[174] = v[139] /* Duplicate variable. */;
   v[187] = -RP44[1]*(RS44[0]*v[139]-RP44[0]);
   v[173] = v[174] /* Duplicate variable. */;
   v[179] = v[141]+v[173];
   v[143] = RP32[1]*(1+RP32[2]*(v[142]-RP32[0]))*v[148]*v[148];
   v[190] = v[191]-v[187];
   n = 3;
   {  /* DBK specific start. */


      pid001_(&n, &v[182], &v[190], NULL, NULL, &v[192], &dot[11]
      , &v[193], NULL, &v[194], &v[195], &v[196], &v[197], &v[198]
      , &v[199], &v[200], &v[201], &v[202], &v[203], RP46, IP46
      , IS46, &sflag);
   }
   n = 3;
   sigvsat0_(&n, &v[176], &v[181], &v[182], &v[183], IS40, &sflag
      );
   n = 2;
   ebvs01_(&n, &v[172], &v[174], &v[176], &v[177], &v[178]);
   v[175] = v[176]+v[172];
   n = 2;
   ebct00_(&n, &v[175], &v[139], &v[189], RP44, IP44, RS44);
   v[138] = v[175] /* Duplicate variable. */;
   n = 2;
   emdpmdc01_(&n, &v[135], &v[138], &v[140], &v[142], &v[144]
      , &v[147], &v[148], &dot[8], &v[149], &v[150], &v[151], &v[152]
      , &v[153], NULL, NULL, &v[154], NULL, NULL, NULL, NULL, &v[155]
      , NULL, NULL, RP32, IS32);
   v[137] = v[150];
   v[146] = v[137] /* Duplicate variable. */;
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_9(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[159] = y[col];
   v[135] = -v[159] /* Duplicate variable. */;
   v[211] = v[159];
   v[216] = (RP52[0]*((v[211]*1.0471975511966e-01)))/1.0471975511966e-01;
   n = 2;
   v[149] = emdpmdc01_macro0_(&n, &v[135], &v[144], RP32, IS32
         );
   v[214] = (v[216]*2*3.141592653589793/60)*RP51[0]*1e-3;
   n = 2;
   sd0000a_(&n, &v[213], &v[214], &v[215], &v[221], &v[222], &v[225]
      , &v[226], &v[227], &v[228], NULL, NULL, &v[229], NULL, NULL
      , NULL, NULL, &v[230], NULL, NULL, RP53, IP53, RS53, IS53
      );
   v[224] = v[213] /* Duplicate variable. */;
   n = 2;
   mecmas21_(&n, &v[232], &dot[12], &v[233], &dot[13], &v[234]
      , &v[231], &v[224], &v[235], &v[236], &v[237], &v[238], &v[239]
      , NULL, NULL, &v[240], NULL, NULL, NULL, NULL, &v[241], NULL
      , NULL, NULL, NULL, &v[242], NULL, NULL, NULL, NULL, &v[243]
      , NULL, NULL, &v[244], NULL, NULL, NULL, NULL, RP54, IP54
      , RS54, IS54);
   v[223] = -v[234] /* Duplicate variable. */;
   v[218] = RP51[0]*1e-3*v[213];
   n = 2;
   mecrn10a_(&n, &v[210], &v[211], &v[212], &v[218], &v[219], NULL
      , NULL, &v[220], NULL, NULL, RP52, IP52, TP52, IS52);
   v[158] = v[210];
   n = 2;
   emdpmdc01_(&n, &v[135], &v[138], &v[140], &v[142], &v[144]
      , &v[147], &v[148], &dot[8], &v[149], &v[150], &v[151], &v[152]
      , &v[153], NULL, NULL, &v[154], NULL, NULL, NULL, NULL, &v[155]
      , NULL, NULL, RP32, IS32);
   v[137] = v[150];
   v[146] = v[137] /* Duplicate variable. */;
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_10(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[160] = y[col];
   v[136] = -v[160] /* Duplicate variable. */;
   v[209] = RP50[1]*(RS50[0]*v[160]-RP50[0]);
   v[212] = v[160];
   v[217] = (RP52[0]*((v[212]*1.74532925199433e-02)))/1.74532925199433e-02;
   n = 3;
   ga00_(&n, &v[265], &v[209], &v[268], RP60, &sflag);
   v[215] = ((v[217])-RP51[1])*3.141592653589793/180*RP51[0]*1e-3;
   n = 2;
   sd0000a_(&n, &v[213], &v[214], &v[215], &v[221], &v[222], &v[225]
      , &v[226], &v[227], &v[228], NULL, NULL, &v[229], NULL, NULL
      , NULL, NULL, &v[230], NULL, NULL, RP53, IP53, RS53, IS53
      );
   v[224] = v[213] /* Duplicate variable. */;
   n = 2;
   mecmas21_(&n, &v[232], &dot[12], &v[233], &dot[13], &v[234]
      , &v[231], &v[224], &v[235], &v[236], &v[237], &v[238], &v[239]
      , NULL, NULL, &v[240], NULL, NULL, NULL, NULL, &v[241], NULL
      , NULL, NULL, NULL, &v[242], NULL, NULL, NULL, NULL, &v[243]
      , NULL, NULL, &v[244], NULL, NULL, NULL, NULL, RP54, IP54
      , RS54, IS54);
   v[223] = -v[234] /* Duplicate variable. */;
   v[266] = v[267]-v[265];
   n = 4;
   ga00_(&n, &v[250], &v[266], &v[269], RP61, &sflag);
   v[218] = RP51[0]*1e-3*v[213];
   n = 2;
   mecrn10a_(&n, &v[210], &v[211], &v[212], &v[218], &v[219], NULL
      , NULL, &v[220], NULL, NULL, RP52, IP52, TP52, IS52);
   n = 4;
   {  /* DBK specific start. */


      pid001_(&n, &v[249], &v[250], &v[251], &dot[14], &v[252]
      , &dot[15], &v[253], &v[254], &v[255], &v[256], &v[257]
      , &v[258], &v[259], &v[260], &v[261], &v[262], &v[263], &v[264]
      , RP58, IP58, IS58, &sflag);
   }
   n = 4;
   sigvsat0_(&n, &v[191], &v[208], &v[249], &v[206], IS62, &sflag
      );
   v[190] = v[191]-v[187];
   n = 3;
   {  /* DBK specific start. */


      pid001_(&n, &v[182], &v[190], NULL, NULL, &v[192], &dot[11]
      , &v[193], NULL, &v[194], &v[195], &v[196], &v[197], &v[198]
      , &v[199], &v[200], &v[201], &v[202], &v[203], RP46, IP46
      , IS46, &sflag);
   }
   v[158] = v[210];
   n = 3;
   sigvsat0_(&n, &v[176], &v[181], &v[182], &v[183], IS40, &sflag
      );
   n = 2;
   ebvs01_(&n, &v[172], &v[174], &v[176], &v[177], &v[178]);
   v[175] = v[176]+v[172];
   n = 2;
   ebct00_(&n, &v[175], &v[139], &v[189], RP44, IP44, RS44);
   v[138] = v[175] /* Duplicate variable. */;
   n = 2;
   emdpmdc01_(&n, &v[135], &v[138], &v[140], &v[142], &v[144]
      , &v[147], &v[148], &dot[8], &v[149], &v[150], &v[151], &v[152]
      , &v[153], NULL, NULL, &v[154], NULL, NULL, NULL, NULL, &v[155]
      , NULL, NULL, RP32, IS32);
   v[137] = v[150];
   v[146] = v[137] /* Duplicate variable. */;
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_11(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[192] = y[col];
   n = 3;
   {  /* DBK specific start. */


      pid001_(&n, &v[182], &v[190], NULL, NULL, &v[192], &dot[11]
      , &v[193], NULL, &v[194], &v[195], &v[196], &v[197], &v[198]
      , &v[199], &v[200], &v[201], &v[202], &v[203], RP46, IP46
      , IS46, &sflag);
   }
   n = 3;
   sigvsat0_(&n, &v[176], &v[181], &v[182], &v[183], IS40, &sflag
      );
   n = 2;
   ebvs01_(&n, &v[172], &v[174], &v[176], &v[177], &v[178]);
   v[175] = v[176]+v[172];
   n = 2;
   ebct00_(&n, &v[175], &v[139], &v[189], RP44, IP44, RS44);
   v[138] = v[175] /* Duplicate variable. */;
   n = 2;
   emdpmdc01_(&n, &v[135], &v[138], &v[140], &v[142], &v[144]
      , &v[147], &v[148], &dot[8], &v[149], &v[150], &v[151], &v[152]
      , &v[153], NULL, NULL, &v[154], NULL, NULL, NULL, NULL, &v[155]
      , NULL, NULL, RP32, IS32);
   v[137] = v[150];
   v[146] = v[137] /* Duplicate variable. */;
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_12(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[232] = y[col];
   v[221] = -v[232] /* Duplicate variable. */;
   v[246] = v[232];
   n = 2;
   sd0000a_(&n, &v[213], &v[214], &v[215], &v[221], &v[222], &v[225]
      , &v[226], &v[227], &v[228], NULL, NULL, &v[229], NULL, NULL
      , NULL, NULL, &v[230], NULL, NULL, RP53, IP53, RS53, IS53
      );
   v[224] = v[213] /* Duplicate variable. */;
   n = 2;
   mecmas21_(&n, &v[232], &dot[12], &v[233], &dot[13], &v[234]
      , &v[231], &v[224], &v[235], &v[236], &v[237], &v[238], &v[239]
      , NULL, NULL, &v[240], NULL, NULL, NULL, NULL, &v[241], NULL
      , NULL, NULL, NULL, &v[242], NULL, NULL, NULL, NULL, &v[243]
      , NULL, NULL, &v[244], NULL, NULL, NULL, NULL, RP54, IP54
      , RS54, IS54);
   v[223] = -v[234] /* Duplicate variable. */;
   v[218] = RP51[0]*1e-3*v[213];
   n = 2;
   mecrn10a_(&n, &v[210], &v[211], &v[212], &v[218], &v[219], NULL
      , NULL, &v[220], NULL, NULL, RP52, IP52, TP52, IS52);
   v[158] = v[210];
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_13(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[233] = y[col];
   v[222] = -v[233] /* Duplicate variable. */;
   v[247] = v[233];
   v[248] = RP55[1]*(RS55[0]*v[233]-RP55[0]);
   n = 2;
   sd0000a_(&n, &v[213], &v[214], &v[215], &v[221], &v[222], &v[225]
      , &v[226], &v[227], &v[228], NULL, NULL, &v[229], NULL, NULL
      , NULL, NULL, &v[230], NULL, NULL, RP53, IP53, RS53, IS53
      );
   v[224] = v[213] /* Duplicate variable. */;
   n = 2;
   mecmas21_(&n, &v[232], &dot[12], &v[233], &dot[13], &v[234]
      , &v[231], &v[224], &v[235], &v[236], &v[237], &v[238], &v[239]
      , NULL, NULL, &v[240], NULL, NULL, NULL, NULL, &v[241], NULL
      , NULL, NULL, NULL, &v[242], NULL, NULL, NULL, NULL, &v[243]
      , NULL, NULL, &v[244], NULL, NULL, NULL, NULL, RP54, IP54
      , RS54, IS54);
   v[223] = -v[234] /* Duplicate variable. */;
   v[218] = RP51[0]*1e-3*v[213];
   n = 2;
   mecrn10a_(&n, &v[210], &v[211], &v[212], &v[218], &v[219], NULL
      , NULL, &v[220], NULL, NULL, RP52, IP52, TP52, IS52);
   v[158] = v[210];
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_14(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[251] = y[col];
   n = 4;
   {  /* DBK specific start. */


      pid001_(&n, &v[249], &v[250], &v[251], &dot[14], &v[252]
      , &dot[15], &v[253], &v[254], &v[255], &v[256], &v[257]
      , &v[258], &v[259], &v[260], &v[261], &v[262], &v[263], &v[264]
      , RP58, IP58, IS58, &sflag);
   }
   n = 4;
   sigvsat0_(&n, &v[191], &v[208], &v[249], &v[206], IS62, &sflag
      );
   v[190] = v[191]-v[187];
   n = 3;
   {  /* DBK specific start. */


      pid001_(&n, &v[182], &v[190], NULL, NULL, &v[192], &dot[11]
      , &v[193], NULL, &v[194], &v[195], &v[196], &v[197], &v[198]
      , &v[199], &v[200], &v[201], &v[202], &v[203], RP46, IP46
      , IS46, &sflag);
   }
   n = 3;
   sigvsat0_(&n, &v[176], &v[181], &v[182], &v[183], IS40, &sflag
      );
   n = 2;
   ebvs01_(&n, &v[172], &v[174], &v[176], &v[177], &v[178]);
   v[175] = v[176]+v[172];
   n = 2;
   ebct00_(&n, &v[175], &v[139], &v[189], RP44, IP44, RS44);
   v[138] = v[175] /* Duplicate variable. */;
   n = 2;
   emdpmdc01_(&n, &v[135], &v[138], &v[140], &v[142], &v[144]
      , &v[147], &v[148], &dot[8], &v[149], &v[150], &v[151], &v[152]
      , &v[153], NULL, NULL, &v[154], NULL, NULL, NULL, NULL, &v[155]
      , NULL, NULL, RP32, IS32);
   v[137] = v[150];
   v[146] = v[137] /* Duplicate variable. */;
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_15(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[252] = y[col];
   n = 4;
   {  /* DBK specific start. */


      pid001_(&n, &v[249], &v[250], &v[251], &dot[14], &v[252]
      , &dot[15], &v[253], &v[254], &v[255], &v[256], &v[257]
      , &v[258], &v[259], &v[260], &v[261], &v[262], &v[263], &v[264]
      , RP58, IP58, IS58, &sflag);
   }
   n = 4;
   sigvsat0_(&n, &v[191], &v[208], &v[249], &v[206], IS62, &sflag
      );
   v[190] = v[191]-v[187];
   n = 3;
   {  /* DBK specific start. */


      pid001_(&n, &v[182], &v[190], NULL, NULL, &v[192], &dot[11]
      , &v[193], NULL, &v[194], &v[195], &v[196], &v[197], &v[198]
      , &v[199], &v[200], &v[201], &v[202], &v[203], RP46, IP46
      , IS46, &sflag);
   }
   n = 3;
   sigvsat0_(&n, &v[176], &v[181], &v[182], &v[183], IS40, &sflag
      );
   n = 2;
   ebvs01_(&n, &v[172], &v[174], &v[176], &v[177], &v[178]);
   v[175] = v[176]+v[172];
   n = 2;
   ebct00_(&n, &v[175], &v[139], &v[189], RP44, IP44, RS44);
   v[138] = v[175] /* Duplicate variable. */;
   n = 2;
   emdpmdc01_(&n, &v[135], &v[138], &v[140], &v[142], &v[144]
      , &v[147], &v[148], &dot[8], &v[149], &v[150], &v[151], &v[152]
      , &v[153], NULL, NULL, &v[154], NULL, NULL, NULL, NULL, &v[155]
      , NULL, NULL, RP32, IS32);
   v[137] = v[150];
   v[146] = v[137] /* Duplicate variable. */;
   n = 2;
   mecrl0a_(&n, &v[137], &v[159], &dot[9], &v[160], &dot[10], &v[158]
      , &v[161], &v[162], &v[163], &v[164], &v[165], &v[166], &v[167]
      , NULL, NULL, &v[168], NULL, NULL, NULL, NULL, &v[169], NULL
      , NULL, NULL, NULL, &v[170], NULL, NULL, NULL, NULL, &v[171]
      , NULL, NULL, RP36, IP36, RS36, IS36);
}


static void localJFuncEval_16(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[283] = y[col];
   v[274] = -v[283];
   v[276] = v[283];
   v[309] = v[274] /* Duplicate variable. */;
   v[322] = -RP76[1]*(RS76[0]*v[274]-RP76[0]);
   v[308] = v[309] /* Duplicate variable. */;
   v[314] = v[276]+v[308];
   v[278] = RP64[1]*(1+RP64[2]*(v[277]-RP64[0]))*v[283]*v[283];
   v[325] = v[326]-v[322];
   n = 5;
   {  /* DBK specific start. */


      pid001_(&n, &v[317], &v[325], NULL, NULL, &v[327], &dot[19]
      , &v[328], NULL, &v[329], &v[330], &v[331], &v[332], &v[333]
      , &v[334], &v[335], &v[336], &v[337], &v[338], RP78, IP78
      , IS78, &sflag);
   }
   n = 5;
   sigvsat0_(&n, &v[311], &v[316], &v[317], &v[318], IS72, &sflag
      );
   n = 3;
   ebvs01_(&n, &v[307], &v[309], &v[311], &v[312], &v[313]);
   v[310] = v[311]+v[307];
   n = 3;
   ebct00_(&n, &v[310], &v[274], &v[324], RP76, IP76, RS76);
   v[273] = v[310] /* Duplicate variable. */;
   n = 3;
   emdpmdc01_(&n, &v[270], &v[273], &v[275], &v[277], &v[279]
      , &v[282], &v[283], &dot[16], &v[284], &v[285], &v[286]
      , &v[287], &v[288], NULL, NULL, &v[289], NULL, NULL, NULL
      , NULL, &v[290], NULL, NULL, RP64, IS64);
   v[272] = v[285];
   v[281] = v[272] /* Duplicate variable. */;
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void localJFuncEval_17(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[294] = y[col];
   v[270] = -v[294] /* Duplicate variable. */;
   v[346] = v[294];
   v[351] = (RP95[0]*((v[346]*1.0471975511966e-01)))/1.0471975511966e-01;
   n = 3;
   v[284] = emdpmdc01_macro0_(&n, &v[270], &v[279], RP64, IS64
         );
   v[349] = (v[351]*2*3.141592653589793/60)*RP83[0]*1e-3;
   n = 3;
   sd0000a_(&n, &v[348], &v[349], &v[350], &v[354], &v[355], &v[358]
      , &v[359], &v[360], &v[361], NULL, NULL, &v[362], NULL, NULL
      , NULL, NULL, &v[363], NULL, NULL, RP84, IP84, RS84, IS84
      );
   v[357] = v[348] /* Duplicate variable. */;
   n = 3;
   mecmas21_(&n, &v[365], &dot[20], &v[366], &dot[21], &v[367]
      , &v[364], &v[357], &v[368], &v[369], &v[370], &v[371], &v[372]
      , NULL, NULL, &v[373], NULL, NULL, NULL, NULL, &v[374], NULL
      , NULL, NULL, NULL, &v[375], NULL, NULL, NULL, NULL, &v[376]
      , NULL, NULL, &v[377], NULL, NULL, NULL, NULL, RP85, IP85
      , RS85, IS85);
   v[356] = -v[367] /* Duplicate variable. */;
   v[353] = RP83[0]*1e-3*v[348];
   n = 3;
   mecrn10a_(&n, &v[345], &v[346], &v[347], &v[353], &v[403], NULL
      , NULL, &v[404], NULL, NULL, RP95, IP95, TP95, IS95);
   v[293] = v[345];
   n = 3;
   emdpmdc01_(&n, &v[270], &v[273], &v[275], &v[277], &v[279]
      , &v[282], &v[283], &dot[16], &v[284], &v[285], &v[286]
      , &v[287], &v[288], NULL, NULL, &v[289], NULL, NULL, NULL
      , NULL, &v[290], NULL, NULL, RP64, IS64);
   v[272] = v[285];
   v[281] = v[272] /* Duplicate variable. */;
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void localJFuncEval_18(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[295] = y[col];
   v[271] = -v[295] /* Duplicate variable. */;
   v[344] = RP82[1]*(RS82[0]*v[295]-RP82[0]);
   v[347] = v[295];
   n = 5;
   ga00_(&n, &v[398], &v[344], &v[401], RP91, &sflag);
   v[352] = (RP95[0]*((v[347]*1.74532925199433e-02)))/1.74532925199433e-02;
   v[350] = ((v[352])-RP83[1])*3.141592653589793/180*RP83[0]*1e-3;
   n = 3;
   sd0000a_(&n, &v[348], &v[349], &v[350], &v[354], &v[355], &v[358]
      , &v[359], &v[360], &v[361], NULL, NULL, &v[362], NULL, NULL
      , NULL, NULL, &v[363], NULL, NULL, RP84, IP84, RS84, IS84
      );
   v[357] = v[348] /* Duplicate variable. */;
   n = 3;
   mecmas21_(&n, &v[365], &dot[20], &v[366], &dot[21], &v[367]
      , &v[364], &v[357], &v[368], &v[369], &v[370], &v[371], &v[372]
      , NULL, NULL, &v[373], NULL, NULL, NULL, NULL, &v[374], NULL
      , NULL, NULL, NULL, &v[375], NULL, NULL, NULL, NULL, &v[376]
      , NULL, NULL, &v[377], NULL, NULL, NULL, NULL, RP85, IP85
      , RS85, IS85);
   v[356] = -v[367] /* Duplicate variable. */;
   v[399] = v[400]-v[398];
   n = 6;
   ga00_(&n, &v[383], &v[399], &v[402], RP92, &sflag);
   v[353] = RP83[0]*1e-3*v[348];
   n = 6;
   {  /* DBK specific start. */


      pid001_(&n, &v[382], &v[383], &v[384], &dot[22], &v[385]
      , &dot[23], &v[386], &v[387], &v[388], &v[389], &v[390]
      , &v[391], &v[392], &v[393], &v[394], &v[395], &v[396], &v[397]
      , RP89, IP89, IS89, &sflag);
   }
   n = 6;
   sigvsat0_(&n, &v[326], &v[343], &v[382], &v[341], IS93, &sflag
      );
   n = 3;
   mecrn10a_(&n, &v[345], &v[346], &v[347], &v[353], &v[403], NULL
      , NULL, &v[404], NULL, NULL, RP95, IP95, TP95, IS95);
   v[325] = v[326]-v[322];
   n = 5;
   {  /* DBK specific start. */


      pid001_(&n, &v[317], &v[325], NULL, NULL, &v[327], &dot[19]
      , &v[328], NULL, &v[329], &v[330], &v[331], &v[332], &v[333]
      , &v[334], &v[335], &v[336], &v[337], &v[338], RP78, IP78
      , IS78, &sflag);
   }
   v[293] = v[345];
   n = 5;
   sigvsat0_(&n, &v[311], &v[316], &v[317], &v[318], IS72, &sflag
      );
   n = 3;
   ebvs01_(&n, &v[307], &v[309], &v[311], &v[312], &v[313]);
   v[310] = v[311]+v[307];
   n = 3;
   ebct00_(&n, &v[310], &v[274], &v[324], RP76, IP76, RS76);
   v[273] = v[310] /* Duplicate variable. */;
   n = 3;
   emdpmdc01_(&n, &v[270], &v[273], &v[275], &v[277], &v[279]
      , &v[282], &v[283], &dot[16], &v[284], &v[285], &v[286]
      , &v[287], &v[288], NULL, NULL, &v[289], NULL, NULL, NULL
      , NULL, &v[290], NULL, NULL, RP64, IS64);
   v[272] = v[285];
   v[281] = v[272] /* Duplicate variable. */;
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void localJFuncEval_19(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[327] = y[col];
   n = 5;
   {  /* DBK specific start. */


      pid001_(&n, &v[317], &v[325], NULL, NULL, &v[327], &dot[19]
      , &v[328], NULL, &v[329], &v[330], &v[331], &v[332], &v[333]
      , &v[334], &v[335], &v[336], &v[337], &v[338], RP78, IP78
      , IS78, &sflag);
   }
   n = 5;
   sigvsat0_(&n, &v[311], &v[316], &v[317], &v[318], IS72, &sflag
      );
   n = 3;
   ebvs01_(&n, &v[307], &v[309], &v[311], &v[312], &v[313]);
   v[310] = v[311]+v[307];
   n = 3;
   ebct00_(&n, &v[310], &v[274], &v[324], RP76, IP76, RS76);
   v[273] = v[310] /* Duplicate variable. */;
   n = 3;
   emdpmdc01_(&n, &v[270], &v[273], &v[275], &v[277], &v[279]
      , &v[282], &v[283], &dot[16], &v[284], &v[285], &v[286]
      , &v[287], &v[288], NULL, NULL, &v[289], NULL, NULL, NULL
      , NULL, &v[290], NULL, NULL, RP64, IS64);
   v[272] = v[285];
   v[281] = v[272] /* Duplicate variable. */;
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void localJFuncEval_20(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[365] = y[col];
   v[354] = -v[365] /* Duplicate variable. */;
   v[379] = v[365];
   n = 3;
   sd0000a_(&n, &v[348], &v[349], &v[350], &v[354], &v[355], &v[358]
      , &v[359], &v[360], &v[361], NULL, NULL, &v[362], NULL, NULL
      , NULL, NULL, &v[363], NULL, NULL, RP84, IP84, RS84, IS84
      );
   v[357] = v[348] /* Duplicate variable. */;
   n = 3;
   mecmas21_(&n, &v[365], &dot[20], &v[366], &dot[21], &v[367]
      , &v[364], &v[357], &v[368], &v[369], &v[370], &v[371], &v[372]
      , NULL, NULL, &v[373], NULL, NULL, NULL, NULL, &v[374], NULL
      , NULL, NULL, NULL, &v[375], NULL, NULL, NULL, NULL, &v[376]
      , NULL, NULL, &v[377], NULL, NULL, NULL, NULL, RP85, IP85
      , RS85, IS85);
   v[356] = -v[367] /* Duplicate variable. */;
   v[353] = RP83[0]*1e-3*v[348];
   n = 3;
   mecrn10a_(&n, &v[345], &v[346], &v[347], &v[353], &v[403], NULL
      , NULL, &v[404], NULL, NULL, RP95, IP95, TP95, IS95);
   v[293] = v[345];
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void localJFuncEval_21(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[366] = y[col];
   v[355] = -v[366] /* Duplicate variable. */;
   v[380] = v[366];
   v[381] = RP86[1]*(RS86[0]*v[366]-RP86[0]);
   n = 3;
   sd0000a_(&n, &v[348], &v[349], &v[350], &v[354], &v[355], &v[358]
      , &v[359], &v[360], &v[361], NULL, NULL, &v[362], NULL, NULL
      , NULL, NULL, &v[363], NULL, NULL, RP84, IP84, RS84, IS84
      );
   v[357] = v[348] /* Duplicate variable. */;
   n = 3;
   mecmas21_(&n, &v[365], &dot[20], &v[366], &dot[21], &v[367]
      , &v[364], &v[357], &v[368], &v[369], &v[370], &v[371], &v[372]
      , NULL, NULL, &v[373], NULL, NULL, NULL, NULL, &v[374], NULL
      , NULL, NULL, NULL, &v[375], NULL, NULL, NULL, NULL, &v[376]
      , NULL, NULL, &v[377], NULL, NULL, NULL, NULL, RP85, IP85
      , RS85, IS85);
   v[356] = -v[367] /* Duplicate variable. */;
   v[353] = RP83[0]*1e-3*v[348];
   n = 3;
   mecrn10a_(&n, &v[345], &v[346], &v[347], &v[353], &v[403], NULL
      , NULL, &v[404], NULL, NULL, RP95, IP95, TP95, IS95);
   v[293] = v[345];
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void localJFuncEval_22(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[384] = y[col];
   n = 6;
   {  /* DBK specific start. */


      pid001_(&n, &v[382], &v[383], &v[384], &dot[22], &v[385]
      , &dot[23], &v[386], &v[387], &v[388], &v[389], &v[390]
      , &v[391], &v[392], &v[393], &v[394], &v[395], &v[396], &v[397]
      , RP89, IP89, IS89, &sflag);
   }
   n = 6;
   sigvsat0_(&n, &v[326], &v[343], &v[382], &v[341], IS93, &sflag
      );
   v[325] = v[326]-v[322];
   n = 5;
   {  /* DBK specific start. */


      pid001_(&n, &v[317], &v[325], NULL, NULL, &v[327], &dot[19]
      , &v[328], NULL, &v[329], &v[330], &v[331], &v[332], &v[333]
      , &v[334], &v[335], &v[336], &v[337], &v[338], RP78, IP78
      , IS78, &sflag);
   }
   n = 5;
   sigvsat0_(&n, &v[311], &v[316], &v[317], &v[318], IS72, &sflag
      );
   n = 3;
   ebvs01_(&n, &v[307], &v[309], &v[311], &v[312], &v[313]);
   v[310] = v[311]+v[307];
   n = 3;
   ebct00_(&n, &v[310], &v[274], &v[324], RP76, IP76, RS76);
   v[273] = v[310] /* Duplicate variable. */;
   n = 3;
   emdpmdc01_(&n, &v[270], &v[273], &v[275], &v[277], &v[279]
      , &v[282], &v[283], &dot[16], &v[284], &v[285], &v[286]
      , &v[287], &v[288], NULL, NULL, &v[289], NULL, NULL, NULL
      , NULL, &v[290], NULL, NULL, RP64, IS64);
   v[272] = v[285];
   v[281] = v[272] /* Duplicate variable. */;
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void localJFuncEval_23(AMESIMSYSTEM * amesys, double t, double * y, double * dot, double * v, double * yprime, double * delta, int col)
{
   int sflag = 1;
   int n;
   double *dbk_wk = amesys->pModel->dbk_wk;

   v[385] = y[col];
   n = 6;
   {  /* DBK specific start. */


      pid001_(&n, &v[382], &v[383], &v[384], &dot[22], &v[385]
      , &dot[23], &v[386], &v[387], &v[388], &v[389], &v[390]
      , &v[391], &v[392], &v[393], &v[394], &v[395], &v[396], &v[397]
      , RP89, IP89, IS89, &sflag);
   }
   n = 6;
   sigvsat0_(&n, &v[326], &v[343], &v[382], &v[341], IS93, &sflag
      );
   v[325] = v[326]-v[322];
   n = 5;
   {  /* DBK specific start. */


      pid001_(&n, &v[317], &v[325], NULL, NULL, &v[327], &dot[19]
      , &v[328], NULL, &v[329], &v[330], &v[331], &v[332], &v[333]
      , &v[334], &v[335], &v[336], &v[337], &v[338], RP78, IP78
      , IS78, &sflag);
   }
   n = 5;
   sigvsat0_(&n, &v[311], &v[316], &v[317], &v[318], IS72, &sflag
      );
   n = 3;
   ebvs01_(&n, &v[307], &v[309], &v[311], &v[312], &v[313]);
   v[310] = v[311]+v[307];
   n = 3;
   ebct00_(&n, &v[310], &v[274], &v[324], RP76, IP76, RS76);
   v[273] = v[310] /* Duplicate variable. */;
   n = 3;
   emdpmdc01_(&n, &v[270], &v[273], &v[275], &v[277], &v[279]
      , &v[282], &v[283], &dot[16], &v[284], &v[285], &v[286]
      , &v[287], &v[288], NULL, NULL, &v[289], NULL, NULL, NULL
      , NULL, &v[290], NULL, NULL, RP64, IS64);
   v[272] = v[285];
   v[281] = v[272] /* Duplicate variable. */;
   n = 3;
   mecrl0a_(&n, &v[272], &v[294], &dot[17], &v[295], &dot[18]
      , &v[293], &v[296], &v[297], &v[298], &v[299], &v[300], &v[301]
      , &v[302], NULL, NULL, &v[303], NULL, NULL, NULL, NULL, &v[304]
      , NULL, NULL, NULL, NULL, &v[305], NULL, NULL, NULL, NULL
      , &v[306], NULL, NULL, RP68, IP68, RS68, IS68);
}


static void (*jfunc_table[])(AMESIMSYSTEM *, double, double *, double *, double *, double *, double *, int) = {
      localJFuncEval_0, localJFuncEval_1, localJFuncEval_2, localJFuncEval_3, localJFuncEval_4, localJFuncEval_5
      , localJFuncEval_6, localJFuncEval_7, localJFuncEval_8, localJFuncEval_9, localJFuncEval_10, localJFuncEval_11
      , localJFuncEval_12, localJFuncEval_13, localJFuncEval_14, localJFuncEval_15, localJFuncEval_16
      , localJFuncEval_17, localJFuncEval_18, localJFuncEval_19, localJFuncEval_20, localJFuncEval_21
      , localJFuncEval_22, localJFuncEval_23
};



static void localJFuncEval(AMESIMSYSTEM *amesys, double t, double *y, double *yprime, double *delta, int col)
{
   int sflag=1; /* Only one flag value is required. */
   int n=1, i=0;
   double *v = amesys->v;
   double *vcopy = amesys->vcopy;
   double *input = amesys->inputs;
   double *output = amesys->outputs;
   double *Z = amesys->discrete_states;
   double *dbk_wk = amesys->pModel->dbk_wk;
   
#if(AME_MODEL_ISEXPLICIT == 1)
   double *dot = yprime;
   
#elif( AME_NBOF_EXPLICIT_STATE > 0 )  
   double dot[AME_NBOF_EXPLICIT_STATE];
   
   /* Initialize the dot vector to the yprime vector. */
   memcpy((void *)dot, (void *)yprime, AME_NBOF_EXPLICIT_STATE*sizeof(double));  
#else
   double *dot = NULL;
#endif    
   
#if(AME_MODEL_ISEXPLICIT == 0)
   /* Initialize everything ready for potential calls to stepdn
      in submodels. */

#if(defined(AME_COSIM) && (AME_NBOF_INPUTS > 0))
   if(amesys->doInterpol) {
      if( getInputsCosimSlave(amesys->csSlave, t, input) != AME_NO_ERROR ) {
         AmeExit(1);
      }
   }
#endif
      
   if(isstabrun_())
   {
      t = amesys->simOptions->fixedTime;
   }
#endif
   
   /* Record current simulation time for ametim_(). */

   SetTimeAtThisStep(t);

   if (holdinputs_())
   {
      /* We reset artificially the time to the initial value
         to give the illusion of held inputs. */

      t = getstarttime_();
   }
   memcpy((void *)vcopy, (void *)v, (size_t)(AME_NBOF_VARS*sizeof(double)));

   /* Assign the state variables y[] calculated by the integrator 
      to the appropriate variables v[] and right calls necessary
      for that state in a case of a switch. */

   (*jfunc_table[col])(amesys, t, y, dot, v, yprime, delta, col);
   
   memcpy((void *)v, (void *)vcopy, (size_t)(AME_NBOF_VARS*sizeof(double)));

   UpFECount(amesys);
}

static void EndOfSimulation(AMESIMSYSTEM *amesys)
{
   int n=1;
   double *y = amesys->states;
   double *v = amesys->v;
   double *Z = amesys->discrete_states;
   double *dbk_wk = amesys->pModel->dbk_wk;


}

static void ameTerminate(AMESIMSYSTEM *amesys)
{
#ifdef AME_WRITE_RUNSTAT
   if(amesys->simOptions ) {
      if(amesys->simOptions->statistics)
      {
         WriteRunStats(amesys);
      }
   }
#endif
#ifdef AME_PROCESS_TIME
   ProcessTime(2);
#endif
      
   /* Save state count, discontinuities and finalize the Performance Analyzer */
#ifdef AME_PERFORMANCE_ANALYZER
   if(!isfixedstepsolver_()){
      PerfAnalyzer_SaveStateCount (amesys);
      PerfAnalyzer_SaveDiscontinuities(amesys);
      PerfAnalyzer_Close(amesys);   
   }
#endif

#ifdef AME_RESULT_FILE
   amesys->CloseResultFile(amesys);
#endif

   EndOfSimulation(amesys);
   AmeCallAtEnd(amesys->ameExitStatus);
   modelCleanStore(amesys->pModel);
}

#ifdef AME_EXPOSE_JACOBIAN
static int getPartialDerivatives(double *A, double *B, double *C, double *D)
{
   int res;
   AMESIMSYSTEM  *amesys = GetGlobalSystem();
   
   SPARSE_MATRIX *Amat, *Bmat, *Cmat, *Dmat;
   
#if(AME_MODEL_ISEXPLICIT == 1)
   if (amesys->tlast > TLAST_MIN)
      res = LDoLinearAnalysisOnDemand(amesys, amesys->numstates,
                                       amesys->tlast, amesys->states,
                                       &Amat, &Bmat, &Cmat, &Dmat);
#else
   if (amesys->tlast > TLAST_MIN)
      res = DDoLinearAnalysisOnDemand(amesys, amesys->neq,
                                       amesys->tlast,
                                       amesys->states, amesys->dotstates,
                                       &Amat, &Bmat, &Cmat, &Dmat);
#endif
   else
      res = 0;  /* failed: system probably not initialized */
   
   if(res != 0) {
      if(Amat) {
         GetMatAsDense(Amat,A);
      }
      if(Bmat) {
         GetMatAsDense(Bmat,B);
      }
      if(Cmat) {
         GetMatAsDense(Cmat,C);
      }
      if(Dmat) {
         GetMatAsDense(Dmat,D);
      }
      FreeSparseMatrix(Amat);
      FreeSparseMatrix(Bmat);
      FreeSparseMatrix(Cmat);
      FreeSparseMatrix(Dmat);
   }
   
   return res;
} /* getPartialDerivatives */

static int setLADetailsFromIO(int num_input_index, int num_output_index,
                              int *input_index, int *output_index, int *nbState)
{
   *nbState = AME_NBOF_EXPLICIT_STATE + AME_NBOF_IMPLICIT_STATE;

   return SetLADetailsFromIO(AME_NBOF_EXPLICIT_STATE, AME_NBOF_IMPLICIT_STATE,
                              AME_NBOF_INPUTS, AME_NBOF_OUTPUTS,
                              num_input_index, num_output_index, input_index, output_index,
                              GInputVarNum, GOutputVarNum, 0.0, 1.0, 1.0, 0.1);
}
#endif

/* ============================================================== */

static void ModelAmeExit(AMESIMSYSTEM *amesys, int status)
{
   /* Will be catch by the state machine */
   amesys->ameExitStatus = status;
   longjmp(amesys->jump_env, status);
}

#ifdef AME_INPUT_IN_MEMORY
#include "SIMOTICS_1_Test_.ssf.h"
static char **getssflist(int *num_items)
{
   *num_items = savestatusflags_length;
   return savestatusflags;
}
#endif

/**************************************************************
*                                                             *
* Function load_subdata_tables reads data for lookuptables    *
* mostly/only used for realtime                               *
*                                                             *
* 0106429                                                     *
* Move the include outside of function. The include now       *
* contains one function per table and a function that calls   *
* them all. This reduces the risk that a compiler crashes due *
* to a huge function.                                         *
**************************************************************/
#ifdef AME_TABLES_IN_MEMORY
#include "SIMOTICS_1_Test_.subdata.h"
#endif

static void load_subdata_tables(void)
{
#ifdef AME_TABLES_IN_MEMORY
   add_all_tables_to_memory();
#endif
}

/***********************************************************
   Purpose  :  Return Simcenter Amesim version used to generate the model C code
               It allows the client to update interface management for
               backward compatibility.
   Author	:  J.Andre
   Creation :  2016 - 09 - 05
   Inputs   :  None 
   Outputs  :  Simcenter Amesim version
   Revision :
************************************************************/
static unsigned int AmesysGetVersion() 
{
	/* Returned number indicates 10000* main verion + 100* SL version + minor */
   /* Eg. Rev15     (15.0.0)  => 150000 */
   /* Eg. Rev15SL1  (15.1.0)  => 150100 */
   /* Eg. Rev15.0.1 (15.0.1)  => 150001 */

   return AMEVERSION;
}

/***********************************************************
   Purpose  :  Return SoldTo ID which Simcenter Amesim model has been generated.
   Author	:  J.Andre
   Creation :  2017 - 02 - 13
   Inputs   :  None 
   Outputs  :  SoldToID
   Revision :
************************************************************/
static const char* getSoldToID()
{
   return "5305435";
}

#ifdef AME_INPUT_IN_MEMORY
#include "SIMOTICS_1_Test_.globalparams.h"
#endif

/***********************************************************
   Purpose  : Load and evaluate model parameters from files
   Author   : J.Andre
   Created  : 2016 - 09 - 08
   Inputs   :
      amesys  : system
      errmsg  : error message
   Outputs  : Error code
   Revision :
************************************************************/
static AMESystemError loadModelParamFromDisk(AMESIMSYSTEM *amesys, AMESystemError *gpError,  AMESystemError *lpError, char *errmsg)
{
   AMESystemError ret;
   *gpError = AME_NO_ERROR;
   *lpError = AME_NO_ERROR;
   
   ret = AmeReadGPFile(amesys);
   
   if(ret == AME_NO_ERROR) {
      *gpError = AmeEvalGlobalParamList(amesys, 1, errmsg);
      *lpError = loadParameterFromFile(amesys->pModel, GetDataFileName(), errmsg);
   }
   return ret;
}

#ifdef AME_INPUT_IN_MEMORY

/***********************************************************
   Purpose    : Load and evaluate model parameters from memory
   Author	  : J.Andre
   Created on : 2016 - 09 - 08
   Inputs	  :
      amesys  : system
      errmsg  : error message
   Outputs	  : Parameter error code
   Revision   :
************************************************************/
static AMESystemError loadModelParamFromMemory(AMESIMSYSTEM *amesys, AMESystemError *gpError,  AMESystemError *lpError, char * errmsg)
{  
#include "SIMOTICS_1_Test_.data.h"
   
   ameAddGlobalParamsFromMemory(amesys,errmsg);
   
   *gpError = AmeEvalGlobalParamList(amesys, 1, errmsg);
   *lpError = loadParameterFromDataTable(amesys->pModel, allparams, errmsg);

   return AME_NO_ERROR;
}

#endif

/***********************************************************
   Purpose    : Load and evaluate model parameters
   Author	  : J.Andre
   Created on : 2016 - 09 - 08
   Inputs	  :
      amesys  : system
      errmsg  : error message
   Outputs	  : Parameter error code
   Revision   :
************************************************************/
static AMESystemError loadModelParameters(AMESIMSYSTEM *amesys)
{
   AMESystemError res = AME_PARAMETER_ERROR;
   char errmsg[PATH_MAX+128];
   AMESystemError ret_gp = AME_NO_ERROR;
   AMESystemError ret_lp = AME_NO_ERROR;
   
   errmsg[0] = '\0';
   
#ifdef AME_INPUT_IN_MEMORY
#ifdef AME_RT_CAN_READ_FILE
   res = loadModelParamFromDisk(amesys, &ret_gp, &ret_lp, errmsg);

   if (res != AME_NO_ERROR) {
      /* If the file is not there - we say nothing */
      if (res != AME_PARAM_FILE_OPEN_ERROR) {
         amefprintf(stderr,"%s",errmsg);
         amefprintf(stderr,"loadParameterFromFile> %s\n",errmsg);
         ClearGPs();
      }
   }
   else {
      amefprintf(stderr,"Using data from disk (%s)\n",GetDataFileName());
   }
#endif
   if(res != AME_NO_ERROR) {
      /* Read all from memory */
      res = loadModelParamFromMemory(amesys, &ret_gp, &ret_lp, errmsg);
      SetGlobalParamReadFile(0);
   }
#else
   /* Read all from disk */
   res = loadModelParamFromDisk(amesys, &ret_gp, &ret_lp, errmsg);
#endif

   if( (res != AME_NO_ERROR) || (ret_gp != AME_NO_ERROR) || (ret_lp != AME_NO_ERROR) ) {
      amefprintf(stderr,"%s",errmsg);
      
      if( (res != AME_NO_ERROR) || (ret_lp != AME_NO_ERROR) ) {
         res = AME_GLOBAL_PARAMETER_ERROR;
      }
   }
   return res;
}

static AMESystemError Input(AMESIMSYSTEM *amesys)
{
   /* Load data files for submodels */
   load_subdata_tables();
   
   /* Load parameters for submodels */
   return loadModelParameters(amesys);
}

static int ameSetOptions(AMESIMSYSTEM *amesys,
                         double tsaveinc, 
                         double maxstepsize,
                         double tolerance,
                         int errorcontrol,
                         int writelevel,
                         int extradisconprint,
                         int runstats,
                         int theruntype,
                         int thesolvertype)
{
   amesys->simOptions->errorType = errorcontrol;
   amesys->simOptions->tol = tolerance;
   amesys->simOptions->rStrtp = extradisconprint;
   amesys->simOptions->statistics = runstats;
   amesys->simOptions->solverType = thesolvertype;
   amesys->simOptions->runType = theruntype;
   amesys->simOptions->iWrite = writelevel;
   amesys->simOptions->tInc = tsaveinc;
   amesys->simOptions->hMax = maxstepsize;   
   
   /* Copy sim option to share with libraries before to modify it */
   memcpy(amesys->sharedSimOptions, amesys->simOptions, sizeof(SIMOptions));
   
   if(amesys->simOptions->solverType) {
      /* It is the cautious option. The maximum time step
         should not exceed the print interval. */
      setmaxstep_(&amesys->simOptions->tInc);
   }
   
#if( AME_MODEL_ISEXPLICIT == 1) && (AME_NBOF_EXPLICIT_STATE == 0 )
   if(maxstepsize > tsaveinc) {
      amefprintf(stderr, "Since the model has no state variable,\n");
      amefprintf(stderr, "the maximum time step has been reduced to %gs.\n", tsaveinc);
      setmaxstep_(&amesys->simOptions->tInc);
   }
#endif
   
   recordtinc_(amesys->simOptions->tInc);
   
   ValidateRuntype(theruntype);

   ameSetupTolerance(amesys->simOptions);

   return 1;
}
                   
static int ameSetOptionsFixed(AMESIMSYSTEM *amesys,
                              double printinterval,
                              int fixed_type,
                              int fixedOrder,
                              double fixed_h,
                              int run_type)
{
   amesys->simOptions->iWrite = 2;
   
   ValidateRuntype(run_type);
   
	/* Ensure that runflag StabilizingRun=0. It might have been set to true */
	/* due to a previous selection for the variable step integrator. */
   ClearStabilizingRun();
   
   amesys->simOptions->fixedOrder = fixedOrder;
   amesys->simOptions->fixedStep  = 1; /* Yes - fixed step */
   amesys->simOptions->fixedH     = fixed_h;

   amesys->simOptions->tInc =  printinterval;

   amesys->simOptions->fixedType  = fixed_type;
   SetIsUsingFixedSolver(( fixed_type == 1)*100 +  (fixed_type != 1)*200 + fixedOrder);

   SetFixedTimeStep(fixed_h);

   return 1;
}

static int ameInputs(AMESIMSYSTEM *amesys, int numInputs, const double *inputARG)
{
   if(numInputs != amesys->numinputs)
   {
      char error_message[256];
      sprintf(error_message, "AMEInputs> Expected %d inputs but got %d\n", amesys->numinputs, numInputs);      
      DisplayMessage(error_message);
      return 0;
   }
#if (AME_NBOF_INPUTS > 0)
   memcpy(amesys->inputs, inputARG, amesys->numinputs*sizeof(double) );
#endif
   return 1;
}

static void doAssembly(AMESIMSYSTEM *amesys)
{
   if (IsAssemblyNecessary_())
   {
      double time = getstarttime_();
      double tmp[AME_NBOF_SOLVER_STATES];
      int local_flag;

      /* Perform the assembly. */
      amesys->consflag = 1;
      local_flag = 0;
#if(AME_MODEL_ISEXPLICIT == 1)
      amesys->FuncEval(amesys, time, amesys->states, tmp, NULL, &local_flag);
#else
      amesys->res(amesys, time, amesys->states, amesys->dotstates, tmp, &local_flag);
#endif
      amesys->first_call = 1;
   
      amesys->consflag = 2;
      local_flag = 0;
#if(AME_MODEL_ISEXPLICIT == 1)
      amesys->FuncEval(amesys, time, amesys->states, tmp, NULL, &local_flag);
#else
      amesys->res(amesys, time, amesys->states, amesys->dotstates, tmp, &local_flag);
#endif
      amesys->first_call = 1;
      amesys->consflag = 0;
   }
}

static int ameEvalTstart(AMESIMSYSTEM *amesys, const double *modelInputs, double *modelOutputs)
{
#ifndef AMERT
   double time = getstarttime_();
   
   ameInputs(amesys, AME_NBOF_INPUTS, modelInputs);
   
   doAssembly(amesys);

   /* Initialize, maybe perform an initialising run */

   if(isstabrun_())
   {
      amesys->simOptions->fixedTime = time;
   }
   amesys->simOptions->stabilOption += 4*amesys->simOptions->solverType;

#if(AME_MODEL_ISEXPLICIT == 1)

   if(!IntegrateInit(amesys, time, time))
   {
      return 0;
   }
#else
   amesys->needrestart = 1;

   DIntegrateInit(amesys, AME_NBOF_EXPLICIT_STATE, time, getfinaltime_(), amesys->simOptions->tInc, amesys->states,
                  amesys->dotstates, amesys->simOptions->hMax, AME_NBOF_SOLVER_STATES, amesys->iwork, amesys->simOptions->reltol, amesys->simOptions->abstol, amesys->simOptions->rStrtp, LIW, LRW, amesys->simOptions->statistics,amesys->simOptions->stabilOption, amesys->simOptions->iWrite, amesys->simOptions->minimalDiscont, Gis_constraint);

   if(isstabrun_())
   {
      /* HELP !!!! */
   }
#endif
  
#if (AME_NBOF_OUTPUTS > 0)
   memcpy(modelOutputs, amesys->outputs, amesys->numoutputs*sizeof(double) );
#endif
   amesys->tlast = time;
   
   return 1;
#else
   char error_message[256];
   sprintf(error_message, "ameEvalTstart> Should never be called for real-time simulation\n");      
   DisplayMessage(error_message);
   return 0;
#endif
}

static int ameSetUpLockedStatus(AMESIMSYSTEM *amesys)
{
#ifdef AME_INPUT_IN_MEMORY
#include "SIMOTICS_1_Test_.lock.h"
   if(0 != SetUpLockedStatesFromMemory(amesys, lockedstates_length, lockedstates_array))
   {
      amefprintf(stderr,"Failed to set the locked states status.\n");
   }
#else
   SetUpLockedStates(GetCircuitName());
#endif
   return 0;
}

static int ameInitializeConditions(AMESIMSYSTEM *amesys)
{
#ifdef AME_VSOLVER_ACCEPTED
   double time = amesys->simOptions->tStart;

   /* Initialise some static variables */
   setfinaltime_(amesys->simOptions->tFinal);

   amesys->first_call=1;  /* should this be done or not ?*/
   amesys->needrestart = 1;

   amesys->tlast = TLAST_MIN;

   memset(amesys->ecount,0,amesys->numstates*sizeof(int));
   memset(amesys->dotstates,0,amesys->numstates*sizeof(double));

#if(AME_MODEL_ISEXPLICIT == 0)
   memset(amesys->iwork,0,LIW*sizeof(int));
#endif
   /*  The following statement covers the case where there are
       no state variables y. */
#if( AME_NBOF_EXPLICIT_STATE == 0 )
   amesys->states[0] = 0.0;
#endif
   
   /* Call Input to read submodel and simulation parameters. */
   if(Input(amesys) != AME_NO_ERROR) return 0;
   modelSetInitValues(amesys->pModel, amesys->states, amesys->discrete_states);
	
   /* Register print interval that maybe used bys some submodels */
   recordtinc_(amesys->simOptions->tInc);
  
   setstarttime_(time);
  
   /* Call pre-initialize function */

   PreInitialize(amesys,amesys->states);

#ifndef AME_INPUT_IN_MEMORY
   if( NeedReloadInputFiles() != 0 )
   {
      ClearGPs();
      if(Input(amesys) != AME_NO_ERROR) return 0;
      modelSetInitValues(amesys->pModel, amesys->states, amesys->discrete_states);
      ClearReloadedFiles();
   }
#endif
   
   /* Call initialize subroutine to set con and icon array members */
   
   Initialize(amesys,amesys->states);

   /* Overwriting initial state values with requests emitted by */
   /* submodels that have a more global view (cf. register.c mechanism) */
   /* Can also fire some callbacks to 'fix' float and integer store */
   OverloadStatesWithRegister(amesys, amesys->states, SVREGISTER_DEFAULT);

   CheckSimParams(amesys, &amesys->simOptions->abstol,
                  &amesys->simOptions->reltol,
                  &amesys->simOptions->hMax); 
   
#ifdef AME_RESULT_FILE
   /*  Open file for results. */
   amesys->AmeReadFile(amesys, &time, amesys->states);
#endif
   
   if(isconrun_() || isusefinval_())
   {
      updateStatesFromModel(amesys, amesys->states, AME_CONTINUOUS_STATE|AME_DISCRETE_STATE);
   }

   /* Read linear analysis specification. */
#ifndef FMI
   SetLADetails(GetLAFileName(), amesys->numstates, amesys->numvars, time,  amesys->simOptions->reltol, amesys->simOptions->abstol, getfinaltime_()-time);

   /* Remove old err file */
   
   remove(GetErrorFileName());
   
   /* Initialize the Performance analyzer */
   if(!isfixedstepsolver_()) {
      PerfAnalyzer_Init(amesys, time, getfinaltime_() );
   }
#endif
   if(isconrun_())
      setstarttime_(time);

   /* Set the locked states info */
   ameSetUpLockedStatus(amesys);   
   
   return 1;
#else
   char error_message[256];
   sprintf(error_message, "AMEInitializeConditions> Should never be called for real-time simulation\n");      
   DisplayMessage(error_message);
   return 0;
#endif
}

#if(AME_MODEL_ISEXPLICIT == 1)
static int ameEvalTstartFixed(AMESIMSYSTEM *amesys, const double *modelInputs, double *modelOutputs)
{
   double time = getstarttime_();
   
   ameInputs(amesys, AME_NBOF_INPUTS, modelInputs);

   doAssembly(amesys);

   amesys->tlast = time;

   /* Evaluation of the model at tStart */
   {
      int local_flag = 0;
      SetIsPrinting(amesys);
      amesys->FuncEval(amesys, time, amesys->states, amesys->yh, NULL, &local_flag);
      ClearIsPrinting(amesys);

#if defined(AME_RESULT_FILE) && !defined(STANDALONESIMULATOR)
      amesys->OutputResults(amesys, amesys->tlast);
      amesys->resultFileStructPtr->lastprinttime = amesys->tlast;
#endif
   
      /* Now, amesys->first_call == 0 (set at the end of FunctionEval) */
   }

#if (AME_NBOF_OUTPUTS > 0)
   memcpy(modelOutputs, amesys->outputs,amesys->numoutputs*sizeof(double) );
#endif
   amesys->tlast = time;
   
#ifdef AME_PROCESS_TIME
   /* Initialization of time timers */
   ProcessTime(0);
#endif
   
   return 1;
}

static int ameInitializeConditionsFixed(AMESIMSYSTEM *amesys)
{
   double start_time = amesys->simOptions->tStart;
   
   assert(amesys);

   {
   static int num_fixed = 15;
   static int FIXED[15] = {9, 10, 45, 53, 110, 144, 145, 180, 188
                        , 245, 279, 280, 315, 323, 378};

      SetGlobalSystemFixed(amesys, num_fixed, FIXED);
   }
   
   
   amesys->first_call=1;  /* should this be done or not ?*/

   amesys->tlast=TLAST_MIN;
   
   /* Init solver */   
   memset(amesys->yh,0,(AME_NBOF_SOLVER_STATES*13)*sizeof(double));

   InitFixedStepIntegrate(amesys);

   setfinaltime_(amesys->simOptions->tFinal);

   /* Call Input to read submodel and simulation parameters. */
   if(Input(amesys) != AME_NO_ERROR) return 0;
   modelSetInitValues(amesys->pModel, amesys->states, amesys->discrete_states);

   recordtinc_(amesys->simOptions->tInc);

   setstarttime_(start_time);
   
   /* Call pre-initialize function */

   PreInitialize(amesys,amesys->states);

#ifndef AME_INPUT_IN_MEMORY
   if( NeedReloadInputFiles() != 0 )
   {
      ClearGPs();
      if(Input(amesys) != AME_NO_ERROR) return 0;
      modelSetInitValues(amesys->pModel, amesys->states, amesys->discrete_states);
      ClearReloadedFiles();
   }
#endif

   /* Call initialize subroutine to set con and icon array members */
   
   Initialize(amesys,amesys->states);

   /* Overwriting initial state values with requests emitted by */
   /* submodels that have a more global view (cf. register.c mechanism) */
   /* Can also fire some callbacks to 'fix' float and integer store */
   OverloadStatesWithRegister(amesys, amesys->states, SVREGISTER_DEFAULT);

#ifdef AME_RESULT_FILE
   /*  Open file for results. */
   amesys->AmeReadFile(amesys, &start_time, amesys->states);
#endif
   
   if(isconrun_() || isusefinval_())
   {
      updateStatesFromModel(amesys, amesys->states, AME_CONTINUOUS_STATE|AME_DISCRETE_STATE);
   }

   /* Set the locked states info */
   ameSetUpLockedStatus(amesys);
   
   if(isconrun_()) {
      setstarttime_(start_time);
   }
   
   return 1;
}
#endif

/*=============================================================================*/

/*=============================================================================*/

/***********************************************************
   Purpose    : Test request acceptance
   Author	  : J.Andre
   Created on : 2016 - 09 - 05
   Inputs	  : 
      event   : entry event
   Outputs	  :
      AME_NO_ERROR : event accepted
      AME_SEQUENCE_ERROR : event refused
   Revision   :
************************************************************/
static AMESystemError AmesysControlRequest(AMESIMSYSTEM *amesys, AMESystemCmd event)
{
   AMESystemError res = AME_NO_ERROR;
   
   if(!amesys) {
      if(event == AME_CMD_GET_MODEL_INFO) {
         return AME_NO_ERROR;
      }
      return AME_SEQUENCE_ERROR;
   }
   
   switch(event) {
      case AME_CMD_RELEASE: {
         if( !(amesys->systemState & (AMESTATE_TERMINATED | AMESTATE_FATAL | AMESTATE_INSTANTIATED)) ) {
            res = AME_SEQUENCE_ERROR;
         }         
      }
      break;
      case AME_CMD_SETUP: {
         if( !(amesys->systemState & AMESTATE_INSTANTIATED) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_INITIALIZE: {
         if(amesys->systemState != AMESTATE_READY) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_TERMINATE: {
         if( !(amesys->systemState & (AMESTATE_RUN | AMESTATE_READY | AMESTATE_ERROR)) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_TSTART: {
         if( !(amesys->systemState & AMESTATE_INITIALIZED) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_STEP: {
         if( !(amesys->systemState & AMESTATE_RUNNING) ) {
            res = AME_SEQUENCE_ERROR;
         }
         else {
            amesys->systemState |= AMESTATE_STEP_IN_PROGRESS;
         }
      }
      break;
      case AME_CMD_RESTART: {
         /* At this time, it is not still implemented */
         /* Depends of clean-up in terminated state and static variables */
         res = AME_SEQUENCE_ERROR;
      }
      break;
      case AME_CMD_GET_MODEL_INFO: {
         res = AME_NO_ERROR;
      }
      break;
      case AME_CMD_SET_MODEL_PARAM_TUNABLE: {
         if( !(amesys->systemState & (AMESTATE_READY | AMESTATE_RUN)) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_SET_MODEL_PARAM:
      case AME_CMD_SET_RUN_PARAM:
      case AME_CMD_SET_SOLVER_PARAM: {
         if(amesys->systemState != AMESTATE_READY) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_GET_MODEL_PARAM:
      case AME_CMD_GET_SOLVER_PARAM:
      case AME_CMD_GET_RUN_PARAM: {
         if( !(amesys->systemState & (AMESTATE_READY | AMESTATE_RUN | AMESTATE_ERROR)) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_REQ_RUN_INTERRUPT: {
         if( !(amesys->systemState & (AMESTATE_INSTANTIATED | AMESTATE_READY | AMESTATE_INITIALIZED | AMESTATE_RUNNING)) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_SET_ENV: {
         if( !(amesys->systemState & AMESTATE_INSTANTIATED) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      case AME_CMD_FUNCEVAL: {
         if( !(amesys->systemState & AMESTATE_RUNNING) ) {
            res = AME_SEQUENCE_ERROR;
         }
      }
      break;
      default:
         res = AME_SEQUENCE_ERROR;
      break;
   }
   
   return res;
}  

/***********************************************************
   Purpose    : Update state and manage result of request
   Author	  : J.Andre
   Created on : 2016 - 09 - 05
   Inputs	  : 
      event   : entry event
      reqResult : result of request achievement
   Outputs	  :
      AME_NO_ERROR : event accepted
      AME_SEQUENCE_ERROR : event refused
   Revision   :
************************************************************/
static AMESystemError AmesysUpdateState(AMESIMSYSTEM *amesys, AMESystemCmd event, AMESystemError reqResult)
{
   unsigned int newState;
   AMESystemError res;

   if(!amesys) {
      return AME_SEQUENCE_ERROR;
   }
   
   newState = amesys->systemState;   /* Default: no change in state */
   res = reqResult;                  /* Default: no change in result */
   
   switch(event) {
      case AME_CMD_INSTANTIATE: {
         if(reqResult != AME_NO_ERROR) {
            newState = AMESTATE_FATAL;
         }
         else {
            newState = AMESTATE_INSTANTIATED;
         }
      }
      break;
      case AME_CMD_SETUP: {
         if(reqResult != AME_NO_ERROR) {
            newState = AMESTATE_FATAL;
         }
         else {
            newState = AMESTATE_READY;
         }
      }
      break;
      case AME_CMD_INITIALIZE: {
         if(reqResult != AME_NO_ERROR) {
            newState = AMESTATE_ERROR;
         }
         else {
            newState = AMESTATE_INITIALIZED;
         }
      }
      break;
      case AME_CMD_TERMINATE: {
         if(reqResult != AME_NO_ERROR) {
            amesys->systemState = AMESTATE_TERMINATED; /* Avoid automatic call to ameterminate */
            newState = AMESTATE_FATAL;
         }
         else {
            newState = AMESTATE_TERMINATED;
         }
      }
      break;
      case AME_CMD_TSTART: {
         if(reqResult != AME_NO_ERROR) {
            newState = AMESTATE_ERROR;
         }
         else {
            newState = AMESTATE_RUNNING;
         }
      }
      break;
      case AME_CMD_STEP: {
         if(reqResult != AME_NO_ERROR) {
            /* test ameExitStatus */
            if(reqResult == AME_EXIT_ERROR) {
               if(amesys->ameExitStatus == 0) {
                  /* Simulation stopped early but normally */
                  amesys->requestinterrupt = 0;
                  newState = AMESTATE_STOPPED;
                  res = AME_NO_ERROR;
               }
            }
            else {
               newState = AMESTATE_ERROR;
            }
         }
         else if(amesys->requestinterrupt) {
            amesys->requestinterrupt = 0;
            newState = AMESTATE_STOPPED;
         }
         newState &= ~AMESTATE_STEP_IN_PROGRESS;
      }
      break;
      case AME_CMD_REQ_RUN_INTERRUPT:
      break;
      case AME_CMD_FUNCEVAL: {
         if(reqResult != AME_NO_ERROR) {
            /* test ameExitStatus */            
            if(reqResult == AME_EXIT_ERROR) {
               if(amesys->ameExitStatus == 0) {
                  /* Simulation stopped early but normally */
                  newState = AMESTATE_STOPPED;
                  res = AME_NO_ERROR;
               }
            }
            else {
               newState = AMESTATE_ERROR;
            }
         }
      }
      break;
      default:
      /* No state change, no exception to catch, just pass result */
      break;
   }
      
   if(amesys->systemState != newState) {
      if(newState == AMESTATE_FATAL) {
         if(amesys->systemState & (AMESTATE_RUN | AMESTATE_READY | AMESTATE_ERROR)) {
            /* Terminating the simulation */
            ameTerminate(amesys);
         }
      }
      
#if !defined(STANDALONESIMULATOR)
      if(newState & (AMESTATE_FATAL | AMESTATE_ERROR)) {
         if(amesys->systemState & AMESTATE_RUN) {
            amefprintf(stderr, "Simcenter Amesim model: simulation failed.\n");
         }
         else if(amesys->systemState & AMESTATE_IDLE) {
            amefprintf(stderr, "Simcenter Amesim model: instantiation failed.\n");
         }
         else if(amesys->systemState & AMESTATE_READY) {
            amefprintf(stderr, "Simcenter Amesim model: initialization failed.\n");
         }
      }
      else if(newState & AMESTATE_INITIALIZED) {
         amefprintf(stdout, "Simcenter Amesim model: initialization done.\n");
      }
      else if(newState & AMESTATE_TERMINATED) {
         amefprintf(stdout, "Simcenter Amesim model: simulation terminated.\n");
      }
      else if((newState & AMESTATE_RUNNING) && (amesys->systemState & AMESTATE_INITIALIZED)) {
         amefprintf(stdout, "Simcenter Amesim model: simulation started.\n");
      }
#endif      
      
      /* Update state */
      amesys->systemState = newState;
   }
   
   if(amesys->systemState == AMESTATE_FATAL) {
      /* Greedy error */
      res = AME_FATAL_ERROR;
   }

   return res;
}
 
/***********************************************************
   Purpose    : Instantiate the system
   Author	  : J.Andre
   Created on : 2016 - 09 - 08
   Inputs	  : None
   Outputs	  : Error code
   Revision   :
************************************************************/
static AMESystemError AmesysInstantiate(AMESIMSYSTEM **amesysPtr)
{
   int jump_ret;
   AMESIMSYSTEM *amesys;
   AMESystemError result = AME_FATAL_ERROR;
   
   S_AME_Model *pModel;
   
   if(*amesysPtr != NULL) {
      return AME_SEQUENCE_ERROR;
   }
   
   result = createModel(&pModel, &GmodelDef, GParamInfo, GVarInfo, GsubmodelNameArray,
                        GcontStateVarNum, GdiscStateVarNum);
   
   if(result == AME_NO_ERROR) {
#if(AME_MODEL_ISEXPLICIT == 1)
      amefprintf(stdout, "Instantiating a system with %d unknowns.\n", AME_NBOF_EXPLICIT_STATE);
#else
      amefprintf(stdout, "Instantiating a system with %d unknowns.\n", AME_NBOF_SOLVER_STATES);
#endif
      if (strcmp(soldToId,"not available") != 0)
         amefprintf(stdout, "Simcenter Amesim version: %s (%s).\n", "2019.1", soldToId);
      else
         amefprintf(stdout, "Simcenter Amesim version: %s.\n", "2019.1");
      result = createAMESystem(&amesys, pModel, AME_NBOF_SOLVER_STATES, AMEVERSION);
   }

   if(result == AME_NO_ERROR) {
      if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* ~try */ 
         SetGlobalSystem(amesys);
         
         SetGlobalSystemFixed(amesys, AME_NBOF_FIXED_VAR_PARAMS, GFixedVarNum);

#ifdef AME_INPUT_IN_MEMORY
         amesys->getssflist = getssflist;
#endif
         amesys->consflag = 0;

#if(AME_MODEL_ISEXPLICIT == 1)
         amesys->FuncEval = localFuncEval;
         amesys->JFuncEval = localJFuncEval;
#else
         amesys->res = localFuncEval;
         amesys->Jres = localJFuncEval;
#endif
         amesys->AmeExit = ModelAmeExit;

         amesys->ameExitStatus = 0;
  
         /* Set input directory to current directory */
         AmeSetInputDir(amesys,NULL);

         /* Set output directory to current directory */
         AmeSetOutputDir(amesys,NULL);
            
         /* Set base name of input files, no extension */
         AmeSetModelBaseName(amesys,"SIMOTICS_1_Test_", NULL);
         
         result = AME_NO_ERROR;
      }
      else { /* Catch AmeExit */
         result = AME_EXIT_ERROR;
      }
   }

   if(result == AME_NO_ERROR) {
      /* Update state and result */
      result =  AmesysUpdateState(amesys, AME_CMD_INSTANTIATE, result);
   }
   
   if(result != AME_NO_ERROR) {
      deleteAMESystem(&amesys);
      SetGlobalSystem(NULL);
   }
   else {
      *amesysPtr = amesys;
   }

   return result;
}

static AMESystemError AmesysTerminate(AMESIMSYSTEM *amesys)
{
   int jump_ret;
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_TERMINATE);
   if(res != AME_NO_ERROR) {
      /* Request not accepted */
      return res;
   }

   if(res == AME_NO_ERROR) { /* Request accepted */  
      if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* ~try */ 
         ameTerminate(amesys);
      }
      else { /* Catch AmeExit */
         res = AME_EXIT_ERROR;
      }
   }
   
   /* Update state and result */
   res =  AmesysUpdateState(amesys, AME_CMD_TERMINATE, res);
   
   return res;
}

static AMESystemError AmesysRelease(AMESIMSYSTEM **amesysPtr)
{
   AMESystemError res = AME_NO_ERROR;
   if(*amesysPtr) {
      SetGlobalSystem(*amesysPtr);
      res = AmesysControlRequest(*amesysPtr, AME_CMD_RELEASE);
   
      if(res == AME_NO_ERROR) { /* Request accepted */
         AmeSignalModelUnload();
         res = deleteAMESystem(amesysPtr);
         SetGlobalSystem(NULL);
      }
   }
   return res;
}

/***********************************************************
   Purpose    : Go in Ready state to be able receive external
                initialization of model and simulation
   Author	  : J.Andre
   Created on : 2016 - 09 - 08
   Inputs	  : loadParam: if true, model parameters are read 
   Outputs	  : Error code
   Revision   :
************************************************************/
static AMESystemError AmesysSetUp(AMESIMSYSTEM *amesys, const int loadParam)
{
   int jump_ret;   
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SETUP);
   
   if(res != AME_NO_ERROR) {
      /* Request not accepted */
      return res;
   }
   
   if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* ~try */     
      amesys->ameExitStatus = 0;

      /* Construct file names */
      AmeConstructFileName(amesys);
      
      /* Read sim file */
      {
         SIMOptions opts;

#ifndef AME_INPUT_IN_MEMORY
         char errMsg[PATH_MAX+128];
         int result = readsimfile(&opts, GetSimFileName(), errMsg);
#else
         int result = readsimfromchararrays(&opts, simparams, simparams_length);
#endif
         
         if(result != 0) {
            
            /* Initialyze Amesystem SIMOptions */
            memcpy(amesys->simOptions,&opts,sizeof(SIMOptions));

#ifdef AME_RESULT_FILE
            amesys->simOptions->outoff = 0;
#else
            amesys->simOptions->outoff = 1;
#endif
            
#ifdef AME_PERFORMANCE_ANALYZER
            ALA_Setparam(opts.autoLA, 1, opts.autoLAstep);
            DISCLOG_SetParam(1);
#endif
         }
         else {
            res = AME_SETUP_ERROR;
         }
      }
   
      if(loadParam) {
         /* Load parameters */
         loadModelParameters(amesys);
      }

   }
   else { /* Catch AmeExit */
      res = AME_EXIT_ERROR;
   }
   
   /* Update state and result */
   res =  AmesysUpdateState(amesys, AME_CMD_SETUP, res);

   return res;
}

/***********************************************************
   Purpose    : Initialize the model
   Author	  : J.Andre
   Created on : 2016 - 09 - 08
   Inputs	  : None 
   Outputs	  : Error code
   Revision   :
************************************************************/
static AMESystemError AmesysInitialize(AMESIMSYSTEM *amesys)
{
   int jump_ret;
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_INITIALIZE);
   
   if(res != AME_NO_ERROR) {
      /* Request not accepted */
      return res;
   }

   if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* try */   
      if(amesys->simOptions->runType & 0x20) {
         /* fixed step initialization */
#if(AME_MODEL_ISEXPLICIT == 1)
         ameSetOptionsFixed(amesys, amesys->simOptions->tInc, amesys->simOptions->fixedType,
            amesys->simOptions->fixedOrder, amesys->simOptions->fixedH, amesys->simOptions->runType);
            
         if(!ameInitializeConditionsFixed(amesys)) {
            res = AME_INITIALIZE_ERROR;
         }
#else
         amefprintf(stderr,"It is not possible to use a fixed step solver\nfor implicit systems.\n");
         res = AME_INITIALIZE_ERROR;
#endif
      }
      else {
#ifdef AME_VSOLVER_ACCEPTED
         /* variable step initialization */
         ameSetOptions(amesys, amesys->simOptions->tInc, amesys->simOptions->hMax, amesys->simOptions->tol,
            amesys->simOptions->errorType, amesys->simOptions->iWrite, amesys->simOptions->rStrtp,
            amesys->simOptions->statistics, amesys->simOptions->runType, amesys->simOptions->solverType);
            
         if(!ameInitializeConditions(amesys)) {
            res = AME_INITIALIZE_ERROR;
         }
#else
         amefprintf(stderr,"It is not possible to use a variable step solver\nfor this interface.\n");
         res = AME_INITIALIZE_ERROR;
#endif
      }
      
#if defined(AME_MEMORY_ACCESS_RT_EXPORT) && (AME_NBOF_REAL_PARAMS>0)
      memcpy(RT_Export_RealParam, amesys->pModel->realParamArray, sizeof(double)*AME_NBOF_REAL_PARAMS);
#endif
#if defined(AME_MEMORY_ACCESS_RT_EXPORT) && (AME_NBOF_INT_PARAMS>0)
      memcpy(RT_Export_IntParam, amesys->pModel->integerParamArray, sizeof(int)*AME_NBOF_INT_PARAMS);
#endif
      
   }
   else { /* Catch AmeExit */
      res = AME_EXIT_ERROR;
   }

   /* Update state and result */
   res =  AmesysUpdateState(amesys, AME_CMD_INITIALIZE, res);
   
   return res;
}

#if(AME_MODEL_ISEXPLICIT == 1)
static void doAFixedStep(AMESIMSYSTEM *amesys, double time)
{
   double timerange;
   double actual_timestep;
   int stepratio;
   int stepratio_ceil;
   int stepratio_floor;
   int i=0;
   int zero=0;
   double tinc;
   int isTimeForPrint=0;
   double next_print_time;

   if (amesys->first_call)
   {
      SetIsPrinting(amesys);
      amesys->FuncEval(amesys, amesys->tlast, amesys->states, amesys->yh, NULL, &zero);
      ClearIsPrinting(amesys);
  
#if defined(AME_RESULT_FILE) && !defined(STANDALONESIMULATOR)
      amesys->OutputResults(amesys, amesys->tlast);
      amesys->resultFileStructPtr->lastprinttime = amesys->tlast;
#endif
   }

   timerange = time - amesys->tlast;
   
   if(timerange <= 0.0)
   {
      return;
   }
   if(amesys->simOptions->fixedH > 0.0)
   {
      stepratio = stepratio_ceil = (int)ceil(timerange/amesys->simOptions->fixedH);
      stepratio_floor = (int)floor(timerange/amesys->simOptions->fixedH);
      
      actual_timestep = timerange/(double)stepratio_ceil;

      if(fabs(actual_timestep-amesys->simOptions->fixedH) > 0.001*amesys->simOptions->fixedH)
      {
         if(stepratio_floor == 0)
         {
#ifdef AMEDEBUG
            amefprintf(stdout,"skipping %14.8e  range= %14.8e  actual_timestep=%14.8e stepratio_ceil=%d   stepratio_floor=%d\n",time, timerange, actual_timestep, stepratio_ceil,stepratio_floor);
#endif
            return;
         }
      
#ifdef AMEDEBUGw
         amefprintf(stdout,"using floor\n");
         amefprintf(stdout,"timerange=%14.8e\n", timerange);
         amefprintf(stdout,"actual_timestep=%14.8e\n", actual_timestep);
         amefprintf(stdout,"amesys->simOptions->fixedH=%14.8e\n", amesys->simOptions->fixedH);
         amefprintf(stdout,"fabs(actual_timestep-amesys->simOptions->fixedH)=%14.8e\n", fabs(actual_timestep-amesys->simOptions->fixedH));
         amefprintf(stdout,"stepratio_floor=%d\n", stepratio_floor);
         amefprintf(stdout,"stepratio_ceil=%d\n", stepratio_ceil);
#endif
         actual_timestep = timerange/(double)stepratio_floor;
         stepratio = stepratio_floor;
      }
      
      if(fabs(actual_timestep-amesys->simOptions->fixedH) > 0.001*amesys->simOptions->fixedH)
      {
#ifdef AMEDEBUGw
         amefprintf(stdout,"Adjusting time step %14.8e => %14.8e\n", amesys->simOptions->fixedH, actual_timestep);
         amefprintf(stdout,"stepratio_floor=%d\n", stepratio_floor);
         amefprintf(stdout,"stepratio_ceil=%d\n", stepratio_ceil);
         amefprintf(stdout,"stepratio=%d\n", stepratio);
         amefprintf(stdout,"timerange=%14.8e\n", timerange);
#endif
      }
   }
   else
   {
      /* just single step from the last point in time when we were called */
      stepratio=1;
      actual_timestep = timerange;
   }

#if defined(AME_RESULT_FILE) && !defined(STANDALONESIMULATOR)
   if(amesys->resultFileStructPtr && !amesys->resultFileStructPtr->outoff)
   {
      next_print_time = GetNextPrintTime(&tinc, amesys->resultFileStructPtr->lastprinttime, amesys->simOptions->tFinal, amesys->simOptions->tInc, actual_timestep);
   }
#endif

#ifdef AMERT
   /* Allow changes of the stepratio - typically on RT platforms. */
   {
      double localstepratio=floor(IL_SIMOTICS_1_Test_step_ratio);
      if (localstepratio >= 1) 
      {
         actual_timestep=timerange/localstepratio;
         stepratio = (int)localstepratio;
      }   
   }
#endif
#ifdef STANDALONESIMULATOR
   FixedStepIntegrate(amesys,AME_NBOF_SOLVER_STATES,amesys->tlast,time,amesys->simOptions->tInc,amesys->states,
         amesys->yh,amesys->simOptions->fixedType,amesys->simOptions->fixedOrder,actual_timestep);
#else 
	for (i=0; (i<stepratio) && (amesys->requestinterrupt == 0);i++)
   {
      /*Integrate one step */
	  if ( amesys->simOptions->fixedType == 1)
	  {
		DoAnABStep(amesys, amesys->numstates, amesys->simOptions->fixedOrder, &amesys->tlast, actual_timestep, amesys->states, amesys->yh);
	  }
	  else
	  {
		DoAnRKStep(amesys, amesys->numstates, amesys->simOptions->fixedOrder, &amesys->tlast, actual_timestep, amesys->states, amesys->yh);
	  }
      
#ifndef AMERT   
      isTimeForPrint = amesys->resultFileStructPtr && (!amesys->resultFileStructPtr->outoff && ((amesys->tlast >= next_print_time) || ((next_print_time-amesys->tlast)/tinc < TIME_ROUNDOFF)));
      if(isTimeForPrint)
      {
#ifdef AME_PROCESS_TIME
         ProcessTime(1);
#endif
         SetIsPrinting(amesys);
         amesys->FuncEval(amesys, amesys->tlast, amesys->states, amesys->yh, NULL, &zero);
         ClearIsPrinting(amesys);
         amesys->OutputResults(amesys,amesys->tlast);
         next_print_time = GetNextPrintTime(&tinc, amesys->tlast, amesys->simOptions->tFinal, amesys->simOptions->tInc, actual_timestep);
      }
      else
#endif
      {
         amesys->FuncEval(amesys, amesys->tlast, amesys->states, amesys->yh, NULL, &zero);
      }
   }
#endif  
   amesys->tlast = time;
}
#endif

static int ameOutputs(AMESIMSYSTEM *amesys, double timeARG, int numOutputs, double *outputARG)
{
   int theprintflag=1;
   double *dot;
   double *v;
   
   assert(amesys);

   v = amesys->v;
   dot = amesys->dotstates;

   if(numOutputs != amesys->numoutputs)
   {
      char error_message[256];
      sprintf(error_message, "AMEOutputs> Expected %d outputs but got %d\n", amesys->numoutputs, numOutputs);
      DisplayMessage(error_message);
      AmeExit(1);
   }

   if (amesys->simOptions->runType == 4)
	{
		/* stabilizing has already been processed during Init.*/
		/*Exit */
		return 1;
	}

   if(timeARG < amesys->tlast)
   {
      DisplayMessage("trying to integrate backwards\n");
      return 0;
   }
#if(AME_MODEL_ISEXPLICIT == 1)
   amesys->t_end_of_time_slice = timeARG;
#ifndef AMERT
   if(!isfixedstepsolver_())
   {
      if(!IntegrateStep(amesys, amesys->tlast, timeARG))
      {
         DisplayMessage("IntegrateStep failed");
         return 0;
      }
      amesys->tlast = timeARG;
   }
   else
   {
      doAFixedStep(amesys, timeARG);
   }
#else
   doAFixedStep(amesys, timeARG);
#endif

#else
   if(!DIntegrateStep(amesys, AME_NBOF_EXPLICIT_STATE, amesys->tlast, timeARG, amesys->simOptions->tInc, amesys->states,
      amesys->dotstates, amesys->simOptions->hMax, AME_NBOF_SOLVER_STATES, amesys->iwork, amesys->simOptions->reltol,
      amesys->simOptions->abstol, amesys->simOptions->rStrtp, LIW, LRW, amesys->simOptions->statistics,
      amesys->simOptions->stabilOption, amesys->simOptions->iWrite, amesys->simOptions->minimalDiscont,
      amesys->needrestart, Gis_constraint, &amesys->requestinterrupt))
   {
      DisplayMessage("DIntegrateStep failed");
      return 0;
   }

   amesys->tlast = timeARG;
#endif

#if (AME_NBOF_OUTPUTS > 0)
   memcpy(outputARG, amesys->outputs, amesys->numoutputs*sizeof(double) );
#endif

   return 1;
}

static AMESystemError AmesysComputeAtTstart(AMESIMSYSTEM *amesys, const double *theInputs, double *theOutputs)
{
   int jump_ret;
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_TSTART);
   
   if(res != AME_NO_ERROR) {
      /* Request not accepted */
      return res;
   }

   if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* try */
      if(amesys->simOptions->runType & 0x20) {
         /* fixed step solver */
#if(AME_MODEL_ISEXPLICIT == 1)
         if(!ameEvalTstartFixed(amesys, theInputs, theOutputs)) {
            res = AME_INITIALIZE_ERROR;
         }
#else
         amefprintf(stderr,"It is not possible to use a fixed step solver\nfor implicit systems.\n");
         res = AME_INITIALIZE_ERROR;
#endif
      }
      else {
         /* variable step solver */
         if(!ameEvalTstart(amesys, theInputs, theOutputs)) {
            res = AME_INITIALIZE_ERROR;
         }
      }
      
   }
   else { /* Catch AmeExit */
      res = AME_EXIT_ERROR;
   }

   /* Update state and result */
   res =  AmesysUpdateState(amesys, AME_CMD_TSTART, res);
   
   return res;
}

/***********************************************************
   Purpose    : Do a co-simulation step
   Author	  : J.Andre
   Created on : 2016 - 09 - 12
   Inputs	  : None 
   Outputs	  : Error code
   Revision   :
************************************************************/
static AMESystemError AmesysStep(AMESIMSYSTEM *amesys, int stepType, const double t, const double *theInputs, double *theOutputs, int doInterpol, const unsigned int *orderTab, const double **polyTab)
{
   int jump_ret;
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_STEP);
   
   if(res != AME_NO_ERROR) {
      /* Request not accepted */
      return res;
   }
   
   if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* try */
   
#if defined(AME_MEMORY_ACCESS_RT_EXPORT) && (AME_NBOF_REAL_PARAMS>0)
      memcpy(amesys->pModel->realParamArray, RT_Export_RealParam, sizeof(double)*AME_NBOF_REAL_PARAMS);
#endif
#if defined(AME_MEMORY_ACCESS_RT_EXPORT) && (AME_NBOF_INT_PARAMS>0)
      memcpy(amesys->pModel->integerParamArray, RT_Export_IntParam, sizeof(int)*AME_NBOF_INT_PARAMS);
#endif

#if defined(AME_COSIM) && (AME_NBOF_INPUTS > 0)
      amesys->doInterpol = doInterpol;
   
      if(amesys->doInterpol) {
         stepType = 1;
         res = setPolyCosimSlave(amesys->csSlave, orderTab, polyTab);
      }
#else
      amesys->doInterpol = 0;
#endif
      if(res == AME_NO_ERROR) {
         if(stepType == 0) {  /* Do a step */
            CheckIfColdStartNeed(amesys->inputs, theInputs, AME_NBOF_INPUTS, amesys->num_steps_taken, &amesys->needrestart);
            
            if(!ameOutputs(amesys, t, AME_NBOF_OUTPUTS, theOutputs)) {
               res = AME_STEP_ERROR;
            }
            if(!ameInputs(amesys, AME_NBOF_INPUTS, theInputs)) {
               res = AME_STEP_ERROR;
            }
         }
         else {  /* Do a step 2 */
            if(amesys->doInterpol == 0) {   
               if(!ameInputs(amesys, AME_NBOF_INPUTS, theInputs)) {
                  res = AME_STEP_ERROR;
               }
            }
            if(!ameOutputs(amesys, t, AME_NBOF_OUTPUTS, theOutputs)) {
               res = AME_STEP_ERROR;
            }
         }
         amesys->num_steps_taken++;
         res |= setTciCosimSlave(amesys->csSlave, t);
      }
      
#if defined(AME_MEMORY_ACCESS_RT_EXPORT) && (AME_NBOF_VARS>0)
      memcpy(RT_Export_Vars, amesys->pModel->varArray, sizeof(double)*AME_NBOF_VARS);
#endif
   }
   else { /* Catch AmeExit */
      res = AME_EXIT_ERROR;
   }

   /* Update state and result */
   res =  AmesysUpdateState(amesys, AME_CMD_STEP, res);
   
   return res;
}

/***********************************************************
   Purpose    : Set solver parameters
   Author	  : J.Andre
   Created on : 2016 - 09 - 09
   Inputs	  : None
   Outputs	  : 
   Revision   :
************************************************************/
static AMESystemError AmesysSetSolverParam(AMESIMSYSTEM *amesys, const solverSettings *solver)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_SOLVER_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      simOptSetSolverParam(amesys->simOptions,solver);
   }
   
   return res;
}

/***********************************************************
   Purpose    : Get solver parameters
   Author	  : J.Andre
   Created on : 2016 - 09 - 09
   Inputs	  : None
   Outputs	  : 
   Revision   :
************************************************************/
static AMESystemError AmesysGetSolverParam(AMESIMSYSTEM *amesys, solverSettings *solver)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_SOLVER_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      simOptGetSolverParam(amesys->simOptions,solver);
   }
   
   return res;
}

/***********************************************************
   Purpose    : Set run parameters
   Author	  : J.Andre
   Created on : 2016 - 09 - 09
   Inputs	  : None
   Outputs	  : 
   Revision   :
************************************************************/
static AMESystemError AmesysSetSimParam(AMESIMSYSTEM *amesys, const simSettings *simOpt)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      simOptSetSimParam(amesys->simOptions,simOpt);
   }

   return res;
}

static AMESystemError AmesysGetSimParam(AMESIMSYSTEM *amesys, simSettings *simOpt)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_RUN_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      simOptGetSimParam(amesys->simOptions, simOpt);
   }
   
   return res;
}

static AMESystemError AmesysSetSimItem(AMESIMSYSTEM *amesys, const int Id, const int enabled)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      if(simOptSetOneOfSimParam(amesys->simOptions, Id, enabled) != 0) {
         res = AME_SETTINGS_ERROR;
      }
   }
   
   return res;
}

/***********************************************************
   Purpose    : Set run parameters
   Author	  : J.Andre
   Created on : 2016 - 09 - 09
   Inputs	  : None
   Outputs	  : 
   Revision   :
************************************************************/
static AMESystemError AmesysSetStdOptions(AMESIMSYSTEM *amesys, const stdOptions *stdOpt)
{   
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      simOptSetStdParam(amesys->simOptions,stdOpt);
   }

   return res;
} 

static AMESystemError AmesysGetStdOptions(AMESIMSYSTEM *amesys, stdOptions *stdOpt)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_RUN_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      simOptGetStdParam(amesys->simOptions, stdOpt);
   }
   
   return res;
}

static AMESystemError AmesysSetStdItem(AMESIMSYSTEM *amesys, const int Id, const int enabled)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      /* Initialyze Amesystem solver parameters */
      if(simOptSetOneOfStdParam(amesys->simOptions, Id, enabled) != 0) {
         res = AME_SETTINGS_ERROR;
      }
   }
   
   return res;
}

/***********************************************************
   Purpose     : Turn off/on results
   Author	   : J.Andre
   Created on  : 2016 - 09 - 09
   Inputs	   : 
      outoff   :  0 : result file off
                  1 : result file on
   Outputs	   :
   Revision    :
************************************************************/
static AMESystemError AmesysEnableResult(AMESIMSYSTEM *amesys, const int out)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      if(out) {
         amesys->simOptions->outoff = 0;
      }
      else {
         amesys->simOptions->outoff = 1;
      }
   }
   
   return res;
}

/***********************************************************
   Purpose     : Return the state of Amesystem
   Author	   : J.Andre
   Created     : 2016 - 09 - 05
   Inputs	   : None
   Outputs	   :
      state    : machine state
   Revision    :
************************************************************/
static unsigned int AmesysGetState(AMESIMSYSTEM *amesys)
{
   if(amesys) {
      return amesys->systemState;
   }
   else {
      return AMESTATE_IDLE;
   }
}

static AMESystemError AmesysGetModelInfo(unsigned int *numinputs, unsigned int *numoutputs, unsigned int *numstates, unsigned int *numimplicits)
{
   *numinputs = AME_NBOF_INPUTS;
   *numoutputs = AME_NBOF_OUTPUTS;
   *numstates = AME_NBOF_EXPLICIT_STATE;
   *numimplicits = AME_NBOF_IMPLICIT_STATE;
   
   return AME_NO_ERROR; 
}

static AMESystemError AmesysGetModelPortName(const char **inputName[], const char **outputName[])
{
   *inputName = GinputVarTitles;
   *outputName = GoutputVarTitles;
      
   return AME_NO_ERROR; 
}

static AMESystemError AmesysGetModelNumParam(unsigned int *numParam)
{
   *numParam = AME_NBOF_PARAMS;
   
   return AME_NO_ERROR; 
}

static AMESystemError AmesysGetParamType(const int nbParam, const int idx[], E_ParamCType paramType[])
{
   AMESystemError res = AME_NO_ERROR;
   int i;
   
   for(i = 0; (i < nbParam) && (res == AME_NO_ERROR); i++) {
      if(i < AME_NBOF_PARAMS) {
         switch(GParamInfo[idx[i]].category) {
            case E_Int_Param:
               paramType[i] = E_CType_IntParam;
            break;
            case E_Text_Param:
               paramType[i] = E_CType_StringParam;
            break;
            default:
               paramType[i] = E_CType_DoubleParam;
            break;
         }
         return AME_NO_ERROR;
      }
      else {
         res = AME_PARAM_IDX_ERROR;
      }
   }
   
   return res;
}

static AMESystemError AmesysFindParamFromVar(const int nbParam, const int varIdx[], int paramIdx[], E_ParamCategory category[])
{
   AMESystemError res = AME_NO_ERROR;
   int idx, i;
   
   for(idx = 0; (idx < nbParam) && (res == AME_NO_ERROR); idx++) {
      res = AME_PARAM_IDX_ERROR;
      for(i = 0; i < AME_NBOF_PARAMS; i++) {
         if(GParamInfo[i].varIdx == varIdx[idx]) {
            paramIdx[idx] = i;
            category[idx] = GParamInfo[i].category;
            res = AME_NO_ERROR;
         }
      }
   }
  
   return res;
}

static AMESystemError AmesysGetIntParamValue(AMESIMSYSTEM *amesys, const int nbParam, const int idx[], int value[])
{
   AMESystemError res;
   int i;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_MODEL_PARAM);
      
   for(i = 0; (i < nbParam) && (res == AME_NO_ERROR); i++) {
      res = getIntParameter(amesys->pModel, idx[i], &value[i]);
   }
   
   return res;
}

static AMESystemError AmesysGetDoubleParamValue(AMESIMSYSTEM *amesys, const int nbParam, const int idx[], double value[])
{
   AMESystemError res;
   int i;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_MODEL_PARAM);
   
   for(i = 0; (i < nbParam) && (res == AME_NO_ERROR); i++) {
      res = getDoubleParameter(amesys->pModel, idx[i], &value[i]);
   }
   
   return res;
}

static AMESystemError AmesysGetStringParamValue(AMESIMSYSTEM *amesys, const int nbParam, const int idx[], const char* value[])
{
   AMESystemError res;
   int i;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_MODEL_PARAM);
   
   for(i = 0; (i < nbParam) && (res == AME_NO_ERROR); i++) {
      res = getStringParameter(amesys->pModel, idx[i], &value[i]);
   }
   
   return res;
}

static AMESystemError AmesysSetIntParamValue(AMESIMSYSTEM *amesys, const int nbParam, const int idx[], const int value[])
{
   AMESystemError res;
   int i;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM_TUNABLE);
   
   for(i = 0; (i < nbParam) && (res == AME_NO_ERROR); i++) {
      int isTunable;
      res = isParamTunable(amesys->pModel, idx[i], &isTunable);
      
      if(res ==  AME_NO_ERROR) {
         if(isTunable) {
            res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM_TUNABLE);
         }
         else {
            res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM);
         }
      }
      if(res ==  AME_NO_ERROR) {
         res = setIntParameter(amesys->pModel, idx[i], value[i]);
      }
      if(res == AME_NO_ERROR) {
         SignalInputChange();
         res = setParamAsUserDefined(amesys->pModel, idx[i]);
      }
   }
   
   return res;
}

static AMESystemError AmesysSetDoubleParamValue(AMESIMSYSTEM *amesys, const int nbParam, const int idx[], const double value[])
{
   AMESystemError res;
   int i;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM_TUNABLE);
      
   for(i = 0; (i < nbParam) && (res == AME_NO_ERROR); i++) {
      int isTunable;
      res = isParamTunable(amesys->pModel, idx[i], &isTunable);
      
      if(res ==  AME_NO_ERROR) {
         if(isTunable) {
            res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM_TUNABLE);
         }
         else {
            res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM);
         }
      }
      if(res ==  AME_NO_ERROR) {
         res = setDoubleParameter(amesys->pModel, idx[i], value[i]);
      }
      if(res == AME_NO_ERROR) {
         SignalInputChange();
         res = setParamAsUserDefined(amesys->pModel, idx[i]);
      }
   }
   
   return res;
}

static AMESystemError AmesysSetStringParamValue(AMESIMSYSTEM *amesys, const int nbParam, const int idx[], const char* value[])
{
   AMESystemError res;
   int i;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM_TUNABLE);
   
   for(i = 0; (i < nbParam) && (res == AME_NO_ERROR); i++) {
      int isTunable;
      res = isParamTunable(amesys->pModel, idx[i], &isTunable);
      
      if(res ==  AME_NO_ERROR) {
         if(isTunable) {
            res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM_TUNABLE);
         }
         else {
            res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM);
         }
      }
      if(res ==  AME_NO_ERROR) {
         res = setStringParameter(amesys->pModel,idx[i], value[i]);
      }
      if(res == AME_NO_ERROR) {
         res = setParamAsUserDefined(amesys->pModel, idx[i]);
      }
   }
   
   return res;
}

static AMESystemError AmesysGetDoubleGlobalParamValue(AMESIMSYSTEM *amesys, const int nbParam, const char* name[], double value[])
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_MODEL_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      int i;
      for(i = 0; i<nbParam; i++) {
         if(getGlobalParamValueByName(name[i], &value[i]) != no_error) {
            res = AME_GLOBAL_PARAMETER_ERROR;
            break;
         }
      }
   }
   
   return res;
}

static AMESystemError AmesysGetStringGlobalParamValue(AMESIMSYSTEM *amesys, const int nbParam, const char* name[], const char* value[])
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_GET_MODEL_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      int i;
      for(i = 0; i<nbParam; i++) {
         if(getTextGlobalParamValueByName(name[i], &value[i]) != no_error) {
            res = AME_GLOBAL_PARAMETER_ERROR;
            break;
         }
      }
   }
   
   return res;
}

static AMESystemError AmesysSetIntGlobalParamValue(AMESIMSYSTEM *amesys, const int nbParam, const char* name[], const int value[])
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM);

   if(res == AME_NO_ERROR) { /* Request accepted */
      int i;
      for(i = 0; i<nbParam; i++) {
         if(ChangeOrAddIntGlobalParamValue(name[i], value[i], 1) != AME_NO_ERROR) {
            res = AME_GLOBAL_PARAMETER_ERROR;
            break;
         }
      }
   }
   
   return res;
}

static AMESystemError AmesysSetDoubleGlobalParamValue(AMESIMSYSTEM *amesys, const int nbParam, const char* name[], const double value[])
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      int i;
      for(i = 0; i<nbParam; i++) {
         if(ChangeOrAddRealGlobalParamValue(name[i], value[i], 1) != AME_NO_ERROR) {
            res = AME_GLOBAL_PARAMETER_ERROR;
            break;
         }
      }
   }

   return res;
}

static AMESystemError AmesysSetStringGlobalParamValue(AMESIMSYSTEM *amesys, const int nbParam, const char* name[], const char* value[])
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_MODEL_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      int i;
      for(i = 0; i<nbParam; i++) {
         if(ChangeOrAddTextGlobalParamValue(name[i], value[i], 1) != AME_NO_ERROR) {
            res = AME_GLOBAL_PARAMETER_ERROR;
            break;
         }
      }
   }

   return res;
}

static AMESystemError AmesysRequestRunInterrupt(AMESIMSYSTEM *amesys)
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_REQ_RUN_INTERRUPT);

   if(res == AME_NO_ERROR) { /* Request accepted */
      amesys->requestinterrupt = 1;

	   /* Update state and result */
	   res =  AmesysUpdateState(amesys, AME_CMD_REQ_RUN_INTERRUPT, res);
   }

   return res;
}

static AMESystemError AmesysSetFinalTime(AMESIMSYSTEM *amesys, const double finaltime)
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);

   if(res == AME_NO_ERROR) { /* Request accepted */
      amesys->simOptions->tFinal = finaltime;
   }

   return res;
}

static AMESystemError AmesysSetInitTime(AMESIMSYSTEM *amesys, const double inittime)
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);

   if(res == AME_NO_ERROR) { /* Request accepted */
      amesys->simOptions->tStart = inittime;
   }

   return res;
}

static AMESystemError AmesysSetPrintInterval(AMESIMSYSTEM *amesys, const double tInc)
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);

   if(res == AME_NO_ERROR) { /* Request accepted */
      amesys->simOptions->tInc = tInc;
   }

   return res;
}

static AMESystemError AmesysSetLogger( int (*newameInternalfprintf)(FILE *fp, const char *fmt, va_list ap) )
{
   ameInstallFprintf(newameInternalfprintf);
   return AME_NO_ERROR;
}

static AMESystemError AmesysSetInputDir(AMESIMSYSTEM *amesys, const char *inputDir)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_ENV);

   if(res == AME_NO_ERROR) { /* Request accepted */
      AmeSetInputDir(amesys, inputDir);
   }

   return res;
}

static AMESystemError AmesysSetOutputDir(AMESIMSYSTEM *amesys, const char *outputDir)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_ENV);

   if(res == AME_NO_ERROR) { /* Request accepted */
      AmeSetOutputDir(amesys, outputDir);
   }

   return res;
}

static AMESystemError AmesysSetBaseFilesName(AMESIMSYSTEM *amesys, const char *baseName, const char* extension)
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_ENV);

   if(res == AME_NO_ERROR) { /* Request accepted */
      AmeSetModelBaseName(amesys, baseName, extension);
   }

   return res;
}

static AMESystemError AmesysSetResultFilesName(AMESIMSYSTEM *amesys, const char *outName)
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_ENV);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      AmeSetResultFileName(amesys, outName);
   }

   return res;
}

/* To ensure compliancy with old template and entry points */
static AMESystemError AmesysSetParamAsInitModel(AMESIMSYSTEM *amesys,
                  double time,
                  double PrintInterval, 
                  double MaxTimeStep,
                  double tolerance,
                  int errCtrl,
                  int writeLevel,
                  int extraDisconPrints,
                  int runStats,
                  int runType,
                  int thesolvertype)
{
   AMESystemError res;

   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);
   res |= AmesysControlRequest(amesys, AME_CMD_SET_SOLVER_PARAM);

   if(res == AME_NO_ERROR) { /* Request accepted */
      SIMOptions *sim_options = amesys->simOptions;   

      solverSettings solver;
      simSettings sim;
      stdOptions std;

      solver.integType =  0; /* Variable solver */
      solver.settings.variable.hMax = MaxTimeStep;
      solver.settings.variable.tol = tolerance;
      solver.settings.variable.errorType = errCtrl;

      simOptSetSolverParam(amesys->simOptions, &solver);

      sim.tStart = time;
      sim.tFinal = amesys->simOptions->tFinal;
      sim.simOpt = 0;

      if(runType & 0x01) {
         sim.simOpt |= SIMOPT_SIM_CONTINUATION_RUN;
      }
      if(runType & 0x02) {
         sim.simOpt |= SIMOPT_SIM_USE_FINAL_VALUES;
      }
      if(writeLevel != 2) {
         sim.simOpt |= SIMOPT_SIM_MONITOR_TIME;
      }
      if(runStats) {
         sim.simOpt |= SIMOPT_SIM_STATISTICS;
      }

      if(PrintInterval <= 0) {
         sim.tInc = -PrintInterval;
         amesys->simOptions->outoff = 1;
      }
      else {
         sim.tInc = PrintInterval;
         amesys->simOptions->outoff = 0;
      }

      simOptSetSimParam(amesys->simOptions, &sim);
   
      std.simMode = ((runType & 0x0c) >> 2);
      std.stdOpt = 0;
   
      if(extraDisconPrints) {
         std.stdOpt |= SIMOPT_STD_DISC_PRINTOUT;
      }
      if(runType & 0x10) {
         std.stdOpt |= SIMOPT_STD_HOLD_INPUTS;
      }
      if(amesys->simOptions->stabilOption & 0x01) {
         std.stdOpt |= SIMOPT_STD_LOCK_STATES;
      }
      if(amesys->simOptions->stabilOption & 0x02) {
         std.stdOpt |= SIMOPT_STD_DIAGNOSTICS;
      }
      if(thesolvertype) {
         std.stdOpt |= SIMOPT_STD_CAUTIOUS;
      }
      if(amesys->simOptions->minimalDiscont) {
         std.stdOpt |= SIMOPT_STD_MIN_DISC_HANDLING;
      }
      if(runType & 0x100) {
         std.stdOpt |= SIMOPT_STD_DISABLE_OPTIMAZED;
      }
      if(sim_options->activityIndex & 0x01) {
         std.stdOpt |= SIMOPT_STD_ACTIVITY;
      }
      if(sim_options->activityIndex & 0x02) {
         std.stdOpt |= SIMOPT_STD_POWER;
      }
      if(sim_options->activityIndex & 0x04) {
         std.stdOpt |= SIMOPT_STD_ENERGY;
      }
      simOptSetStdParam(amesys->simOptions, &std);
   }
   return res;
}

/* To ensure compliancy with old template and entry points */
static AMESystemError AmesysSetParamAsFixedStep(AMESIMSYSTEM *amesys,
                                       double start_time, int run_type, int solver_type,
                                       int runge_kutta_order, double fixed_h, double printinterval)
{
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_SET_RUN_PARAM);
   res |= AmesysControlRequest(amesys, AME_CMD_SET_SOLVER_PARAM);
   
   if(res == AME_NO_ERROR) { /* Request accepted */
      solverSettings solver;
      
      res = AME_NO_ERROR;

      solver.integType = 1; /* Fixed solver */
      solver.settings.fixed.fixedH = fixed_h;
      solver.settings.fixed.fixedOrder = runge_kutta_order;
      solver.settings.fixed.fixedType = solver_type;
      
      simOptSetSolverParam(amesys->simOptions, &solver);
      amesys->simOptions->tStart = start_time;
      
      if(printinterval <= 0) {
         amesys->simOptions->outoff = 1;
         amesys->simOptions->tInc = -printinterval;
      }
      else {
         amesys->simOptions->outoff = 0;
         amesys->simOptions->tInc = printinterval;
      }
      simOptSetOneOfSimParam(amesys->simOptions, SIMOPT_SIM_CONTINUATION_RUN, run_type & 0x01);
      simOptSetOneOfSimParam(amesys->simOptions, SIMOPT_SIM_USE_FINAL_VALUES, (run_type & 0x02)>>1);
   }
   return res;
}

#ifndef AME_COSIM

static AMESystemError AmesysInitME(AMESIMSYSTEM *amesys, int withAssembly)
{
   AMESystemError res = AME_NO_ERROR;
   int jump_ret;
   
   res = AmesysControlRequest(amesys, AME_CMD_INITIALIZE);
   
   if(res != AME_NO_ERROR) {
      /* Request not accepted */
      return res;
   }
   
   /* Load parameters */
   res = Input(amesys);
   
   /* Set initial values */
   modelSetInitValues(amesys->pModel, amesys->states, amesys->discrete_states);
   
   if(res == AME_NO_ERROR) {
      if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* try */
         
         setstarttime_(amesys->simOptions->tStart);
         setfinaltime_(amesys->simOptions->tFinal);

         amesys->first_call = 1;  /* should this be done or not ?*/
         amesys->needrestart = 1;

         amesys->tlast = TLAST_MIN;

         memset(amesys->ecount,0,amesys->numstates*sizeof(int));
         memset(amesys->dotstates,0,amesys->numstates*sizeof(double));
         
         PreInitialize(amesys,amesys->states);
         
#ifndef AME_INPUT_IN_MEMORY
         if( NeedReloadInputFiles() != 0 )
         {
            ClearGPs();
            Input(amesys);
            /* Set initial values */
            modelSetInitValues(amesys->pModel, amesys->states, amesys->discrete_states);
            ClearReloadedFiles();
         }
#endif

         Initialize(amesys,amesys->states);
         
         OverloadStatesWithRegister(amesys, amesys->states, SVREGISTER_DEFAULT);
         
         if(withAssembly) {
            doAssembly(amesys);
         }
         amesys->tlast = getstarttime_();
#ifdef AME_PROCESS_TIME
         /* Initialization of time timers */
         ProcessTime(0);
#endif
      }
      else { /* Catch AmeExit */
         res = AME_EXIT_ERROR;
      }
   }
   
   /* Update state and result */
   res =  AmesysUpdateState(amesys, AME_CMD_INITIALIZE, res);
   
   if(res == AME_NO_ERROR) {
      /* Go in Run state: simulate acknowledgment of evaluation at t start command */
      res =  AmesysUpdateState(amesys, AME_CMD_TSTART, res);
      
#if defined(AME_MEMORY_ACCESS_RT_EXPORT) && (AME_NBOF_REAL_PARAMS>0)
      memcpy(RT_Export_RealParam, amesys->pModel->realParamArray, sizeof(double)*AME_NBOF_REAL_PARAMS);
#endif
#if defined(AME_MEMORY_ACCESS_RT_EXPORT) && (AME_NBOF_INT_PARAMS>0)
      memcpy(RT_Export_IntParam, amesys->pModel->integerParamArray, sizeof(int)*AME_NBOF_INT_PARAMS);
#endif
   }
   
   return res;
}

static AMESystemError AmesysFuncEvalME(AMESIMSYSTEM *amesys, double t, double *y, double *yprime, double *delta, int *flag)
{
   int jump_ret;
   AMESystemError res;
   
   SetGlobalSystem(amesys);
   res = AmesysControlRequest(amesys, AME_CMD_FUNCEVAL);
   
   if(res != AME_NO_ERROR) {
      /* Request not accepted */
      return res;
   }
   
   if( (jump_ret = setjmp(amesys->jump_env)) == 0) { /* try */
      amesys->doInterpol = 0;
      localFuncEval(amesys, t, y, yprime, delta, flag);
   }
   else { /* Catch AmeExit */
      res = AME_EXIT_ERROR;
   }

   /* Update state and result */
   res =  AmesysUpdateState(amesys, AME_CMD_FUNCEVAL, res);   
   return res;
}

#endif

/****************************/

#define SYSNME SIMOTICS_1_Test_

#if defined(STANDALONESIMULATOR)
#include "ame_standalone_simulator.h"

#elif defined(FMICS1)
#include "ame_fmics1.h"
#elif defined(FMICS2)
#include "ame_fmics2.h"
#elif defined(FMIME1) || defined(FMIME2)
#if(AME_MODEL_ISEXPLICIT == 0)
#error "FMI for model exchange is not allowed for implicit model."
#elif defined(AMERT)
#error "FMU for real-time is not allowed for model exchange."
#elif defined(FMIME1)
#include "ame_fmime1.h"
#else
#include "ame_fmime2.h"
#endif
#elif defined(FMIX)
#include "ame_user_cosim.h"

#elif defined(AMEUSERCOSIM)
#include "ame_user_cosim.h"

#elif defined(AME_CS_SIMULINK)
#include "ame_simulink_cosim.h"

#elif defined(AME_ME_SIMULINK)
#include "ame_simulink_me.h"

#elif defined(LABVIEWCOSIM)
#include "labview_cosim.h"

#elif defined(AMEVERISTAND)
#if(AME_MODEL_ISEXPLICIT == 0)
#error "VeriStand interface is not allowed for implicit model."
#else
#include "AME_NIVERISTAND_API.c"
#define AMEVERISTAND_LOG_FILE "c:\\temp\\NIV_SIMOTICS_1_Test_.log"
#include "NIV_model.c"
#endif

#elif defined(AME_CS_ADAMS)
#include "ame_adams_cosim.h"

#elif defined(AME_ME_ADAMS)
#include "adams_cont.h"

#elif defined(AME_CS_MOTION)
#include "ame_motion_cosim.h"

#elif defined(AME_ME_MOTION)
#include "ame_motion_me.h"

#elif defined(DISCRETEPART)
#include "ame_discrete_part.h"

#elif defined(GEN_COSIM)
#include "gen_cosim.h"

#else
#error "Unknown interface defined. Cannot generate Amesim model code."
#endif
