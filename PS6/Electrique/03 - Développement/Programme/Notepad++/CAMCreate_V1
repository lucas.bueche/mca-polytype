(*--------------------------------------------------------------------------------
-- Company: Polytype - HEIA-FR
-- Engineer/Student: FABIO ZAUGG
--
-- Create Date:   11:33:33 04/03/2021
-- Design Name:   
-- Module Name:   CAMCreate.stl
-- Project Name:  Module de Chargement 
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- Code pour module de Chargement MCA - Calcul des paramètre de la chaine et creation de la camme du SLAVE ( SERVOMOTEUR )
-- Dependencies:
-- 		
-- Revision:
-- Revision 0.01 -  Tested and working
-- Additional Comments:
-- 
-- Notes: 
-- R1 et R2 sont les deux détecteurs reflex
--------------------------------------------------------------------------------*)
INTERFACE
	USEPACKAGE cam;
	USELIB LCamHdl;
	
	USES Chain_Detection; // Utilise ses variables globales
	
	
	VAR_GLOBAL 
		createcam_cmd : BOOL ; // Variable pour activer la creation de la came
		resetcam : BOOL ;	// Variable pour reseter la came
		
		calculateChain : BOOL := FALSE ; // Variable pour activer le calcul de paramêtre de la chaine ... Sinon si  Buffer vide il y a des divisions par 0
		
		Picot_length_mm: REAL ; // longueur du picot 
		Loading_offset_tube: REAL := 200; //mm - distance entre tube et le début du picot 
		
		Diametre_gear: REAL := 200;//mm  - diametre de la roue entre le servomoteur et les courroie
		
		Tube_length_mm: REAL := 100; 
		Tube_dia_mm: REAL := 50;
		Tube_thikness_mm: REAL : 1.0; 
		
		Chain_speed: REAL ; // in mm/s - vitesse de la chaine
		Chain_separation: REAL ; //in ms - temps entre la passage de deux picot devant le meme détecteur ( R1 ou R2)
		
		Insertion_time: REAL ; // temps a disposition pour l'insertion
		
		
		
	END_VAR
	
	PROGRAM Createcam;
END_INTERFACE

IMPLEMENTATION

	PROGRAM Createcam
		VAR 
			CamProfile:sLCamHdlCamProfileType;
			FBCreateCam:FBLCamHdlCreateCam;
			
			Mean_t_Between: REAL ; // temps qui passe entre la detection du meme picot entre R1 et R2 En moyenne
			Mean_t_Full: REAL ;    // temps qui passe entre la detection de deux picot sur le meme detecteur - En moyenne
			
			Master_rps: REAL ; // round per second of master (360deg)
			
			Master_cam_deg: REAL ; // position du master ( Virtualisation de la chaine)
			Following_cam_deg: REAL ; // Position du slave ( virtualisation de moteur de chargement)
		
		END_VAR 
		
		IF calculateChain = TRUE THEN
		
			//Calculate mean times of 11 last samples
			
			Mean_t_Between := BUF_T_Between.MEAN_VAL() * Cycle_interrupt_time; //ms --gives us speed of picot ( mean of 11 last mesures)
			Mean_t_Full :=BUF_T_Full.MEAN_VAL() * Cycle_interrupt_time; // ms --give us time separation between two picots ( mean OF 11 last mesures)
			
			//Calculate needed variables 
			Chain_speed:=(Detector_separation / Mean_t_Between) * 1000; //mm/s	 -- ([mm]/[ms])*1000	
			Insertion_time:=(Tube_dia_mm -2 * Tube_thikness_mm) / Chain_speed; //s
			Master_degs:= 360 / (Mean_t_Full / 1000); // deg/ secondes
			
			//Calculate cam position in degree of masterAxis and followingAxis for loading of 1 tube
			Master_cam_deg:= Master_degs * Insertion_time; //deg - is the angle the master makes during the given time
			Following_cam_deg:=((Loading_offset_tube + Tube_length_mm -0.05 * Picot_length_mm) / (3.1415 * Diametre_gear)) * 360; // Loading Lengt(=LoadingOffset+TubeLength+5%ofPicotLenght) divided per length of 1 motor turn multiplicated per 360 
			//is the angle the Servomotor makes during the given time for inserting 1 TUBE
			
			
			
			//Values for Cam creation
			CamProfile.i16NumberOfElements := 2; // To points 
			CamProfile.asCamElement[0].eCamSegmentType := Poly_5; // Makes symetric speed curve
			CamProfile.asCamElement[0].sSegmentParameter.r64MasterPosStart := 0.0; 				// Start point for insertion 
			CamProfile.asCamElement[0].sSegmentParameter.r64MasterPosEnd := Master_cam_deg; 	// Chain angle at end of insertion
			CamProfile.asCamElement[0].sSegmentParameter.r64SlavePosStart := 0.0; 				// Start point for insertion
			CamProfile.asCamElement[0].sSegmentParameter.r64SlavePosEnd := Following_cam_deg;	// Servomotor angle at end of insertion	
			
			CamProfile.asCamElement[1].eCamSegmentType := Poly_5;								// Need to be changed to linear to exclude errors
			CamProfile.asCamElement[1].sSegmentParameter.r64MasterPosStart := Master_cam_deg;	// Chain angle at end of insertion
			CamProfile.asCamElement[1].sSegmentParameter.r64MasterPosEnd := 360.0;
			CamProfile.asCamElement[1].sSegmentParameter.r64SlavePosStart:= Following_cam_deg;	// Servomotor angle at end of insertion	
			CamProfile.asCamElement[1].sSegmentParameter.r64SlavePosEnd := Following_cam_deg;	// Servomotor virtual position at end of cycle
			
// Modulo needs to be set at "Following_cam_deg" value for the Following axes in order to work.
		
		END_IF;
		
		FBCreateCam(
	execute := createcam_cmd // Create cam with the values when set to true
	,reset := resetcam
	,toCam := Cam_1
	,camProfile := CamProfile
	// ,performanceMode := 
	// ,done => 
	// ,busy => 
	// ,error => 
	// ,errorID => 
	// ,errorInfo => 
	// ,errorSegNo => 
	);
	
	END_PROGRAM 
END_IMPLEMENTATION
