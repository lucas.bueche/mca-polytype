%% premiers essai du bloc de creation des rampes

EndPos = 0.215; %Distance d'insertion
EndTime = 0.120;%Temps d'insertion
PeakVel = EndPos*2/EndTime; % Velocit�e Maximale
%PeakVel = 3.6;
%Velocity*EndTime must be greater than q(end)-q(0).
%Velocity*EndTime/2 must be less than or equal to q(end)-q(0).


 wpts = [0 EndPos]; %position en (mm)
 
[q, qd, qdd, tvec, pp] = trapveltraj(wpts, 501,'EndTime',EndTime,'PeakVelocity',PeakVel);

%wpts etant les point donn�es
%501 le nombre de valeur par vecteur de sortie 
%End Time et Peak vel les valeur calcul�es ci-dessus

       subplot(3,1,1)
       plot(tvec, q)
       subplot(3,1,2)
       plot(tvec, qd)
       subplot(3,1,3)
       plot(tvec, qdd)

