from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.forms import ModelForm

# Create your models here.
class Chain(models.Model):
    chain_name = models.CharField(max_length=100, unique=True)
    edit_date = models.DateField(auto_now=True)
    distance = models.FloatField()
    length = models.FloatField()
    diameter = models.FloatField()
    note = models.CharField(max_length=300, blank=True, default='')
    CAO = models.FileField(upload_to='chains/CAO', blank='True', default='')
    schematic = models.FileField(upload_to='chains/Schematic', blank=True, default='')

    def __str__(self):
        return self.chain_name

class Setting(models.Model):
    setting_name = models.CharField(max_length=200, unique=True)
    chain = models.ForeignKey(Chain, on_delete=models.CASCADE)
    tube_length = models.FloatField()
    tube_width = models.FloatField()

    def __str__(self):
        return self.setting_name

class Error(models.Model):
    class Alert_Type(models.TextChoices):
        WARNING = '1', _('Warning')
        LOW = '2', _('Low')
        MEDIUM = '3', _('Medium')
        HIGH = '4', _('High')
        CRITICAL = '5', _('Critical')

    error_name = models.CharField(max_length=100)
    error_description = models.CharField(max_length=300)
    alert_type = models.CharField(max_length=1, choices=Alert_Type.choices, default=Alert_Type.WARNING)
    timestamp = models.DateTimeField(default=timezone.now)
    ack = models.BooleanField(default=False)