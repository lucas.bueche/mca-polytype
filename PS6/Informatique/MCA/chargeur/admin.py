from django.contrib import admin

from .models import Chain, Setting, Error
# Register your models here.

admin.site.register(Chain)
admin.site.register(Setting)
admin.site.register(Error)