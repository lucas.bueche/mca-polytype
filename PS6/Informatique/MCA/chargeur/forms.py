from django.forms import ModelForm, Textarea
from django.utils.translation import gettext_lazy as _
from .models import Chain, Setting

class SettingForm(ModelForm):
    class Meta:
        model = Setting
        fields = ['setting_name','tube_length','tube_width','chain']

class ChainForm(ModelForm):
    class Meta:
        model = Chain
        fields = ['chain_name','diameter','length','distance','note', 'CAO','schematic']
        labels = {
            'chain_name' : _('Name'),
            'diameter' : _('Tooth Diameter (in mm)'),
            'length' : _('Tooth Length (in mm)'),
            'distance' : _('Distance between teeth (in mm)'),
        }
        widget = {
            'note' : Textarea(attrs={'cols':80, 'rows':20}),
        }