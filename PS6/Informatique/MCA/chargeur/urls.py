from django.urls import path

from . import views

app_name = 'chargeur'
urlpatterns = [
    #//ex: /chargeur/
    path('', views.index, name='index'),
    path('<int:setting_id>', views.index_load, name='index_load'),
    path('load', views.load, name='load'),
    path('chain/', views.chain, name='chain'),
    path('chain/<int:chain_id>/', views.chain_detail, name='chain_detail'),
    path('chain/add/', views.add_chain, name='add_chain'),
    path('stats/', views.stats, name='stats'),
    path('error/', views.error, name='error'),
    path('error/<int:error_id>/', views.error_detail, name='error_detail'),
    path('about/', views.about, name ='about')
]