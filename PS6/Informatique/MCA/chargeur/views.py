import chargeur
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, Http404
from django.views.generic.edit import UpdateView
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.http import HttpResponse
from django.contrib import messages

from .models import Chain, Setting, Error
from .forms import ChainForm, SettingForm

def index(request):
    active = 'index'
    form = SettingForm(request.POST or None)
    if request.method == 'POST':
        if 'save' in request.POST:
            if form.is_valid():
                form.save()
                messages.info(request, 'Setting has been successfully added')
                return redirect('chargeur:index')
    context = {
        'new' : True,
        'active' : active,
        'form': form
        }
    return render(request, 'chargeur/index.html', context)

def index_load(request, setting_id):
    active = 'index'
    setting = Setting.objects.get(pk=setting_id)
    form = SettingForm(request.POST or None, instance=setting)
    if form.is_valid():
        form.save()
        messages.info(request, 'Setting has been successfully updated')
        return redirect('chargeur:index')
    context = {
        'active' : active,
        'form': form
    }
    return render(request, 'chargeur/index.html', context)

def load(request):
    active = 'index'
    setting_list = Setting.objects.order_by('setting_name')
    if request.method == 'POST':
        setting_id =  request.POST['setting_id']
        return redirect('chargeur:index_load', setting_id)
    context = {
        'active' : active,
        'setting_list' : setting_list
    }
    return render(request, 'chargeur/index.html', context)

def chain(request):
    active = 'chains'
    chain_list = Chain.objects.order_by('chain_name')
    context = {
        'active' : active,
        'chain_list':chain_list
        }
    return render(request, 'chargeur/chain.html', context)

def chain_detail(request, chain_id):
    active = 'chains'
    chain_list = Chain.objects.order_by('chain_name')
    chain = Chain.objects.get(pk=chain_id)
    form = ChainForm(request.POST or None, instance=chain)
    if request.method == 'POST':
        if 'update' in request.POST:
            form = ChainForm(request.POST or None, request.FILES or None, instance=chain)
            if form.is_valid():
                form.save()
                messages.info(request, 'Chain has been successfully updated')
                return redirect('chargeur:chain_detail', chain_id)
        elif 'delete' in request.POST:
            chain.delete()
            return redirect('chargeur:chain')
    context = {
        'active' : active,
        'chain_list':chain_list,
        'chain': chain,
        'form': form,
    }
    return render(request, 'chargeur/chain.html', context)

def add_chain(request):
    active = 'chains'
    chain_list = Chain.objects.order_by('chain_name')
    form = ChainForm(request.POST or None, request.FILES)
    if form.is_valid():
        form.save()
        messages.info(request, 'Chain has been successfully added')
        return redirect('chargeur:add_chain')
    context = {
        'active' : active,
        'chain_list': chain_list,
        'form': form,
    }
    return render(request, 'chargeur/addChain.html', context)

def stats(request):
    active = 'stats'
    context = {
        'active' : active
    }
    return render(request, 'chargeur/stats.html', context)

def error(request):
    active = 'errors'
    warning_list = Error.objects.filter(alert_type='1').order_by("-timestamp")
    low_list = Error.objects.filter(alert_type='2').order_by("-timestamp")
    medium_list = Error.objects.filter(alert_type='3').order_by("-timestamp")
    high_list = Error.objects.filter(alert_type='4').order_by("-timestamp")
    critical_list = Error.objects.filter(alert_type='5').order_by("-timestamp")
    context = {
        'active' : active,
        'warning_list':warning_list,
        'low_list':low_list,
        'medium_list':medium_list,
        'high_list':high_list,
        'critical_list':critical_list,
        }
    return render(request, 'chargeur/error.html', context)

def error_detail(request, error_id):
    return HttpResponse("Error number %s page" % error_id)

def about(request):
    active = 'about'
    context = {
        'active' : active
    }
    return render(request, "chargeur/about.html", context)